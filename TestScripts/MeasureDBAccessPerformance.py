from influxdb import InfluxDBClient
from timeit import default_timer

timeLimit = 1645455919000000000

influxDbClient = InfluxDBClient(host='lxyrpc01.gsi.de', database="cryring_db", username='cry_data', password='cryring')

dataSetCount = influxDbClient.query('SELECT count(current) FROM YR08DT1ML_ContAcq WHERE time > ' + str(timeLimit))
startTime = default_timer()
result = influxDbClient.query('SELECT * FROM YR08DT1ML_ContAcq WHERE time > ' + str(timeLimit))
runTime = default_timer() - startTime

print('time:', runTime, 'number of datasets:', dataSetCount)

