from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework import CommonFunctions as cf
import matplotlib.pyplot as plt
import re
import pandas as pd
from pandas import DataFrame
from datetime import datetime

timeFormatRegex = '[\\d]{4}-[\\d]{2}-[\\d]{2}T[\\d]{2}:[\\d]{2}:[\\d]{2}.[\\d]{1,9}Z'


def homogenizeTimeExpression(timeExpression) -> str:
    if isinstance(timeExpression, int):
        epochInSeconds = timeExpression / 1.0e10
        return cf.convertUnixEpochToInfluxDatetime(epochInSeconds)
    elif isinstance(timeExpression, float):
        return datetime.utcfromtimestamp(timeExpression).strftime(cf.InfluxTimeFormatString())
    elif isinstance(timeExpression, str):
        regex = re.compile(timeFormatRegex)
        if regex.match(timeExpression):
            return timeExpression
        else:
            raise Exception("Input did not have the right format. Input was: " + str(
                timeExpression) + '. Expected format was: YYYY-MM-DDThh:mm:ss.fZ with at least one digit after comma.')
    else:
        raise Exception("Don't know what to do with time expression " + str(timeExpression))


class InfluxDataExtractor():
    _influxConnector = None
    _dbClient = None

    def __init__(self):
        self._influxConnector = InfluxDbConnector(clientType='HTTP')
        self._dbClient = self._influxConnector.initFluxClient()

    def executeStatement(self, queryStatement: str, measurementID: str = None) -> DataFrame:
        queryResult = None

        try:
            queryResult = self._influxConnector.executeFluxStatement(queryStatement=queryStatement)
        except Exception as err:
            print('Error occurred when executing query: ' + queryStatement)
            print(err)
            raise err

        return queryResult

    def getDataFromInfluxByTimeRange(self, measurementID: str, lowerTimeLimit, upperTimeLimit, fieldNames: str =
    None) -> pd.DataFrame:
        """
        Method to fetch data within given time range. Fields in select can be limited by providing fieldNames. If no
        fieldNames have been specified, query performs "SELECT *". fieldnames can as well be given as regular expression

        :param measurementID: name of the measurement, normally a nomenclature. Use Grafana to check available
        measurements
        :param lowerTimeLimit: lower limit of the time range to select. Provide either as UNIX epoch (integer,
        10 digits = seconds, 19 digits = nanoseconds) or as formatted datetime string (don't forget the trailing "Z")
        :param upperTimeLimit: lower limit of the time range to select. Provide either as UNIX epoch (integer,
        10 digits = seconds, 19 digits = nanoseconds) or as formatted datetime string (don't forget the trailing "Z")
        :return:
        """

        if fieldNames is None:
            queryStatement = 'SELECT * FROM ' + measurementID + """ WHERE time>='""" + homogenizeTimeExpression(
                lowerTimeLimit) + """' AND time<='""" + homogenizeTimeExpression(upperTimeLimit) + """'"""
        else:
            queryStatement = 'SELECT ' + fieldNames + ' FROM ' + measurementID + """ WHERE time>='""" + \
                             homogenizeTimeExpression(
                lowerTimeLimit) + """' AND time<='""" + homogenizeTimeExpression(upperTimeLimit) + """'"""

        return self.executeStatement(queryStatement=queryStatement, measurementID=measurementID)

    def plotDataInTimeRange(self, measurementID: str, lowerTimeLimit, upperTimeLimit, fieldNames: str = None,
                            xTimeAxis: bool = False):
        result: pd.DataFrame = self.getDataFromInfluxByTimeRange(measurementID, lowerTimeLimit, upperTimeLimit,
                                                                 fieldNames)
        if xTimeAxis:
            datetimeSeries = pd.Series(map(lambda ts: datetime.strptime(ts, cf.InfluxTimeFormatString()),
                                           result['time']))
            result['time'] = datetimeSeries
            result.plot(x='time', y='mean')
            plt.xlabel('UTC time')
        else:
            result.plot()
        plt.show()
