import sys
from datetime import datetime
from Framework.InfluxDbConnectorFlux import InfluxDbConnector
import pandas as pd
from pandas import DataFrame, Series
from matplotlib import pyplot as plt
import numpy as np
from stumpy import mpdist
from scipy.optimize import differential_evolution
from scipy.stats import skew
import traceback

from sklearn.linear_model import LinearRegression


class PulsedDataExtractor():

    _DATETIMEFORMAT = '%Y-%m-%d %H:%M:%S.%f+00:00'
    _fetchResult_frame: DataFrame
    _final_result_frame = DataFrame()
    _influxConnector = InfluxDbConnector(clientType='UDP')
    _influxConnector.initInfluxQLClient()
    _mp_discrimination_level: float
    _reference_buffer_size: int
    _reference_buffer_frame = DataFrame()

    def __init__(self, reference_buffer_size: int, mp_discrimination_level: float):
        self._reference_buffer_size = reference_buffer_size
        self._mp_discrimination_level = mp_discrimination_level

    def aggregate_rawdata(self, rawdata_data_frame: DataFrame, aggregate_frame: DataFrame, aggregate_data: bool = True):
        print('starting to aggregate', end='\r')
        rawdata_data_frame.dropna()
        rawdata_data_frame.reset_index(inplace=True)  # remove index
        rawdata_data_frame.rename({'index': 'time', 'intensity': 'intensity', 'lastInjection': 'lastinjection',
                              'lastExtraction': 'lastextraction'}, axis=1, inplace=True)
        rawdata_data_frame['time'] = pd.to_datetime(rawdata_data_frame['time'], utc=True, unit='ns')
        rawdata_data_frame = rawdata_data_frame.set_index('time')

        output_frame = DataFrame()

        if aggregate_data:
            # output_frame['baseline'] = rawdata_data_frame.agg(func=calculate_column_median, axis=1)
            output_frame['rawdata_sum'] = rawdata_data_frame.values.sum()
            output_frame['rawdata_std'] = rawdata_data_frame.values.std()
            output_frame['rawdata_mean'] = rawdata_data_frame.values.mean()
            output_frame['rawdata_median'] = np.mean(rawdata_data_frame.values)
            output_frame['rawdata_skewness'] = skew(rawdata_data_frame.values)

            # data_without_baseline = rawdata_data_frame.transpose() - output_frame['baseline'].transpose()
            # data_without_baseline = data_without_baseline.transpose()
            boxfit_data = rawdata_data_frame.agg(func=PulsedDataExtractor._do_boxfit, axis=1)

            output_frame = pd.concat([boxfit_data, boxfit_data], axis=1)
        else:
            output_frame = rawdata_data_frame

        print('*************** aggregation result **************\n', output_frame)
        aggregate_frame = pd.concat([aggregate_frame, output_frame.reset_index()], axis=1)

        return aggregate_frame

    @staticmethod
    def _box_for_boxfit(data, *offset_slope_height_center_width):
        """
        from https://stackoverflow.com/questions/46624376/problems-fitting-to-boxcar-function-using-scipys-curvefit
        -in-python
        :param data: array-like data
        :param height: box height
        :param center: box center
        :param width: box width
        :return: box function
        """
        offset, slope, height, center, width = offset_slope_height_center_width
        fit_function = offset + slope * data + height * (center - width / 2 < data) * (data < center + width / 2)
        return fit_function

    @staticmethod
    def _do_boxfit(data: DataFrame):
        res = {'success': False}
        print('boxfitting...', end='\r')

        data_smoothed: np.ndarray = PulsedDataExtractor._smooth(data[20:], 51)
        smoothed_max = np.max(data_smoothed)
        x = np.linspace(0, len(data_smoothed), num=len(data_smoothed))
        bounds = [((np.mean(data_smoothed[:100]) - np.mean(data_smoothed[:100]) * 0.1),
                   (np.mean(data_smoothed[:100]) + np.mean(data_smoothed[:100]) * 0.1)), (-10000, 10000),
                  (0, np.max(data_smoothed)), (0, len(data_smoothed)), (100, 10000)]

        offset = slope = height = center = width = signal_mean = signal_std = signal_skew = 0.0

        signal_mean = np.nan
        signal_std = np.nan
        signal_skew = np.nan

        try:
            res = differential_evolution(lambda p: np.sum((PulsedDataExtractor._box_for_boxfit(x,
                                                                                               *p) - data_smoothed) ** 2),
                                         bounds=bounds)
            offset = res['x'][0]
            slope = res['x'][1]
            height = res['x'][2]
            center = res['x'][3]
            width = res['x'][4]

            signal_lower = int(center - width / 2) + 10  # pad signal to avoid spike at end
            signal_upper = int(center + width / 2) - 10
            signal_data = data[signal_lower:signal_upper].reset_index()
            signal_data = signal_data.drop(columns={'index'})

            signal_mean = signal_data.values.mean()
            signal_std = signal_data.values.std()
            signal_skew = skew(signal_data.values)[0]

        except Exception as err:
            traceback.print_exception(type(err), err, err.__traceback__)
            pass

        if res['success']:
            out_dict = {}
            out_dict['boxfit_smoothed_max'] = smoothed_max
            out_dict['boxfit_offset'] = offset
            out_dict['boxfit_slope'] = slope
            out_dict['boxfit_height'] = height
            out_dict['boxfit_center'] = center
            out_dict['boxfit_width'] = width
            out_dict['boxfit_signal_mean'] = signal_mean
            out_dict['boxfit_signal_std'] = signal_std
            out_dict['boxfit_signal_skew'] = signal_skew
            # linear fit of top of signal
            try:
                #linfit = scipy.stats.linregress(x=np.linspace(0, len(signal_data), num=len(signal_data)),
                # y=signal_data)

                x_data = np.linspace(signal_lower, signal_upper, num=len(signal_data))
                linear_regressor = LinearRegression()
                linear_fit = linear_regressor.fit(x_data[:, np.newaxis], signal_data)
                print(linear_fit.coef_)
                out_dict['boxfit_signal_fit_slope'] = linear_fit.coef_[0][0]

                # plt.scatter(x_data, signal_data, s=1)
                # y_fit = linear_regressor.predict(x_data[:, np.newaxis])
                # plt.plot(x_data, y_fit)
                # plt.show()
            except Exception:
                traceback.print_exc(file=sys.stdout)
                pass

            return Series(out_dict)
        else:
            return Series()

    def fetchDataFrameFromInflux(self, measurement: str, fieldSelector: str, lowerTime: int = None,
                                 upperTime: int = None) -> (object, DataFrame):
        self._fetchResult_frame = DataFrame()
        whereStatement = None
        whereForPrint = ''

        if lowerTime is not None and upperTime is not None:
            whereStatement = "time>" + str(lowerTime) + " AND time<" + str(upperTime)
            whereForPrint = "time>" + pd.to_datetime(lowerTime, utc=False).strftime(self._DATETIMEFORMAT) + \
                            " AND time<" + pd.to_datetime(upperTime, utc=False).strftime(self._DATETIMEFORMAT)
        elif lowerTime is not None and upperTime is None:
            whereStatement = "time>" + str(lowerTime)
            whereForPrint = "time>" + pd.to_datetime(lowerTime, utc=True).strftime(self._DATETIMEFORMAT)

        print(datetime.now(), '- Fetching data where', whereForPrint)
        self._fetchResult_frame = self._influxConnector.fetchDataByInfluxQL(measurement, fieldSelector, whereStatement)
        print(type(self._fetchResult_frame))

        if (self._fetchResult_frame.__len__()) > 0 | (self._fetchResult_frame is not None):
            print('fetched', self._fetchResult_frame.values.__len__(), 'rows.')
        else:
            print('query did not return data')

        return (self, self._fetchResult_frame)

    def fetchRawdataAsTimeSeries(self, measurement: str, lowerTime: str = None,
                                 upperTime: str = None) -> DataFrame:
        """
        Special fetch method providing functionality to fetch raw-data where each raw-data sample is stored as one
        column in the database as data organized as time series

        :param measurement: measurement ID = device nomen
        :param sampling_frequency: raw data sampling frequency
        :param lowerTimeEpoch: start timestamp for fetching data
        :param upperTimeEpoch: stop timestamp for fetching data
        :return: pandas.DataFrame with time series data of rawdata pulses
        """

        fieldSelector = '/rawdata.*/'
        # fetch data as inidexed array
        out_data = self.fetchDataFrameFromInflux(measurement, fieldSelector, lowerTime, upperTime)
        if out_data is not None:
            out_data = out_data.reset_index()
            out_data = out_data.rename(columns={'index': 'acq_stamp'})
            out_data = out_data.melt('acq_stamp', var_name='rawdata')
            out_data.rawdata = out_data.rawdata.str.extract('(\d+)').astype(int)
            rawdiff_as_ns = out_data.rawdata.sub(out_data.rawdata.max()).abs().mul(100)
            out_data['timestamp'] = out_data.acq_stamp.sub(pd.to_timedelta(rawdiff_as_ns, unit='ns'))
            out_data = out_data.sort_values('timestamp', ignore_index=True)
            out_data = out_data.rename(columns={'rawdata': 'index'})
            out_data = out_data.set_index('timestamp')

            # plt.plot(out_data['value'].values)  # plt.show()

        return out_data

    def mpdistance_for_fetch_result(self, aggregate_rawdata: bool = True):

        data = self._fetchResult_frame.T
        data = data.reset_index(drop=True)

        for item in data.items():
            print('Sampling mp dist...')

            if self._reference_buffer_frame.values.__len__() == 0:
                self._reference_buffer_frame = pd.concat([self._reference_buffer_frame, item[1].to_frame().T], axis=0)

            mean_signal = self._reference_buffer_frame.mean()

            mp_dist = mpdist(item[1].values.astype(float), mean_signal.values, m=len(mean_signal.values),
                             percentage=1.0, normalize=True)

            print('mpdist is:', mp_dist, 'limit is:', mp_dist_limit)

            result_dict = Series({'time': item[0], 'mpdist': mp_dist})
            rawdata_result = DataFrame()
            if mp_dist < mp_dist_limit:
                self._reference_buffer_frame = pd.concat([self._reference_buffer_frame, item[1].to_frame().T], axis=0)
            if aggregate_rawdata:
                rawdata_result = self.aggregate_rawdata(item[1].to_frame().T, rawdata_result, True)
                rawdata_result = rawdata_result.drop(columns={'time'})

            mp_dist_result = Series(result_dict).to_frame()
            mp_dist_result = pd.concat([mp_dist_result, rawdata_result.T], axis=0, ignore_index=True)
            mp_dist_result = mp_dist_result.T

            if len(rawdata_result.columns) > 0:
                mp_dist_result = mp_dist_result.drop(columns={2})
            else:
                nan_dict = {}
                for i in range(2, len(self._final_result_frame.columns) + 1):
                    nan_dict[i] = np.nan
                nan_series = Series(nan_dict)
                mp_dist_result = pd.concat([mp_dist_result, nan_series.to_frame().T], axis=1)

            mp_dist_result = mp_dist_result.reset_index(drop=True)
            mp_dist_result = mp_dist_result.rename(columns={0: 'time', 1: 'mpdist'})

            if len(rawdata_result.columns) > 0:
                counter = 2
                for column in rawdata_result.columns:
                    mp_dist_result = mp_dist_result.rename(columns={counter: column})
                    counter += 1
            else:
                counter = 2
                for column in self._final_result_frame.columns[1:]:
                    mp_dist_result = mp_dist_result.rename(columns={counter: column})
                    counter += 1

            mp_dist_result = mp_dist_result.set_index('time')

            try:
                self._final_result_frame = pd.concat([self._final_result_frame, mp_dist_result])
            except Exception:
                traceback.print_exc()
                pass

            if (self._reference_buffer_frame.values.__len__() >= buffer_size) & (mp_dist < mp_dist_limit):
                head_index = self._reference_buffer_frame.head(1).index
                print('dropping index', head_index)
                self._reference_buffer_frame.drop(index=head_index, inplace=True)

        return self

    def get_final_frame(self) -> DataFrame:
        return self._final_result_frame.T.drop_duplicates().T

    @staticmethod
    def _smooth(array, window_len=11, window_type='hanning'):
        """
        from: https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html
        smooth the data using a window with requested size.

        This method is based on the convolution of a scaled window with the signal.
        The signal is prepared by introducing reflected copies of the signal
        (with the window size) in both ends so that transient parts are minimized
        in the begining and end part of the output signal.

        input:
            array: the input signal
            window_len: the dimension of the smoothing window; should be an odd integer
            window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
                flat window will produce a moving average smoothing.

        output:
            the smoothed signal

        example:

        t=linspace(-2,2,0.1)
        x=sin(t)+randn(len(t))*0.1
        y=smooth(x)

        see also:

        numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
        scipy.signal.lfilter

        TODO: the window parameter could be the window itself if an array instead of a string
        NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of
        just y.
        """

        if array.ndim != 1:
            raise (ValueError, "smooth only accepts 1 dimension arrays.")

        if array.size < window_len:
            raise (ValueError, "Input vector needs to be bigger than window size.")

        if window_len < 3:
            return array

        if not window_type in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
            raise (ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

        s = np.r_[array[window_len - 1:0:-1], array, array[-2:-window_len - 1:-1]]
        # print(len(s))
        if window_type == 'flat':  # moving average
            w = np.ones(window_len, 'd')
        else:
            w = eval('np.' + window_type + '(window_len)')

        y = np.convolve(w / w.sum(), s, mode='valid')
        return y


if __name__ == '__main__':

    buffer_size = 21
    mp_dist_limit = 100.0

    extractor = PulsedDataExtractor(buffer_size, mp_dist_limit)

    lowerDateTime = pd.to_datetime('2022-04-01 12:00:00', utc=False, unit='ns')
    maxDateTime = pd.to_datetime('2022-07-10 12:40:00', utc=False, unit='ns')

    fetch_batch_length = 1 * 60 * 1000000000

    lowerTimeEpoch = lowerDateTime.value
    upperTimeEpoch = lowerTimeEpoch + fetch_batch_length  # batch length = 1 day

    result = DataFrame()
    buffer_frame = DataFrame()

    while lowerTimeEpoch < maxDateTime.value:
        extractor, data = extractor.fetchDataFrameFromInflux('YRT1DT2F', '/rawdata.*/', lowerTimeEpoch, upperTimeEpoch)
        print(data)

        lowerTimeEpoch = upperTimeEpoch
        upperTimeEpoch = lowerTimeEpoch + fetch_batch_length  # batch length = 1 day

        if len(data.values) <= 0:
            continue

        extractor.mpdistance_for_fetch_result(True)

        print('****************** writing output file ****************')
        output_data = extractor.get_final_frame()
        print(output_data.values.__len__(), 'lines')
        output_data.to_parquet('mpdist_with_boxfit_21Samples_20220504-0509.parquet')

    #print(out_frame)
    # out_frame = out_frame.set_index('time')
    #plt.plot(out_frame['mpdist'])
    #plt.show()

