import pandas as pd
from timeit import default_timer


if __name__ == '__main__':
    start = default_timer()
    d1 = pd.read_hdf('output.h5')
    stop1 = default_timer()
    dataFrame = pd.read_parquet('YR08DT1ML_Intensity_2022.parquet')
    stop2 = default_timer()

    print('hdf', stop1 - start)
    print('parquet', stop2 - stop1)
    exit()

    dataFrame = pd.read_parquet('YR08DT1ML_Intensity_2022.parquet')
    dataFrame.to_hdf('output.h5', 'time')