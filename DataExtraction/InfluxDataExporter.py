from influxdb import DataFrameClient

from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
from Framework.FluxStatementBuilder import QueryStatement
from Framework import CommonFunctions as cf
import pandas as pd
from pandas import DataFrame, HDFStore, Series
from datetime import datetime
from matplotlib import pyplot as plt
import scipy.signal as sig
import scipy
from scipy.optimize import differential_evolution, curve_fit
from scipy.signal import cspline1d, cspline1d_eval
import scipy.fft as fft
from scipy.stats import skew
import traceback
import numpy as np


def aggregateRawdata(data_frame: DataFrame, aggregate_frame: DataFrame):
    print('starting to aggregate', end='\r')
    data_frame.dropna()
    data_frame.reset_index(inplace=True)  # remove index
    data_frame.rename({'index': 'time', 'intensity': 'intensity', 'lastInjection': 'lastinjection',
                            'lastExtraction': 'lastextraction'}, axis=1, inplace=True)
    data_frame['time'] = pd.to_datetime(data_frame['time'], dateTimeFormat, utc=True)
    data_frame = data_frame.set_index('time')

    '''
    for result in rawdata_data_frame.values:
        filtered: np.ndarray = smooth(result[20:] - np.mean(result[20:100]), 51)
        x = np.linspace(0, len(filtered), num=len(filtered))
        bounds = [(0, np.max(filtered)), (0, len(filtered)), (100, 10000)]
        res = differential_evolution(lambda p: np.sum((box(x, *p) - filtered) ** 2), bounds=bounds)

        plt.plot(filtered)
        plt.step(x, box(x, res['x'][0], res['x'][1], res['x'][2]), where='mid')
        plt.show()
    '''

    output_frame = DataFrame()

    if aggregate_data:
        # output_frame['baseline'] = rawdata_data_frame.agg(func=calculate_column_median, axis=1)
        print('aggregate sum', end='\r')
        output_frame['rawdata_sum'] = data_frame.agg(func=np.sum, axis=1)
        print('aggregate standard devication', end='\r')
        output_frame['rawdata_std'] = data_frame.agg(func=np.std, axis=1)
        print('aggregate mean', end='\r')
        output_frame['rawdata_mean'] = data_frame.agg(func=np.mean, axis=1)
        print('aggregate median', end='\r')
        output_frame['rawdata_median'] = data_frame.agg(func=np.median, axis=1)
        print('aggregate skew', end='\r')
        output_frame['rawdata_skewness'] = data_frame.agg(func=skew, axis=1)
        print('aggregate fft', end='\r')
        output_frame['rawdata_mean_fftfreq'] = np.mean(fft.fftfreq(output_frame.size, d=1E-8)[:output_frame.size // 2])

        # data_without_baseline = rawdata_data_frame.transpose() - output_frame['baseline'].transpose()
        # data_without_baseline = data_without_baseline.transpose()
        boxfit_data = data_frame.agg(func=do_boxfit, axis=1)

        output_frame = output_frame.join(boxfit_data)
    else:
        output_frame = data_frame


    print('*************** aggregation result **************\n', output_frame)

    aggregate_frame = pd.concat([aggregate_frame, output_frame])

    return aggregate_frame

def box_for_boxfit(data, *offset_slope_height_center_width):
    """
    from https://stackoverflow.com/questions/46624376/problems-fitting-to-boxcar-function-using-scipys-curvefit-in-python
    :param data: array-like data
    :param height: box height
    :param center: box center
    :param width: box width
    :return: box function
    """
    offset, slope, height, center, width = offset_slope_height_center_width
    fit_function = offset + slope * data + height * (center - width / 2 < data) * (data < center + width / 2)
    return fit_function


def calculate_column_median(data):
    calc_data = np.mean(data[20:100])
    return calc_data


def do_boxfit(data):
    res = {'success': False}
    print('boxfitting...', end='\r')

    data_smoothed: np.ndarray = smooth(data[20:], 51)
    smoothed_max = np.max(data_smoothed)
    x = np.linspace(0, len(data_smoothed), num=len(data_smoothed))
    bounds = [((np.mean(data_smoothed[:100]) - np.mean(data_smoothed[:100]) * 0.1), (np.mean(data_smoothed[:100])
            + np.mean(data_smoothed[:100]) * 0.1)), (-10000, 10000), (0, np.max(data_smoothed)), (0,
                                                                                                  len(data_smoothed)), (100,
                                                                                                                 10000)]

    offset = slope = height = center = width = signal_mean = signal_std = signal_skew = 0.0


    try:
        res = differential_evolution(lambda p: np.sum((box_for_boxfit(x, *p) - data_smoothed) ** 2), bounds=bounds)
        offset = res['x'][0]
        slope = res['x'][1]
        height = res['x'][2]
        center = res['x'][3]
        width = res['x'][4]

        signal_lower = int(center - width / 2) + 10 # pad signal to avoid spike at end
        signal_upper = int(center + width / 2) - 10
        signal_data = data[signal_lower:signal_upper]

        linfit = scipy.stats.linregress(x=np.linspace(0, len(signal_data), num=len(signal_data)), y=signal_data)

        signal_mean = np.mean(signal_data)
        signal_std = np.std(signal_data)
        signal_skew = skew(signal_data)

    except Exception as err:
        traceback.print_exception(type(err), err, err.__traceback__)
        pass

    if res['success']:
        out_dict = {}
        out_dict['boxfit_smoothed_max'] = smoothed_max
        out_dict['boxfit_offset'] = offset
        out_dict['boxfit_slope'] = slope
        out_dict['boxfit_height'] = height
        out_dict['boxfit_center'] = center
        out_dict['boxfit_width'] = width
        out_dict['boxfit_signal_mean'] = signal_mean
        out_dict['boxfit_signal_std'] = signal_std
        out_dict['boxfit_signal_skew'] = signal_skew
        try:
            out_dict['boxfit_signal_fit_slope'] = linfit.slope
        except:
            pass
        try:
            out_dict['boxfit_signal_fit_stderr'] = linfit.stderr
        except:
            pass


        # print('box-fit result:', out_dict)

        return Series(out_dict)
    else:
        return Series()

def exportToFlatArray():
    influxConnector = InfluxDbConnector(clientType='HTTP')
    dbClient = influxConnector.initFluxClient()

    queryStatement: str = QueryStatement(icr().productiveDBConfig()).filter_measurement_single(
        deviceName).filter_field_regex(cf.RawdataRegex()).sort().limit(numberOfMeasurements)

    queryResult: DataFrame = influxConnector.executeFluxStatement(queryStatement=queryStatement)

    dataArray = list()
    for dataSet in queryResult.get_points():
        for item in dataSet.values():
            try:
                dataArray.append(float(item))
            except Exception as err:
                print(err)

    outputFile = open('influxData.txt', 'w')
    outputFile.write(str(dataArray))


def exportToCSV(measurement: str, lowerTime: str = None, upperTime: str = None):
    statement = QueryStatement(_productiveDB).filter_measurement_single(measurement)
    if lowerTime is None and upperTime is not None:\
        statement.range(stop_time=upperTime)
    elif lowerTime is not None and upperTime is None:
        statement.range(start_time=lowerTime)
    else:
        statement.range(start_time=lowerTime, stop_time=upperTime)

    influxConnector = InfluxDbConnector(clientType='HTTP')
    dbClient = influxConnector.initFluxClient()
    print('Fetching data')
    result: pd.DataFrame = influxConnector.executeFluxStatement(queryStatement=statement.build_statement())
    print('writing to file')
    result.to_csv(csvFileName)


def fetchDataFrameFromInflux(measurement: str, fieldSelector: str, lowerTimeEpoch: int = None, upperTimeEpoch: int =
None) -> DataFrame:

    whereStatement = None

    if lowerTimeEpoch is not None and upperTimeEpoch is not None:
        whereStatement = "time>" + str(lowerTimeEpoch) + " AND time<" + str(upperTimeEpoch)
        whereForPrint = "time>" + pd.to_datetime(lowerTimeEpoch, utc=False).strftime(dateTimeFormat) + " AND time<" \
                        + pd.to_datetime(upperTimeEpoch, utc=False).strftime(dateTimeFormat)
    elif lowerTimeEpoch is not None and upperTimeEpoch is None:
        whereStatement = "time>" + str(lowerTimeEpoch)
        whereForPrint = "time>" + pd.to_datetime(lowerTimeEpoch, utc=True)

    influxConnector = InfluxDbConnector(clientType='UDP')
    influxConnector.initInfluxQLClient()
    print(datetime.now(), '- Fetching data where', whereForPrint)
    result: pd.DataFrame = influxConnector.fetchDataByInfluxQL(measurement, fieldSelector, whereStatement)
    if result.__len__() > 0:
        # result = result[measurement]
        print('fetched', len(result.values), 'rows.')
        return result
    else:
        print('query did not return data')
        return DataFrame()


def fetchRawdataAsTimeSeries(measurement: str, sampling_frequency: int, lowerTimeEpoch: int = None, upperTimeEpoch:
int = None) -> DataFrame:
    """
    Special fetch method providing functionality to fetch raw-data where each raw-data sample is stored as one 
    column in the database as data organized as time series
    
    :param measurement: measurement ID = device nomen
    :param sampling_frequency: raw data sampling frequency 
    :param lowerTimeEpoch: start timestamp for fetching data
    :param upperTimeEpoch: stop timestamp for fetching data
    :return: pandas.DataFrame with time series data of rawdata pulses
    """

    fieldSelector = '/rawdata.*/'
    # fetch data as inidexed array
    out_data = fetchDataFrameFromInflux(measurement, fieldSelector, lowerTimeEpoch, upperTimeEpoch)
    if out_data is not None:
        out_data = out_data.reset_index()
        out_data = out_data.rename(columns={'index': 'acq_stamp'})
        out_data = out_data.melt('acq_stamp', var_name='rawdata')
        out_data.rawdata = out_data.rawdata.str.extract('(\d+)').astype(int)
        rawdiff_as_ns = out_data.rawdata.sub(out_data.rawdata.max()).abs().mul(100)
        out_data['timestamp'] = out_data.acq_stamp.sub(pd.to_timedelta(rawdiff_as_ns, unit='ns'))
        out_data = out_data.sort_values('timestamp', ignore_index=True)
        out_data = out_data.rename(columns={'rawdata': 'index'})
        out_data = out_data.set_index('timestamp')

        # plt.plot(out_data['value'].values)
        # plt.show()

    return out_data


def smooth(array, window_len=11, window_type='hanning'):
    """
    from: https://scipy-cookbook.readthedocs.io/items/SignalSmooth.html
    smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        array: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if array.ndim != 1:
        raise(ValueError, "smooth only accepts 1 dimension arrays.")

    if array.size < window_len:
        raise(ValueError, "Input vector needs to be bigger than window size.")

    if window_len < 3:
        return array

    if not window_type in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise(ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

    s = np.r_[array[window_len - 1:0:-1], array, array[-2:-window_len - 1:-1]]
    # print(len(s))
    if window_type == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window_type + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='valid')
    return y


if __name__ == '__main__':
    output_file_type = 'parquet'

    deviceName = 'GE02DT_ML' # ESR trafo, data between data between 2022-05-19 and 2022-07-06
    # deviceName = 'GS09DT_ML' # SIS18 trafo, data between 2022-05-19 and 2022-07-06

    lowerDateTime = pd.to_datetime('2022-05-20 04:20:00', utc=False, unit='ns')
    maxDateTime = pd.to_datetime('2022-05-20 05:30:00', utc=False, unit='ns')

    propertySuffix = '_ContinuousAcquisition'
    # fieldName: str = 'intensity'
    fieldSelector = '*'
    # fileNameSuffix = '2022_Part10.csv'
    # numberOfMeasurements = 10
    _productiveDB = icr().getProductiveDB()
    # csvFileName = '_'.join([deviceName, fieldName.capitalize(), fileNameSuffix])
    fetch_batch_length = 30 * 60 * 1000000000 # slice data extraction into batches of nanoseconds
    dateTimeFormat = '%Y-%m-%d %H:%M:%S.%f+00:00'
    aggregate_data = False # special variable for aggegating 'rawdata' channels

    deviceName = deviceName + propertySuffix

    lowerTimeEpoch = lowerDateTime.value
    upperTimeEpoch = lowerTimeEpoch + fetch_batch_length # batch length = 1 day
    maxTimeEpoch = maxDateTime.value

    # ------------------------------------------------
    output_frame: DataFrame = DataFrame()

    while lowerTimeEpoch < maxTimeEpoch:


        # fetch_result: DataFrame = fetchRawdataAsTimeSeries(deviceName, sampling_frequency=1E7,
        #                                                   lowerTimeEpoch=lowerTimeEpoch,
        #                                                   upperTimeEpoch=upperTimeEpoch)

        fetch_result: DataFrame = fetchDataFrameFromInflux(measurement=deviceName, fieldSelector=fieldSelector,
                                                           lowerTimeEpoch=lowerTimeEpoch, upperTimeEpoch=upperTimeEpoch)

        if (fetch_result is not None) & (fetch_result.size > 0):
            if aggregate_data:
                aggregate = DataFrame()
                aggregate = aggregateRawdata(fetch_result, aggregate)
                # append to file
                output_frame = pd.concat([output_frame, aggregate])


        lowerTimeEpoch = upperTimeEpoch
        upperTimeEpoch = lowerTimeEpoch + fetch_batch_length  # batch length = 1 day

    output_file_name = '_'.join([deviceName, str(lowerDateTime).split(' ')[0], str(maxDateTime).split(' ')[0]])
    output_file_name = '.'.join([output_file_name, output_file_type])
    output_frame.to_parquet(output_file_name)
    print('Wrote file:', output_file_name)


