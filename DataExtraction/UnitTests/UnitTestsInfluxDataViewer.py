from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework import CommonFunctions as cf
import unittest
from datetime import datetime

_testMeasurement = 'YR00_GENETIC_SCAN'
_testField = 'iteration'
_lowerTimeString = '2020-06-06T11:41:00.0Z'
_upperTimeString = '2020-06-06T20:10:00.0Z'


class MyTestCase(unittest.TestCase):
    _theDataViewer = InfluxDataExtractor()

    def test_instantiation(self):
        self.assertIsNotNone(self._theDataViewer)

    def test_getDataFromInfluxByTimeRange_AllFields_dateTimeStrings(self):
        self.assertIsNotNone(
            self._theDataViewer.getDataFromInfluxByTimeRange(_testMeasurement, _lowerTimeString, _upperTimeString))

    def test_getDataFromInfluxByTimeRange_AllFields_unixEpochs(self):
        self.assertIsNotNone(
            self._theDataViewer.getDataFromInfluxByTimeRange(_testMeasurement, datetime.strptime(_lowerTimeString,
                                                                                                 cf.InfluxTimeFormatString()).timestamp(), datetime.strptime(_upperTimeString, cf.InfluxTimeFormatString()).timestamp()))

    def test_getDataFromInfluxByTimeRange_OneFieldFields_unixEpochs(self):
        result = self._theDataViewer.getDataFromInfluxByTimeRange(_testMeasurement, _lowerTimeString,
                                                                  _upperTimeString, _testField)
        self.assertIsNotNone(result)
        self.assertTrue(len(result.values) > 0)

    def  test_plotDataInTimeRange_OneFieldFields_unixEpochs(self):
        self._theDataViewer.plotDataInTimeRange(_testMeasurement, _lowerTimeString,
                                                                  _upperTimeString, _testField)

if __name__ == '__main__':
    unittest.main()
