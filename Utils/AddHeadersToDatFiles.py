from Framework import CommonFunctions as cf
from os import listdir
import os
import re

"""
This is a helper file adding headers with deployment unit and device name information to *.dat (archivingDB exports)
"""


def main():
    myPath = os.getcwd() + os.sep
    #             + "archive_files" \
    #             + os.sep

    files = [f for f in listdir(myPath) if os.path.isfile(os.path.join(myPath, f))]

    for dataFileName in files:
        fileObject = open(dataFileName, "r")
        fileContent = fileObject.read()
        fileObject.close()

        myPath = os.getcwd() + os.sep + "archive_files" + os.sep

        regexDeviceName = re.compile(r'deviceName=[\w0-9]+')
        regexResult = None
        deviceName = None

        if regexDeviceName.search(fileContent) is not None:
            regexResult = regexDeviceName.findall(fileContent, 0, 100)
        else:
            deviceName = cf.getDeviceNameFromFileName(dataFileName, 2)
            fileContent = '# deviceName=' + deviceName + '\n' + fileContent

        if regexResult is not None:
            resultItems = regexResult[0].split('=')
            deviceName = resultItems[1]

        regexDeploymentUnit = re.compile(r'deploymentUnit=[\w0-9]+')

        if not regexDeploymentUnit.search(fileContent) is not None:
            fileContent = '# deploymentUnit=' + cf.getDeploymentUnit(deviceName) + '\n' + fileContent
            fileObject = open(myPath + dataFileName, "w")
            fileObject.write(fileContent)
            fileObject.close()


if __name__ == "__main__":
    main()
