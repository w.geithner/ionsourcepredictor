from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from pandas import DataFrame
import pandas as pd
from influxdb_client import WritePrecision
import glob

pathToDaveExports = r'D:\Downloads'


class DaveDataImporter:
    _influxConnection = InfluxDbConnector(clientType='HTTP')

    def __init__(self):
        self._influxConnection.initFluxClient()

    def close(self):
        self._influxConnection.close()

    def importToInfluxDB(self, file_path: str, measurementName: str, time_index_name: str,
                         write_precision: WritePrecision = WritePrecision.NS):
        fileContent: DataFrame = pd.read_csv(file_path)
        fileContent = fileContent.set_index(time_index_name)
        self._influxConnection.writeDataFrame(fileContent, measurement_name=measurementName,
                                              write_precision=write_precision)


if __name__ == '__main__':
    filesInDirectory: list = glob.glob(pathToDaveExports + '\\*YRT1IZ1P*.csv')
    importer = DaveDataImporter()
    for file in filesInDirectory:
        qualifier = file.split('--')[1].split('-')
        measurement_ID = qualifier[0]
        with open(file, 'r') as fileStream:
            time_index = fileStream.readline().split(',')[0]
            timestamp_length = len(fileStream.readline().split(',')[0])

        time_precision: WritePrecision.NS
        if timestamp_length == 13:
            time_precision = WritePrecision.MS
        elif timestamp_length == 16:
            time_precision = WritePrecision.US
        elif timestamp_length == 19:
            time_precision = WritePrecision.NS
        else:
            raise AssertionError

        importer.importToInfluxDB(file_path=file, measurementName=measurement_ID, time_index_name=time_index,
                                  write_precision=time_precision)

    importer.close()
