from influxdb.resultset import ResultSet

from Framework.ActiveMqConnector import ActiveMqConnector
from Framework.InfluxDbConnectorFlux import InfluxDbConnector
import json
from matplotlib import pyplot as plt
import numpy as np
from scipy import signal
from scipy import stats
import pandas as pd
from pandas import DataFrame
import matrixprofile
from datetime import datetime
from datetime import timedelta
import math
import time

dbConnector = InfluxDbConnector('HTTP')
influxClient = dbConnector.initFluxClient()

continuousMode = False
doWriteToDB = True
doSmoothData = False
plotRawDataAndProfile = False
plotIndividualShot = False
plotIndividualShotsAndProfile = False
deviceName = "YRT1DT2F"
testFieldName = 'meancurrent'
arrayFieldName = 'rawdata'
sendRawDataToInflux = True
shotsInAnalysis = 5
averagingWindowSize = 10
lastAcquisitionTime = datetime.now()

meanCurrentArray = None
resamplingGoal = 2000  # resample to goal if required, use none to skip resampling

amqConnector = ActiveMqConnector()
amqConnector.startConnection()

while True:
    outArrayResampled = None
    outArraySmoothed = None
    outArrayMatrixProfile = None
    counter = 0
    startCounter = 0
    mpWindowSize = 0

    formatString = "%Y-%m-%dT%H:%M:%S.%fZ"
    upperDateTime = datetime.utcnow()
    #upperDateTime = pd.to_datetime('2019-12-18T13:07:00')
    upperTimeString = datetime.strftime(upperDateTime, formatString)

    lowerDateTime = upperDateTime - timedelta(seconds=600)
    lowerTimeString = datetime.strftime(lowerDateTime, formatString)

    testTime = lowerDateTime
    if not continuousMode:
        while upperDateTime > testTime:
            statement: str = "SELECT " + testFieldName + " FROM " + deviceName + " ORDER BY time DESC LIMIT " + str(shotsInAnalysis) + ";"
            signalDescriptorDataSet: ResultSet = dbConnector.executeFluxStatement(statement)
            dataSetCounter = 0
            for item in signalDescriptorDataSet.get_points():
                dataSetCounter = dataSetCounter + 1
                timeString: str = item['time'][:-4] + 'Z'
                if dataSetCounter == shotsInAnalysis:
                    try:
                        testTime = datetime.strptime(timeString, formatString)
                    except Exception as err:
                        print(err)
                    print('Waiting for', shotsInAnalysis, 'shots after last round...', upperDateTime, ' > ', testTime)
            time.sleep(1.0 * shotsInAnalysis)
    else:

        statement: str = "SELECT LAST(" + testFieldName + ") FROM " + deviceName + ";"
        testDataSet: DataFrame = dbConnector.executeFluxStatement(statement)
        for item in testDataSet.items():
            timeString: str = item.index()
            try:
                testTime = datetime.strptime(timeString, formatString)
            except Exception as err:
                print(err)
            break

        while lastAcquisitionTime == testTime:
            statement: str = "SELECT LAST(" + testFieldName + ") FROM " + deviceName + ";"
            testDataSet: DataFrame = dbConnector.executeFluxStatement(statement)
            for item in testDataSet.items():
                timeString: str = item['time']
                try:
                    testTime = datetime.strptime(timeString, formatString)
                except Exception as err:
                    print(err)
                break
            print("Waiting for availability of", shotsInAnalysis, "new shots in database.")
            time.sleep(1.0 * shotsInAnalysis)

    # fetch analysis data from influx server: YRT1DT1F_analysis. this are my multi-class training data , use as X-data
    statement: str = "SELECT /" + arrayFieldName + "[0-9]{6}/ FROM " + deviceName + " ORDER BY time DESC LIMIT " \
                     + str(shotsInAnalysis) + ";"
    # + "where time >= '" + lowerTimeString + "' AND  time  <= '" + upperTimeString + "';"\
    print("Fetching data from DB")
    signalDescriptorDataSet: DataFrame = dbConnector.executeFluxStatement(statement)
    print("Done fetching data. Will process now...")

    timestamps = None
    dataArray = None
    averagingWindow = signal.windows.blackmanharris(averagingWindowSize)

    acquisitionTime = None

    itemtimestamps = list()

    for item in signalDescriptorDataSet.items():

        itemtimestamp = item["time"]
        itemtimestamps.append(int(pd.to_datetime(itemtimestamp).timestamp() * 1000000000))

        if acquisitionTime is None:
            acquisitionTime = itemtimestamp
            lastAcquisitionTime = pd.to_datetime(itemtimestamp)
            print('Item timestamp:', acquisitionTime)

        if counter >= startCounter:
            itemValues = list(item.values())
            meanCurrent = itemValues[1]
            if meanCurrentArray is None:
                meanCurrentArray = meanCurrent
            else:
                meanCurrentArray = np.append(meanCurrentArray, [meanCurrent])

            if timestamps is None:
                timestamps = pd.to_datetime(itemValues[0])
            else:
                timestamps = np.append(timestamps, pd.to_datetime(itemValues[0]))

            dataArray = np.array(itemValues[2:], dtype=float)

            # Resample data
            if resamplingGoal is not None:
                dataArrayResampled = signal.resample(dataArray, resamplingGoal)
            else:
                dataArrayResampled = dataArray

            mpWindowSize = len(dataArrayResampled)

            # smooth data
            if doSmoothData:
                dataArraySmoothed = np.convolve(dataArray, averagingWindow)
            else:
                dataArraySmoothed = dataArray

            if outArrayResampled is None:
                outArrayResampled = dataArrayResampled
            else:
                outArrayResampled = np.append(outArrayResampled, [dataArrayResampled])

            rawDataSmoothed = signal.resample(dataArraySmoothed, resamplingGoal)
            if outArraySmoothed is None:
                outArraySmoothed = rawDataSmoothed
            else:
                outArraySmoothed = np.append(outArraySmoothed, [rawDataSmoothed])

            if plotIndividualShotsAndProfile:
                for window in np.arange(143, 2000, 1):
                    plt.close()
                    mtp = matrixprofile.algorithms.mpx(dataArray, window)
                    # mtpSmoothed = matrixprofile.algorithms.mpx(dataArraySmoothed, window)
                    prefix = "shot_" + str(counter) + "_window_" + str(window)
                    fig, axes = plt.subplots(2, 1, constrained_layout=True)
                    fig.suptitle(prefix, fontsize=14)
                    axes[0].plot(dataArray)
                    axes[0].set_ylabel("raw signal [a.u]")
                    axes[1].plot(mtp['mp'])
                    axes[1].set_ylabel("mat. prof.")
                    # axes[2].plot(mtpSmoothed[0])
                    # axes[2].set_ylabel("smoothed mat. prof.")
                    # axes[2].set_xlabel("channel")
                    print("saving: ", prefix, ".png")
                    # plt.savefig(prefix + ".png")
                    plt.show()
        if counter >= startCounter + shotsInAnalysis:
            print("done")
            break

        counter = counter + 1

    print("Number of shots in measurement:", counter)
    print('Number of points in data sample:', len(outArrayResampled))

    if plotIndividualShot:
        plt.close()
        plt.plot(dataArray)
        plt.show()

    if mpWindowSize is not None and outArrayResampled is not None:
        if mpWindowSize * (shotsInAnalysis + 1) >= len(outArrayResampled):
            print("Calculating Matrix Profile. ******************************************")
            try:
                start_time = time.perf_counter()
                matrixProfileData = matrixprofile.algorithms.scrimp_plus_plus(outArrayResampled, mpWindowSize)
                print('Matrix Profile calculation time: ', str(time.perf_counter() - start_time))
                acqnanotime = int(pd.to_datetime(acquisitionTime).timestamp() * 1000000000)
                datananotime = int(datetime.now().timestamp() * 1000000000)
                amqOutput = {"devicename": deviceName, "acqtimestamp": acqnanotime, "datatimestamp": datananotime,
                             "itemtimestamps": itemtimestamps
                    #, "data": [0.0]}
                , "data": (matrixProfileData['mp']).tolist()}

                jsonString: str = str(json.dumps(amqOutput))
                #print("JSON string: ", jsonString)
                print("Sending data to ActiveMQ.")
                start_time = time.perf_counter()
                if amqConnector.isRunning():
                    amqConnector.sendStringMessageToQueue(jsonString)

                statistics = stats.describe(matrixProfileData['mp'])

                if statistics is not None and not math.isnan(statistics.minmax[0]) and not math.isnan(statistics.minmax[1]) and \
                        not math.isnan(statistics.mean) and not math.isnan(statistics.variance and doWriteToDB):
                    influxPoint: str = deviceName + "_MatrixProfile" \
                                    " min=" + str(statistics.minmax[0]) \
                                    + ",max=" + str(statistics.minmax[1]) \
                                    + ",mean=" + str(statistics.mean) \
                                    + ",variance=" + str(statistics.variance) \
                                    + ",skewness=" + str(statistics.skewness) \
                                    + ",kurtosis=" + str(statistics.kurtosis) \
                                    + ",shotcount=" + str(counter)
                    if sendRawDataToInflux:
                        pointCount = 1
                        for value in matrixProfileData['mp']:
                            influxPoint = influxPoint + ",rawdata" + str(pointCount).zfill(6) + '=' + str(value)
                            pointCount = pointCount + 1

                    influxPoint = influxPoint + " " + str(pd.Timestamp(pd.to_datetime(acquisitionTime)).value)

                    print("New influx point: ", influxPoint)

                    dbConnector.doWriteAtomic([influxPoint])
                    print('Time for sending data:', str(time.perf_counter() - start_time))
                else:
                    print("Received NaN values")
            except Exception as ex:
                print('Error occurred during MP calculation: ', ex.with_traceback())

            # for mpWindow in np.arange(1500, 3 * matrixProfileLength, 100):

            plt.close()
            if plotRawDataAndProfile:
                fig, axes = plt.subplots(2, 1, constrained_layout=True)
                fig.suptitle("Window size = " + str(mpWindowSize))
                #axes[0].plot(outArrayResampled)
                axes[0].set_ylabel("mean current")
                axes[0].plot(outArrayResampled)
                axes[0].set_ylabel("raw signal [a.u]")
                print("Calculating matrix profile of resampled data...")
                #out = matrixProfile.scrimp_plus_plus(outArrayResampled, mpWindow, random_state=2)
                axes[1].plot(matrixProfileData['mp'])
                axes[1].set_yscale('log')
                axes[1].set_ylabel("mat. prof. (log Scale)")
                plt.show()
                plt.close()

                plt.hist(matrixProfileData['mp'], bins=46)
                plt.show()

    # time.sleep(20)

exit()