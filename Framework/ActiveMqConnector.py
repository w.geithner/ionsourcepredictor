import enum
from stomp import *  # for connection to activeMQ message broker
from ConfigProvider import ConfigProvider


class ActiveMqQueue(enum.Enum):
    inbound = 1
    error = 2
    test = 3


class ActiveMqConnector:

    _messageBrokerConnection = None
    _configProvider = None

    def __init__(self):
        self._configProvider = ConfigProvider()
        self._messageBrokerConnection = Connection([(self._configProvider.getBroker_Url(),
                                                     self._configProvider.getBroker_Port())])
        self._messageBrokerConnection.start()
        self._messageBrokerConnection.connect(self._configProvider.getBroker_User(),
                                              self._configProvider.getBroker_password(), wait=True)

    def isRunning(self):
        return self._messageBrokerConnection.is_connected()

    def sendStringMessageToQueue(self, stringMessage: str, queue: ActiveMqQueue = ActiveMqQueue.inbound):
        """Method to send a string message to an ActiveMQ.

        Parameters
        __________

        stringMessage : str
            The message text
        queue : str
            parameter allowing to fork message queue. Allowed values:

            - 'logging' = default queue
            - 'error' = queue to send message dumps to activeMQ
        """

        if self._messageBrokerConnection is None:
            self.__init__()

        if self._messageBrokerConnection.is_connected():
            try:
                if queue == ActiveMqQueue.inbound:
                    self._messageBrokerConnection.send(destination=self._configProvider.getBroker_LoggingQueueName(),
                                                       body=stringMessage)
                elif queue == ActiveMqQueue.error:
                    self._messageBrokerConnection.send(destination=self._configProvider.getBroker_ErrorQueueName(),
                                                       body=stringMessage)
                elif queue == ActiveMqQueue.test:
                    self._messageBrokerConnection.send(destination=self._configProvider.getBroker_TestQueueName(),
                                                       body=stringMessage)
            except Exception as ex:
                print(str(ex))
        else:
            raise Exception("Message broker seems to be dead... no connection possible")

    def startConnection(self) -> bool:
        """
        Method starting message broker connection.
        :return: true is connection has been established, else false.
        """
        self._messageBrokerConnection.start()
        if not self._messageBrokerConnection.is_connected():
            self._messageBrokerConnection.connect(self._configProvider.getBroker_User(), self._configProvider.getBroker_password(),
                                                  wait=True)

        return self._messageBrokerConnection.is_connected()

    def stopConnection(self) -> bool:
        """
        Method for stopping the connection to activemq.
        :return: true is queue has been stopped, else false.
        """
        self._messageBrokerConnection.stop()
        self._messageBrokerConnection.disconnect()
        return not self._messageBrokerConnection.is_connected()
