from Framework.ConstantsContainer import ConstantsContainer
import numpy as np
import re
import time
from datetime import datetime


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def getKeyValueFromItem(lineItem: str):
    itemString = lineItem
    itemString = itemString.replace("\n", "")

    key: str = ''
    value = None

    key_value = itemString.split("=")
    if len(key_value) > 1:
        key = key_value[0]
        try:
            value = key_value[1]
            value = getTypedValue(value)
        except IndexError as err:
            print("Error splitting: " + itemString)
            raise err
    else:
        key = key_value[0]
        value = key_value[0]

    timeStampFields = {'acquisitionStamp',
                       'chainStartStamp',
                       'eventStamp',
                       'processStartStamp',
                       'sequenceStartStamp',
                       'timestampAcq',
                       'timestampCycle'}

    # if key == 'timestampAcq':
    #       value = pd.to_datetime(value)

    if key is None or value is None:
        raise ValueError('key was None or value was None')

    return key, value


def RawdataRegex() -> str:
    return '/rawdata[0-9]{6}/'


def InfluxTimeFormatString() -> str:
    return '%Y-%m-%dT%H:%M:%S.%fZ'


def PandasTimeFormatString() -> str:
    return '%Y-%m-%d %H:%M:%S.%f'

def getTypedValue(stringValue: str):
    """
    Method to convert a string into numbers. This is done in steps by testing regular expressions:
    1) integer
    2) float
    3) float with engineering notation
    4) numpy array
    5) if everything fails return the string
    :return: converted value
    """

    intRegex = re.compile('^-?[0-9]+$')
    floatRegex = re.compile('^-?[0-9][0-9,\.]+$')
    engineerRegex = re.compile('^-?[0-9][0-9,\.,e,E,\-,\+]+$')
    arrayRegex = re.compile('\[[\d,\s,\.]+\]$')

    # check for integer
    if intRegex.match(stringValue):
        try:
            return int(stringValue)
        except ValueError as err:
            print("Error occurred: " + str(err))

    # check for float
    elif floatRegex.match(stringValue):
        try:
            return float(stringValue)
        except ValueError as err:
            print("Error occurred: " + str(err))

    # check for engineering notation
    elif engineerRegex.match(stringValue):
        try:
            value = float("{:.15f}".format(float(stringValue)))
            return value
        except  ValueError as err:
            print("Error occurred: " + str(err))
    # check for array
    elif arrayRegex.match(stringValue):
        try:
            stringValue = stringValue.lstrip('[')
            stringValue = stringValue.rstrip(']')
            valueArray = np.fromstring(stringValue, dtype='f', sep=', ')
            return valueArray
        except ValueError as err:
            print("Error occurred: " + str(err))
    else:
        return stringValue


def getLinesInFile(filePath: str, fileName: str) -> int:
    with open(filePath + fileName) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def appendDictToList(listObj: list, dictObj: object):
    if isinstance(dictObj, dict):
        return listObj + [dictObj]
    elif isinstance(dictObj, list):
        return listObj + dictObj


def convertDatetimeToUnixTime(dateTimeString: str):
    convertedTimestamp = None
    if len(dateTimeString) == 8:
        # assume format YYYYMMDD
        convertedTimestamp = time.mktime(datetime.strptime(dateTimeString, "%Y%m%d").timetuple())
    elif len(dateTimeString) == 17:
        # assume format YYYYMMDD hh:mm:ss
        convertedTimestamp = time.mktime(datetime.strptime(dateTimeString, "%Y%m%d %H:%M:%S").timetuple())

    nanoseconds = convertedTimestamp * 1000000000

    return int("{:.0f}".format(nanoseconds))


def convertUnixEpochToInfluxDatetime(unixEpochSeconds: float) -> str:
    utcTime = datetime.utcfromtimestamp(unixEpochSeconds)
    influxTime: str = datetime.strftime(utcTime, InfluxTimeFormatString())
    return influxTime


def getDeviceNameFromFileName(fileName: str, deviceNamePosition: int) -> str:
    fileNameRegex = re.compile("[_.]+")
    splitArray = fileNameRegex.split(fileName)
    return splitArray[deviceNamePosition]


def getDeploymentUnit(deviceName: str) -> str:
    duAssignments = ConstantsContainer().getDeploymentUnitAssignments()
    if deviceName in duAssignments['powerSupply']:
        return 'powerSupply'
    elif deviceName in duAssignments['trafo']:
        return 'trafo'
    elif deviceName in duAssignments['massFlowController']:
        return 'massFlowController'


def getReducedData(inArray: np.array, reductionFactor: float) -> np.array:
    outArray = list()

    if reductionFactor > 1.0:
        binIndexCounter = 0
        binArray = []
        for item in inArray:
            if binIndexCounter >= reductionFactor:
                meanValue = np.mean(binArray)
                outArray.append(meanValue)
                binArray = [item]
                binIndexCounter = 0
            else:
                binArray.append(item)

            binIndexCounter = binIndexCounter + 1
    else:
        outArray = inArray

    return outArray


def main():
    print(convertDatetimeToUnixTime('20190314'))


if __name__ == '__main__':
    main()
