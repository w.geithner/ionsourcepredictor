from kafka import *
import json


class KafkaBrokerConnector():

    _kafkaClient = None
    _kafkaConsumer = None
    _kafkaProducer = None

    def __init__(self):
        self._kafkaClient = KafkaClient(bootstrap_servers='lxyrpc01.gsi.de:9092', client_id='pyKafkaClient')
        self._kafkaConsumer = KafkaConsumer(bootstrap_servers='lxyrpc01.gsi.de:9092', client_id='pyKafkaConsumer',
                                            auto_offset_reset='latest')
        self._kafkaProducer = KafkaProducer(bootstrap_servers='lxyrpc01.gsi.de:9092', client_id='pyKafkaProducer')

    def sendTextMessage(self, topic: str, messageText: str):

        self._kafkaProducer.send(topic=topic, value=messageText.encode())

    def consumeFromBroker(self, topic_name: str):
        self._kafkaConsumer.subscribe([topic_name])
        for message in self._kafkaConsumer:
            print(message, '\n')