from Framework.InfluxDbConnectorFlux import InfluxDbConnector
import pandas as pd


class AbstractInfluxDBReader(object):

    _influxConnector: InfluxDbConnector = InfluxDbConnector('HTTP')

    def __init__(self):
        self._influxConnector.initFluxClient()

    def _executeQuery(self, queryStatement: str, dropDuplicateRows: bool = True):
        result: pd.DataFrame = pd.DataFrame(self._influxConnector.executeFluxStatement(queryStatement).get_points())
        if dropDuplicateRows:
            result = result.drop_duplicates()
        return result
