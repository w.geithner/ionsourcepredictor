import pandas as pd
from Framework.influxDBReaders.AbstractInfluxDBReader import AbstractInfluxDBReader
import Framework.CommonFunctions as cf


class InfluxTrafoDataReader(AbstractInfluxDBReader):

    def __init__(self):
        super().__init__()

    def getSequenceStartStamps(self, measurementID: str) -> pd.DataFrame:
        queryStatement = 'SELECT distinct(sequenceStartStamp) from ' + measurementID

        return self._executeQuery(queryStatement)

    def getRawDataAll(self, groupBySequenceStamp: bool = False):
        if groupBySequenceStamp:
            queryStatement = 'SELECT rawdata, sequenceStartStamp from YRT1DT1F_rawdata GROUP BY sequenceStartStamp'
        else:
            queryStatement = 'SELECT rawdata from YRT1DT1F_rawdata'

        return self._executeQuery(queryStatement)

    def getRawDataBySequenceStartStamp(self, sequenceTimeStamp: str):
        queryStatement = 'SELECT rawdata from YRT1DT1F_rawdata WHERE sequenceStartStamp = ' + \
                         sequenceTimeStamp

        return self._executeQuery(queryStatement)

    def getRawDataByAcqTimeFrame(self, lowerDate: str, lowerTime: str, upperDate: str, upperTime: str):

        lowerString = lowerDate + ' ' + lowerTime
        lowerString = lowerString.replace('-', '') # clean string from '-' separating year, month, ...
        lowerDateTime: int = cf.convertDatetimeToUnixTime(lowerString)

        upperString = upperDate + ' ' + upperTime
        upperString = upperString.replace('-', '') # clean string from '-' separating year, month, ...
        upperDateTime: int = cf.convertDatetimeToUnixTime(upperString)


        queryStatement = 'SELECT rawdata, sequenceStartStamp from YRT1DT1F_rawdata WHERE ' + str(lowerDateTime) \
                         + ' < time < ' + str(upperDateTime)

        return self._executeQuery(queryStatement)

    def getMeanCurrentAll(self):
        queryStatement = 'SELECT meancurrent, sequenceStartStamp, timestampCycle FROM YRT1DT1F_meancurrent'

        return self._executeQuery(queryStatement)

    def getRoiMeanCurrentAll(self):

        queryStatement = 'SELECT roiMeanCurrent, sequenceStartStamp, timestampCycle FROM YRT1DT1F_roimeancurrent'

        return self._executeQuery(queryStatement)

    def getMeanCurrentByAcqTimeFrame(self, lowerDate: str, lowerTime: str, upperDate: str, upperTime: str):
        queryStatement = """SELECT meancurrent, sequenceStartStamp, timestampCycle from YRT1DT1F_meancurrent WHERE time >= '""" + \
                         lowerDate + 'T' + lowerTime + '.0Z' + """' AND time <= '""" + \
                         upperDate + 'T' + upperTime + '.0Z' + """'"""

        return self._executeQuery(queryStatement)
