import pandas as pd
from Framework.influxDBReaders.AbstractInfluxDBReader import AbstractInfluxDBReader


class InfluxDBReader_PowerSupply(AbstractInfluxDBReader):

    _deviceName = None

    def __init__(self, deviceName: str):
        super().__init__()
        self._deviceName = deviceName

    def getCurrentAll(self, test: bool = False) -> pd.DataFrame:

        return self._getFieldValuesAll('current', test=test)

    def getCurrentInAcqTimeFrame(self, lowerDate: str, lowerTime: str, upperDate: str, upperTime: str) -> pd.DataFrame:

        return self._getFieldInAcqTimeFrame('current', lowerDate, lowerTime, upperDate, upperTime)

    def _getFieldValuesAll(self, fieldName: str, test: bool = False):

        measurementSuffix = fieldName.lower()

        queryStatement = 'SELECT ' + \
                         fieldName + \
                         ', sequenceStartTime, timestampCycle FROM ' +\
                         self._deviceName + \
                         '_' + \
                         measurementSuffix

        if test:
            queryStatement = queryStatement + ' LIMIT 10'  # for unittests limit read to 10 lines for perfomance

        return self._executeQuery(queryStatement)

    def _getFieldInAcqTimeFrame(self, fieldName: str, lowerDate: str, lowerTime: str, upperDate: str, upperTime: str):
        measurementSuffix = fieldName.lower()
        queryStatement = 'SELECT ' + fieldName + ', sequenceStartStamp, timestampCycle from ' + \
                         self._deviceName + '_' + measurementSuffix + """ WHERE time >= '""" + \
                         lowerDate + 'T' + lowerTime + '.0Z' + """' AND time <= '""" + \
                         upperDate + 'T' + upperTime + '.0Z' + """'"""

        return self._executeQuery(queryStatement)
