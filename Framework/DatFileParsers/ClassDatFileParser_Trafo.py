from Framework.ConstantsContainer import ConstantsContainer
from Framework.DatFileParsers.ClassAbstractDatFileParser import AbstractDatFileParser
from Framework import CommonFunctions as cf
import pandas as pd
import datetime


class DatFileParser_Trafo(AbstractDatFileParser, ConstantsContainer):

    def __init__(self, deviceName, serverConfig: str, testMode=False):
        super().__init__(deviceName=deviceName, testMode=testMode, serverConfig=serverConfig)
        self._deviceName = deviceName
        self._testMode = testMode
        self._parseResult = None

    def parseFile(self, filePath: str, fileName: str, rawDataResamplingFactor: float):
        """
        Method to parse a complete dat file at once
        :param filePath: path to dat file
        :param fileName: name of the .dat file
        :param rawDataResamplingFactor: a compression factor causing averaging over n channel of the raw data
        :return:
        """

        print('Resampling factor: ' + str(rawDataResamplingFactor))

        totalLineCount = cf.getLinesInFile(filePath + fileName)

        print(fileName + " - Lines to process: " + str(totalLineCount - 2), end='\r')

        fileObject = open(fileName, "r")
        lineCounter = 1
        cf.printProgressBar(0, totalLineCount, prefix='Progress:', suffix='Complete', length=50)

        dataFileLine = fileObject.readline()

        # Loop over entries of data file
        chargeList = list()
        maxCurrentList = list()
        meanCurrentList = list()
        particlesList = list()
        rawDataList = list()
        roiChargeList = list()
        roiMaxCurrentList = list()
        roiMeanCurrentList = list()
        roiParticlesList = list()

        startTime = datetime.datetime.now()

        while dataFileLine:

            if dataFileLine.startswith('#'):
                dataFileLine = fileObject.readline()
                continue
            else:
                super().updateProgressBar(startTime, lineCounter, totalLineCount, fileName)

            if self._testMode and lineCounter > 10:
                # in test mode (e.g. for unit tests, read only first 10 lines of data
                break

            parsedLineDataList = self.parseLine(dataFileLine,
                                                self._deviceName,
                                                fileName=fileName,
                                                rawDataResamplingFactor=rawDataResamplingFactor,
                                                lineNumber=lineCounter)

            if len(parsedLineDataList) > 0:
                for parsedDataDict in parsedLineDataList.items():
                    if parsedDataDict[0] == 'charge':
                        chargeList = cf.appendDictToList(chargeList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'maxCurrent':
                        maxCurrentList = cf.appendDictToList(maxCurrentList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'meanCurrent':
                        meanCurrentList = cf.appendDictToList(meanCurrentList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'particles':
                        particlesList = particlesList + cf.appendDictToList(particlesList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'rawdata':
                        rawDataList = cf.appendDictToList(rawDataList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'roiCharge':
                        roiChargeList = cf.appendDictToList(roiChargeList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'roiMaxCurrent':
                        roiMaxCurrentList = cf.appendDictToList(roiMaxCurrentList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'roiMeanCurrent':
                        roiMeanCurrentList = cf.appendDictToList(roiMeanCurrentList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'roiParticles':
                        roiParticlesList = cf.appendDictToList(roiParticlesList, parsedDataDict[1])

            lineCounter = lineCounter + 1
            dataFileLine = fileObject.readline()

        fileObject.close()

        if len(chargeList) > 0:
            chargeFrame = pd.DataFrame().from_dict(chargeList)
            maxCurrentFrame = pd.DataFrame().from_dict(maxCurrentList)
            meanCurrentFrame = pd.DataFrame().from_dict(meanCurrentList)
            particlesFrame = pd.DataFrame().from_dict(particlesList)
            rawDataFrame = pd.DataFrame().from_dict(rawDataList)
            roiChargeFrame = pd.DataFrame().from_dict(roiChargeList)
            roiMaxCurrentFrame = pd.DataFrame().from_dict(roiMaxCurrentList)
            roiMeanCurrentFrame = pd.DataFrame().from_dict(roiMeanCurrentList)
            roiParticlesFrame = pd.DataFrame().from_dict(roiParticlesList)

            return {'charge': chargeFrame
                    , 'maxCurrent': maxCurrentFrame
                    , 'meanCurrent': meanCurrentFrame
                    , 'particles': particlesFrame
                    , 'rawdata': rawDataFrame
                    , 'roiCharge': roiChargeFrame
                    , 'roiMaxCurrent': roiMaxCurrentFrame
                    , 'roiMeanCurrent': roiMeanCurrentFrame
                    , 'roiParticles': roiParticlesFrame
                    }
        else:
            return {}

    def parseLine(self, datFileLine: str, deviceName: str, fileName: str, rawDataResamplingFactor: float, lineNumber:
    int, storageMode: str = None):
        """
        
        :param datFileLine: 
        :param deviceName: 
        :param fileName: 
        :param rawDataResamplingFactor: 
        :param lineNumber: 
        :param storageMode: 
        :return: 
        """

        recordItems = str(datFileLine).split(";")

        trafoDict = dict()
        trafoDict['deviceName'] = deviceName
        trafoDict['measurement'] = deviceName
        trafoDict['filename'] = fileName
        trafoDict['linenumber'] = lineNumber

        chargeDict = dict()
        chargeDict['deviceName'] = deviceName
        chargeDict['measurement'] = deviceName + '_totalcharge'
        chargeDict['filename'] = fileName
        chargeDict['linenumber'] = lineNumber

        maxCurrentDict = dict()
        maxCurrentDict['deviceName'] = deviceName
        maxCurrentDict['measurement'] = deviceName + '_maxcurrent'
        maxCurrentDict['filename'] = fileName
        maxCurrentDict['linenumber'] = lineNumber

        meanCurrentDict = dict()
        meanCurrentDict['deviceName'] = deviceName
        meanCurrentDict['measurement'] = deviceName + '_meancurrent'
        meanCurrentDict['filename'] = fileName
        meanCurrentDict['linenumber'] = lineNumber

        particlesDict = dict()
        particlesDict['deviceName'] = deviceName
        particlesDict['measurement'] = deviceName + '_particles'
        particlesDict['filename'] = fileName
        particlesDict['linenumber'] = lineNumber

        # raw data are treated differently
        rawDataDict = dict()
        rawDataDict['deviceName'] = deviceName + '_rawdata'

        rawDataColsDict = dict()
        rawDataColsDict['deviceName'] = deviceName

        roiChargeDict = dict()
        roiChargeDict['deviceName'] = deviceName
        roiChargeDict['measurement'] = deviceName + '_roicharge'
        roiChargeDict['filename'] = fileName
        roiChargeDict['linenumber'] = lineNumber

        roiMaxCurrentDict = dict()
        roiMaxCurrentDict['deviceName'] = deviceName
        roiMaxCurrentDict['measurement'] = deviceName + '_roimaxcurrent'
        roiMaxCurrentDict['filename'] = fileName
        roiMaxCurrentDict['linenumber'] = lineNumber

        roiMeanCurrentDict = dict()
        roiMeanCurrentDict['deviceName'] = deviceName
        roiMeanCurrentDict['measurement'] = deviceName + '_roimeancurrent'
        roiMeanCurrentDict['filename'] = fileName
        roiMeanCurrentDict['linenumber'] = lineNumber

        roiParticlesDict = dict()
        roiParticlesDict['deviceName'] = deviceName
        roiParticlesDict['measurement'] = deviceName + '_roiparticles'
        roiParticlesDict['filename'] = fileName
        roiParticlesDict['linenumber'] = lineNumber

        # populate data frames for different measurements: loop over entries in one row separated in the file by
        # semicolons
        for item in recordItems:
            try:
                key, value = cf.getKeyValueFromItem(item)
            except Exception as err:
                print("Conversion failed with error: " + str(err))
                continue

            trafoDict[key] = value

            # first, do not perform special treatment of rawData arrays (store them as numpy.array)
            if key in self._fieldsCharge:
                chargeDict[key] = value
            elif key in self._fieldsMaxCurrent:
                maxCurrentDict[key] = value
            elif key in self._fieldsMeanCurrent:
                meanCurrentDict[key] = value
            elif key in self._fieldsParticles:
                particlesDict[key] = value
            elif key in self._fieldsRawData:
                rawDataDict[key] = value
            elif key in self._fieldsRawDataCols:
                rawDataDict[key] = value
            elif key in self._fieldsRoiCharge:
                roiChargeDict[key] = value
            elif key in self._fieldsRoiMeanCurrent:
                roiMeanCurrentDict[key] = value
            elif key in self._fieldsRoiMaxCurrent:
                roiMaxCurrentDict[key] = value
            elif key in self._fieldsRoiParticles:
                roiParticlesDict[key] = value
            elif key in self._fieldsGeneric:
                chargeDict[key] = value
                maxCurrentDict[key] = value
                meanCurrentDict[key] = value
                particlesDict[key] = value
                rawDataDict[key] = value
                roiChargeDict[key] = value
                roiMeanCurrentDict[key] = value
                roiMaxCurrentDict[key] = value
                roiParticlesDict[key] = value

            if key == 'actualMarkers':
                i = 1
                for valueItem in value:
                    dictKey = 'actualmarkers' + str(i).zfill(6)
                    trafoDict[dictKey] = valueItem
                    i = i + 1
            if key == 'roiFromEvents':
                roiChargeDict[key] = value
                roiMeanCurrentDict[key] = value
                roiMaxCurrentDict[key] = value
                roiParticlesDict[key] = value

        rawDataDicts = dict()
        if storageMode == 'r' or storageMode == 'rc':
            measurement = deviceName + '_rawdata'
            rawDataDicts = DatFileParser_Trafo.getRawDataDicts(rawDataDict, measurement, fileName, lineNumber,
                                                               rawDataResamplingFactor, storageMode='r')
            rawDataDicts = rawDataDicts['rawdatarows']

        rawDataColsDicts = dict()
        if storageMode == 'c' or storageMode == 'rc':
            measurement = deviceName + '_rawdatacols'
            rawDataColsDicts = DatFileParser_Trafo.getRawDataDicts(rawDataDict, measurement, fileName, lineNumber,
                                                               resamplingFactor=rawDataResamplingFactor,
                                                               storageMode='c')

            trafoDict['rawData'] = rawDataColsDicts['rawdatacols']['rawdata']

        # append dictionaries to output dictionary
        output = {
            'trafo': trafoDict
            , 'charge': chargeDict
            , 'maxCurrent': maxCurrentDict
            , 'meanCurrent': meanCurrentDict
            , 'particles': particlesDict
            , 'rawdata': rawDataDicts
            , 'rawdatacols': rawDataColsDicts
            , 'roiCharge': roiChargeDict
            , 'roiMaxCurrent': roiMaxCurrentDict
            , 'roiMeanCurrent': roiMeanCurrentDict
            , 'roiParticles': roiParticlesDict}

        self._parseResult = output

        return output

    @staticmethod
    def getRawDataDicts(initialDict: dict, measurement: str, fileName: str, lineNumber: int,
                        resamplingFactor: float = 1.0, storageMode: str = None) -> dict:
        """
        Method for rawData items with rawData stored as array. Splits up the array into one dictionary per array item

        :param initialDict:
        :param measurement:
        :param fileName:
        :param lineNumber:
        :param resamplingFactor:
        :param storageMode:
        :return:
        """
        rawData = initialDict['rawData']
        timeStep = 1e9 / initialDict['frequency']

        rawDataResampled = cf.getReducedData(rawData, resamplingFactor)

        outputDicts = dict()
        if storageMode == 'r' or storageMode == 'rc':
            rawDataList = list()
            for rawDataItem in rawDataResampled:
                # create one dictionary per raw data entry
                if outputDicts is not None and len(outputDicts) > 0:
                    nanos = int(timeStep * len(outputDicts))
                else:
                    nanos = 0
                newDict = initialDict.copy()
                del newDict['rawData']
                newTimeStamp = initialDict['timestampAcq'] + nanos
                newDict['timestampAcq'] = newTimeStamp
                newDict['rawdata'] = rawDataItem
                newDict['measurement'] = measurement
                newDict['filename'] = fileName
                newDict['linenumber'] = lineNumber
                rawDataList.append(newDict)
            outputDicts['rawdatarows'] = rawDataList

        if storageMode == 'c' or storageMode == 'rc':
            newDict = initialDict.copy()
            del newDict['rawData']
            newDict['measurement'] = measurement
            newDict['filename'] = fileName
            newDict['linenumber'] = lineNumber
            newDict['rawdata'] = rawDataResampled

            outputDicts['rawdatacols'] = newDict

        return outputDicts
