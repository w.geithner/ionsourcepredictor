from Framework.DatFileParsers.ClassDatFileParser_Trafo import DatFileParser_Trafo


class ParserFactory(object):

    @staticmethod
    def getParser(fileName, testMode=False):
        fileObject = open(fileName, "r")

        dataFileLine = fileObject.readline()

        deploymentUnit = None
        deviceName = None

        while dataFileLine:
            # this is to read the header lines, indicated by '#'
            if not dataFileLine.startswith('#'):
                break

            headerToken = dataFileLine.split('=')
            if headerToken[0].find('deploymentUnit') > -1:
                deploymentUnit = headerToken[1]
            elif headerToken[0].find('deviceName') > -1:
                deviceName = headerToken[1]
            else:
                continue

            dataFileLine = fileObject.readline()

        fileObject.close()

        if deploymentUnit is None:
            raise ValueError('Variable ''deploymentUnit'' was None for file ' + fileName
                             + '. Does this file have a properly formed header?')
        if deviceName is None:
            raise ValueError('Variable ''deviceName'' was None for file ' + fileName
                             + '. Does this file have a properly formed header?')

        if deploymentUnit.find('Trafo2') > -1:
            return DatFileParser_Trafo(deviceName, testMode)
        elif deploymentUnit.find('PowerSupply') > -1:
            doNothing = True
        else:
            raise ValueError('Unsupported deployment unit type ' + deploymentUnit)