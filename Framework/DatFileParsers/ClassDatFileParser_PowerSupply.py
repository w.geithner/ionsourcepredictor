from Framework.ConstantsContainer import ConstantsContainer
from Framework.DatFileParsers.ClassAbstractDatFileParser import AbstractDatFileParser
from Framework.ListToInfluxStringParser import ListToInfluxStringParser as lisp
from Framework import CommonFunctions as cf
import datetime

class DatFileParser_PowerSupply(AbstractDatFileParser, ConstantsContainer):

    def __init__(self, deviceName, serverConfig: str, testMode: bool = False):
        super().__init__(deviceName=deviceName, testMode=testMode, serverConfig=serverConfig)

    def parseFile(self, filePath: str, fileName: str) -> list:
        """
        Method to parse a complete dat file at once
        :param fileName: name of the .dat file
        :return: a dictionary of dictionaries of data sets related to measurement contexts
        """

        totalLineCount = cf.getLinesInFile(filePath, fileName)

        print(fileName + " - Lines to process: " + str(totalLineCount - 2), end='\r')

        fileObject = open(filePath + fileName, "r")
        lineCounter = 1
        cf.printProgressBar(0, totalLineCount, prefix='Progress:', suffix='Complete', length=50)

        dataFileLine = fileObject.readline()

        # Loop over entries of data file
        currentList = list()
        voltageList = list()
        powerSupplyList = list()

        startTime = datetime.datetime.now()

        while dataFileLine:

            if dataFileLine.startswith('#'):
                dataFileLine = fileObject.readline()
                continue
            else:
                super().updateProgressBar(startTime, lineCounter, totalLineCount, fileName)

            if self._testMode and lineCounter > 10:
                # in test mode (e.g. for unit tests, read only first 10 lines of data
                break

            parsedLineDataList = self.parseLine(dataFileLine, self._deviceName, lineNumber=lineCounter,
                                                fileName=fileName)

            if len(parsedLineDataList) > 0:
                for parsedDataDict in parsedLineDataList.items():
                    if parsedDataDict[0] == 'current':
                        currentList = cf.appendDictToList(currentList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'voltage':
                        voltageList = cf.appendDictToList(voltageList, parsedDataDict[1])
                    elif parsedDataDict[0] == 'powerSupply':
                        powerSupplyList = cf.appendDictToList(powerSupplyList, parsedDataDict[1])

            lineCounter = lineCounter + 1
            dataFileLine = fileObject.readline()

        fileObject.close()

        outputList = list()

        if len(currentList) > 0:
            outputList.extend(lisp.parseListToInfluxString(currentList))
        if len(voltageList) > 0:
            outputList.extend(lisp.parseListToInfluxString(voltageList))
        if len(powerSupplyList) > 0:
            outputList.extend(lisp.parseListToInfluxString(powerSupplyList))

        return outputList

    def parseLine(self, datFileLine, deviceName: str, fileName: str, lineNumber: int) -> dict:
        """
        Method for parsing an individual line

        :param datFileLine:
        :param deviceName:
        :param fileName:
        :return:
        """
        recordItems = str(datFileLine).split(";")

        measurementID = deviceName

        if super().checkLineExistsInDb(datFileLine, fileName, measurementID):
            # if record exists in database, return empty dict
            return {}

        powerSupplyDict = dict()
        powerSupplyDict['deviceName'] = deviceName
        powerSupplyDict['measurement'] = measurementID
        powerSupplyDict['filename'] = fileName
        powerSupplyDict['linenumber'] = lineNumber

        measurementID = deviceName + ConstantsContainer.suffixCurrent

        currentDict = dict()
        currentDict['deviceName'] = deviceName
        currentDict['measurement'] = measurementID
        currentDict['filename'] = fileName
        currentDict['linenumber'] = lineNumber

        measurementID = deviceName + ConstantsContainer.suffixVoltage

        voltageDict = dict()
        voltageDict['deviceName'] = deviceName
        voltageDict['measurement'] = measurementID
        voltageDict['filename'] = fileName
        voltageDict['linenumber'] = lineNumber

        # populate data frames for different measurements: loop over entries in one row separated in the file by
        # semicolons
        for item in recordItems:

            if item.find('FAIR.SELECTOR.') > -1:
                result = item.partition('FAIR.SELECTOR.')
                selectorItems = result[2].split(':')
                if selectorItems[0] == 'ALL':
                    #currentDict['SELECTOR'] = selectorItems[0]
                    #voltageDict['SELECTOR'] = selectorItems[0]
                    powerSupplyDict['SELECTOR'] = selectorItems[0]

                else:
                    for selectorItem in selectorItems:
                        selectorItem_keyvalue = selectorItem.split('=')
                        key = 'SELECTOR_' + selectorItem_keyvalue[0]
                        value = selectorItem_keyvalue[1]
                        #currentDict[key] = value
                        #voltageDict[key] = value
                        powerSupplyDict[key] = value
            else:
                try:
                    key, value = cf.getKeyValueFromItem(item)
                except Exception as err:
                    print("Conversion failed with error: " + str(err))
                    continue

                # first, do not perform special treatment of rawData arrays (store them as numpy.array)
                if key in self._fieldsCurrent:
                    #currentDict[key] = value
                    powerSupplyDict[key] = value
                elif key in self._fieldsVoltage:
                    #voltageDict[key] = value
                    powerSupplyDict[key] = value
                elif key in self._fieldsGeneric:
                    #currentDict[key] = value
                    #voltageDict[key] = value
                    powerSupplyDict[key] = value

        # append dictionaries to output dictionary
        output = {
            'powerSupply': powerSupplyDict
            #'current': currentDict,
            #'voltage': voltageDict
            }

        return output