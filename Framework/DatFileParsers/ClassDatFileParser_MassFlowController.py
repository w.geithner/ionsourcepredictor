from Framework.ConstantsContainer import ConstantsContainer
from Framework.DatFileParsers.ClassAbstractDatFileParser import AbstractDatFileParser
from Framework.ListToInfluxStringParser import ListToInfluxStringParser as lisp
from Framework import CommonFunctions as cf
import datetime


class DatFileParser_MassFlowController(AbstractDatFileParser, ConstantsContainer):

    def __init__(self, deviceName, serverConfig: str, testMode: bool = False):
        super().__init__(deviceName=deviceName, serverConfig=serverConfig, testMode=testMode)

    def parseFile(self, filePath: str, fileName: str) -> list:
        """
        Method to parse a complete dat file at once
        :param fileName: name of the .dat file
        :return: a dictionary of dictionaries of data sets related to measurement contexts
        """

        totalLineCount = cf.getLinesInFile(filePath, fileName)

        print(fileName + " - Lines to process: " + str(totalLineCount - 2), end='\r')

        fileObject = open(filePath + fileName, "r")
        lineCounter = 1
        # cf.printProgressBar(0, totalLineCount, prefix='Progress:', suffix=fileName + ' - Complete', length=100)

        dataFileLine = fileObject.readline()

        # Loop over entries of data file
        flowList = list()

        startTime = datetime.datetime.now()

        while dataFileLine:

            if dataFileLine.startswith('#'):
                dataFileLine = fileObject.readline()
                continue
            else:
                super().updateProgressBar(startTime, lineCounter, totalLineCount, fileName)

            if self._testMode and lineCounter > 10:
                # in test mode (e.g. for unit tests, read only first 10 lines of data
                break

            parsedLineDataList = self.parseLine(dataFileLine, self._deviceName, lineNumber=lineCounter, fileName=fileName)

            for parsedDataDict in parsedLineDataList.items():
                if parsedDataDict[0] == 'flow':
                    flowList = cf.appendDictToList(flowList, parsedDataDict[1])


            dataFileLine = fileObject.readline()
            lineCounter = lineCounter + 1

        fileObject.close()

        if len(flowList) > 0:
            return lisp.parseListToInfluxString(flowList)

    def parseLine(self, datFileLine, deviceName: str, lineNumber: int, fileName: str) -> dict:
        """

        :param datFileLine:
        :param deviceName:
        :param lineNumber:
        :param fileName:
        :return:
        """

        recordItems = str(datFileLine).split(";")

        flowDict = dict()
        flowDict['deviceName'] = deviceName

        # populate data frames for different measurements: loop over entries in one row separated in the file by
        # semicolons
        for item in recordItems:
            if item.find('FAIR.SELECTOR.') > -1:
                result = item.partition('FAIR.SELECTOR.')
                selectorItems = result[2].split(':')
                if selectorItems[0] == 'ALL':
                    flowDict['SELECTOR'] = selectorItems[0]
                else:
                    for selectorItem in selectorItems:
                        selectorItem_keyvalue = selectorItem.split('=')
                        key = 'SELECTOR_' + selectorItem_keyvalue[0]
                        value = selectorItem_keyvalue[1]
                        flowDict[key] = value
            else:
                try:
                    key, value = cf.getKeyValueFromItem(item)
                except Exception as err:
                    print("Conversion failed with error: " + str(err))
                    continue

                # first, do not perform special treatment of rawData arrays (store them as numpy.array)
                if key in self._fieldsFlow:
                    flowDict[key] = value
                elif key in self._fieldsGeneric:
                    if key == 'timestampAcq':
                        flowDict[key] = value
                    else:
                        flowDict[key] = value

        # add one column with measurement-id
        flowDict['measurement'] = deviceName + '_flow'
        flowDict['filename'] = fileName
        flowDict['linenumber'] = lineNumber

        # append dictionaries to output dictionary
        output = {
            'flow': flowDict
            }

        return output