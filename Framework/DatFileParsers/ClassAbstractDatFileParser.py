from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework import CommonFunctions as cf
import datetime

class AbstractDatFileParser(object):

    _deviceName = None
    _testMode = False
    _serverConfig = None

    def __init__(self, deviceName, serverConfig: str, testMode: bool=False):
        self._deviceName = deviceName
        self._testMode = testMode
        self._serverConfig = serverConfig

    def checkLineExistsInDb(self, datafileLine: str, fileName: str, measurementID: str):
        splitArray = datafileLine.split(';')

        acquisitionStamp = None

        for item in splitArray:
            if item.startswith('acquisitionStamp='):
                acquisitionStamp = item.split('=')[1]
                break

        return InfluxDbConnector('HTTP', serverConfig=self._serverConfig).checkIfRecordExistsInDb(measurementID,
                                                                                                 fileName,
                                                                                 acquisitionStamp)

    @staticmethod
    def updateProgressBar(startTime, lineCounter: int, totalLineCount: int, datFileName: str):
        diffTime = datetime.datetime.now() - startTime
        timePerStep = (diffTime.seconds * 1e6 + diffTime.microseconds) / lineCounter
        timeToEnd = timePerStep * (totalLineCount - lineCounter - 2)  # "- 2" for two header lines
        timeToEnd = datetime.datetime.now() + datetime.timedelta(microseconds=timeToEnd)
        progressSuffix = 'Completed, file ' + datFileName + ', line ' + str(lineCounter) + ' of ' \
                         + str(totalLineCount - 2) + ', end time: ' + str(timeToEnd)
        cf.printProgressBar(lineCounter, totalLineCount - 2, prefix='Progress:', suffix=progressSuffix, length=100)
