class QueryStatement:

    _filter_prefix = ' |> filter(fn:(r) => r.'
    _queryStatement: str = ''

    def __init__(self, bucket_name: str):
        self._queryStatement = 'from(bucket: "' + bucket_name + '")'

    def __add_to_statement__(self, string_to_add):
        self._queryStatement = self._queryStatement + string_to_add
        return self

    def count(self, column: str = '_value'):
        """
        Method adding "count" part to statement (equiv. to SELECT COUNT(...))
        :param column: provide column name other than "_value"
        :return: QueryStatement
        """
        self.__add_to_statement__(' |> count(column: ' + column + ')')
        return self

    def filter_field_single(self, field_id: str):
        self.__add_to_statement__(self._filter_prefix + '_field == "' + field_id + '")')
        return self

    def filter_field_regex(self, field_regex):
        self.__add_to_statement__(self._filter_prefix + '_field =~ ' + field_regex + ')')
        return self

    def filter_field_multiple(self, field_list: list):
        self.__add_to_statement__(self._filter_prefix + '_field == "' + field_list[0] + '"')
        if len(field_list) > 1:
            for field_id in field_list[1:]:
                self.__add_to_statement__(' or r._field == "' + field_id + '"')
        self.__add_to_statement__(')')
        return self

    def filter_measurement_single(self, measurement_id: str):
        self.__add_to_statement__(self._filter_prefix + '_measurement == "' + measurement_id + '")')
        return self

    def filter_measurement_regex(self, measurement_regex: str):
        self.__add_to_statement__(self._filter_prefix + '_measurement =~ ' + measurement_regex + '")')
        return self

    def filter_by_value(self, value_comparator: str):
        """
        Filter part for the comparison of values. You have to pass as well the comparison method ("==", "<", ">",
        ">=", "<=", "!=", "=~") together with the comparison value
        :param value_comparator:
        :return: instance of QueryStatement
        """
        self.__add_to_statement__(self._filter_prefix + '_value ' + value_comparator + '")')
        return self

    def first(self):
        self.__add_to_statement__(' |> first()')
        return self

    def build_statement(self):
        return self._queryStatement

    def last(self):
        self.__add_to_statement__(' |> last()')
        return self

    def limit(self, limit_count: int = 1):
        self.__add_to_statement__(' |> limit(n: ' + str(limit_count) + ')')
        return self

    def max(self, column: str = '_value'):
        """
        Method adding "max.." part to statement
        :param column: provide column name other than "_value"
        :return: QueryStatement
        """
        self.__add_to_statement__(' |> max(column: ' + column + ')')
        return self

    def range(self, start_time: str = '2018-01-01T00:00:00.0Z', stop_time: str = None):
        """
        appender for range part in flux statement
        :param start_time: string containing query start time. Has to be one of
        * RFC3339 timestamp: YYYY-MM-DD or YYYY-MM-DDThh:mm:ss.fZ
        * UNIX epoch timestamp in nanosecond precision
        * relative time to now. E.g.  -10h, -20m, etc
        Default start_time is Jan 2018, as 2018 InfluxDB project was started.
        :param stop_time: same as start_time, but optional
        :return: flux statement string
        """
        if stop_time is None:
            self.__add_to_statement__(' |> range(start: ' + start_time + ')')
        else:
            self.__add_to_statement__(' |> range(start: ' + start_time + ', stop: ' + stop_time + ')')

        return self

    def sort(self, sort_direction: str = 'DESC', sort_by_columns: list = None):
        if sort_by_columns is None and sort_direction == 'DESC':
            self.__add_to_statement__(' |> sort(desc: true)')
        elif sort_by_columns is None and sort_direction == 'ASC':
            self.__add_to_statement__(' |> sort(desc: false)')
        elif sort_by_columns is not None and sort_direction == 'DESC':
            self.__add_to_statement__(' |> sort(columns:[')
            for column in sort_by_columns:
                self.__add_to_statement__('"' + column + '", ')
            self.build_statement().removesuffix(', ')
            self.__add_to_statement__('], desc: true)')
        elif sort_by_columns is not None and sort_direction == 'ASC':
            self.__add_to_statement__(' |> sort(columns:[')
            for column in sort_by_columns:
                self.__add_to_statement__('"' + column + '", ')
            self._queryStatement = self.build_statement()[: len(self.build_statement()) - 2]
            self.__add_to_statement__('], desc: false)')

        return self