
class ListToInfluxStringParser:
    @staticmethod
    def parseListToInfluxString(inputList: list) -> list:

        output: list = list()

        for item  in inputList:

            if len(item) == 0:
                continue

            measurementID: str = item['measurement']

            if measurementID == 'YRT1DT1F':
                ListToInfluxStringParser.parseTrafoData(item, output)
            elif measurementID == 'YRT1IN1X_all' or measurementID == 'YRT1IN1E_all' or measurementID == \
                    'YRT1IN1M_all' or measurementID == 'YRT1IN1K_all' or measurementID == 'YRT1IN1O_all':
                ListToInfluxStringParser.parsePowerSupplyData(item, output)
            else:
                if 'charge' in item:
                    ListToInfluxStringParser.parseChargeData(item, output)
                elif 'current' in item:
                    ListToInfluxStringParser.parseCurrentData(item, output)
                elif 'flow' in item:
                    ListToInfluxStringParser.parseFlowData(item, output)
                elif 'maxCurrent' in item:
                    ListToInfluxStringParser.parseMaxCurrentData(item, output)
                elif 'meanCurrent' in item:
                    ListToInfluxStringParser.parseMeanCurrentData(item, output)
                if 'particles' in item:
                    ListToInfluxStringParser.parseParticlesData(item, output)
                elif 'rawdata' in item:
                    ListToInfluxStringParser.parseRawData(item, output)
                elif 'rawdatacols' in item:
                    ListToInfluxStringParser.parseRawDataColumnar(item, output)
                elif 'roiCharge' in item:
                    ListToInfluxStringParser.parseRoiChargeData(item, output)
                elif 'roiMaxCurrent' in item:
                    ListToInfluxStringParser.parseRoiMaxCurrentData(item, output)
                if 'roiMeanCurrent' in item:
                    ListToInfluxStringParser.parseRoiMeanCurrentData(item, output)
                if 'roiParticles' in item:
                    ListToInfluxStringParser.parseRoiParticlesData(item, output)
                if 'voltage' in item:
                    ListToInfluxStringParser.parseVoltageData(item, output)

        return output

    @staticmethod
    def parseChargeData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}" NOTE: format of actual markers cause trouble
                                # ********** TAGS ***********
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ********** FIELDS *********
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',charge_units="{charge_units}"'
                                #  ',charge_status={charge_status}'
                                # specific
                                ",charge={charge}"
                                ",linenumber={linenumber}"
                                ',sequenceStartStamp={sequenceStartStamp}'
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , charge=inputDict['charge']
                                        , charge_status=inputDict['charge_status']
                                        , charge_units=inputDict['charge_units']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseCurrentData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ********* TAGS *************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ********* FIELDS ***********
                                # specific
                                ' filename="{filename}"'
                                ",current={current}"
                                ",current_max={current_max}"
                                ",current_min={current_min}"
                                ",currentSet={currentSet}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , current=inputDict['current']
                                        , current_max=inputDict['current_max']
                                        , current_min=inputDict['current_min']
                                        , current_units=inputDict['current_units']
                                        , currentSet=inputDict['currentSet']
                                        , currentSet_units=inputDict['currentSet_units']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseFlowData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # *********** TAGS ***************
                                #',tagFilename="{filename}"'
                                # *********** FIELDS *************
                                ' filename="{filename}"'
                                #  ",flow_status={flow_status}"
                                #  ',flow_units="{flow_units}"'
                                #  ',flowSet_units="{flowSet_units}"'
                                # specific
                                ",flow={flow}"
                                ",flowSet={flowSet}"
                                ",linenumber={linenumber}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , flow=inputDict['flow']
                                        , flow_status=inputDict['flow_status']
                                        , flow_units=inputDict['flow_units']
                                        , flowSet=inputDict['flowSet']
                                        , flowSet_units=inputDict['flowSet_units']
                                        # generic
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseMaxCurrentData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # ************* TAGS ****************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ************* FIELDS ***************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',maxCurrent_status={maxCurrent_status}'
                                #  ',maxCurrent_units="{maxCurrent_units}"'
                                # specific
                                ",maxCurrent={maxCurrent}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , maxCurrent=inputDict['maxCurrent']
                                        , maxCurrent_status=inputDict['maxCurrent_status']
                                        , maxCurrent_units=inputDict['maxCurrent_units']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseMeanCurrentData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # ************ TAGS ***************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ************ FIELDS *************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',meancurrent_status={meancurrent_status}'
                                #  ',meancurrent_units="{meancurrent_units}"'
                                # specific
                                ",meancurrent={meancurrent}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , meancurrent=inputDict['meanCurrent']
                                        , meancurrent_status=inputDict['meanCurrent_status']
                                        , meancurrent_units=inputDict['meanCurrent_units']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseParticlesData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # *********** TAGS **************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # *********** FIELDS ************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',particles_status={particles_status}'
                                # specific
                                ",particles={particles}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , particles=inputDict['particles']
                                        , particles_status=inputDict['particles_status']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parsePowerSupplyData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ********* TAGS **************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ********* FIELDS ************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',voltage_units="{voltage_units}"'
                                #  ',voltageSet_units="{voltageSet_units}"'
                                # specific
                                ",voltage={voltage}"
                                ",voltage_max={voltage_max}"
                                ",voltage_min={voltage_min}"
                                ",voltageSet={voltageSet}"
                                ",current={current}"
                                ",current_max={current_max}"
                                ",current_min={current_min}"
                                ",currentSet={currentSet}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , current=inputDict['current']
                                        , current_max=inputDict['current_max']
                                        , current_min=inputDict['current_min']
                                        , current_units=inputDict['current_units']
                                        , currentSet=inputDict['currentSet']
                                        , currentSet_units=inputDict['currentSet_units']
                                        , voltage=inputDict['voltage']
                                        , voltage_max=inputDict['voltage_max']
                                        , voltage_min=inputDict['voltage_min']
                                        , voltage_units=inputDict['voltage_units']
                                        , voltageSet=inputDict['voltageSet']
                                        , voltageSet_units=inputDict['voltageSet_units']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseRawData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # ************ TAGS ***************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                ',tagFilename="{filename}"'
                                # ************ FIELDS *************
                                #  ",chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ",frequency={frequency}"
                                # specific
                                ",rawdata={rawdata}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , rawdata=inputDict['rawdata']
                                        , frequency=inputDict['frequency']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , linenumber=inputDict['linenumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseRawDataColumnar(inputDict: dict, influxDataString):
        colString = ''
        for i in range(1, len(inputDict['rawdatacols'])):
            colNum = str(i)
            colNum = colNum.zfill(6)
            colString = colString + ',rd' + colNum + '=' + str(inputDict['rawdatacols'][i])

        influxDataString.append("{measurement}"
                                ",tagSsequenceStartStamp={sequenceStartStamp}"
                                ',tagFilename="{filename}"'
                                ' filename="{filename}"'
                                '{rawDataCols}' # Note: ',' suffixing string is part of rawDataCols
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement'],
                                        sequenceStartStamp=inputDict['sequenceStartStamp'],
                                        filename=inputDict['filename'],
                                        rawDataCols=colString,
                                        linenumber=inputDict['linenumber'],
                                        timestamp=inputDict['timestampAcq']
                                        )
                                )

    @staticmethod
    def parseRoiChargeData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # *********** TAGS ***************
                                ",tagSsequenceStartStamp={sequenceStartStamp}"
                                #',tagFilename="{filename}"'
                                # *********** FIELDS *************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',roiCharge_status={roiCharge_status}'
                                #  ",roiCharge_units={roiCharge_units}"
                                #  ',roiFromEvents="{roiFromEvents}"'
                                # specific
                                ",roiCharge={roiCharge}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , roiCharge=inputDict['roiCharge']
                                        , roiCharge_status=inputDict['roiCharge_status']
                                        , roiCharge_units=inputDict['roiCharge_units']
                                        , roiFromEvents=inputDict['roiFromEvents']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseRoiMaxCurrentData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # *********** TAGS *************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # *********** FIELDS ***********
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',roiMaxCurrent_status={roiMaxCurrent_status}'
                                #  ',roiMaxCurrent_units="{roiMaxCurrent_units}"'
                                #  ',roiFromEvents="{roiFromEvents}"'
                                # specific
                                ",roiMaxCurrent={roiMaxCurrent}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , roiMaxCurrent=inputDict['roiMaxCurrent']
                                        , roiMaxCurrent_status=inputDict['roiMaxCurrent_status']
                                        , roiMaxCurrent_units=inputDict['roiMaxCurrent_units']
                                        , roiFromEvents=inputDict['roiFromEvents']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseRoiMeanCurrentData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # ************** TAGS **************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ************** FIELDS ************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',roiMeanCurrentStddev_units="{roiMeanCurrentStddev_units}"'
                                #  ',roiFromEvents="{roiFromEvents}"'
                                #  ',roiMeanCurrent_status={roiMeanCurrent_status}'
                                #  ',roiMeanCurrent_units="{roiMeanCurrent_units}"'
                                #  ",roiMeanCurrentStddev={roiMeanCurrentStddev}"
                                # specific
                                ",roiMeanCurrent={roiMeanCurrent}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , roiMeanCurrent=inputDict['roiMeanCurrent']
                                        , roiMeanCurrent_status=inputDict['roiMeanCurrent_status']
                                        , roiMeanCurrent_units=inputDict['roiMeanCurrent_units']
                                        , roiMeanCurrentStddev=inputDict['roiMeanCurrentStddev']
                                        , roiMeanCurrentStddev_units=inputDict['roiMeanCurrentStddev_units']
                                        , roiFromEvents=inputDict['roiFromEvents']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseRoiParticlesData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ",actualMarkers={actualMarkers}"
                                # ********* TAGS **************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ************ FIELDS ***********
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                # ",timestampCycle={timestampCycle}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',roiParticles_status={roiParticles_status}'
                                #  ',roiFromEvents="{roiFromEvents}"'
                                # specific
                                ",roiParticles={roiParticles}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , roiParticles=inputDict['roiParticles']
                                        , roiParticles_status=inputDict['roiParticles_status']
                                        , roiFromEvents=inputDict['roiFromEvents']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))

    @staticmethod
    def parseTrafoData(inputDict: dict, influxDataString):

        colString = ''
        for i in range(1, len(inputDict['rawData'])):
            colString = colString + ',rawdata' + str(i).zfill(6) + '=' + str(inputDict['rawData'][i])

        influxDataString.append("{measurement}"
                                # generic
                                " acquisitionStamp={acquisitionStamp}"
                                #""",actualMarkers='{actualMarkers}'"""
                                # ********* TAGS **************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                # ',tagFilename="{filename}"'
                                # ************ FIELDS ***********
                                ",chainIndex={chainIndex}"
                                ",chainStartStamp={chainStartStamp}"
                                ",eventNumber={eventNumber}"
                                ",eventStamp={eventStamp}"
                                ',filename="{filename}"'
                                ",processIndex={processIndex}"
                                ",processStartStamp={processStartStamp}"
                                ",sequenceIndex={sequenceIndex}"
                                ",timestampAcq={timestampAcq}"
                                ",timestampCycle={timestampCycle}"
                                ",timingGroupID={timingGroupID}"
                                ",roiParticles_status={roiParticles_status}"
                                #",roiFromEvents={roiFromEvents}"
                                ## specific
                                ",totalcharge={totalcharge}"
                                ",maxcurrent={maxcurrent}"
                                ",meancurrent={meancurrent}"
                                ",particles={particles}"
                                "{rawData}"  # Note: ',' suffixing string is part of rawDataCols
                                ",roiCharge={roiCharge}"
                                ",roiParticles={roiParticles}"
                                ",roiMaxCurrent={roiMaxCurrent}"
                                ",roiMeanCurrent={roiMeanCurrent}"
                                ",roiParticles={roiParticles}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestampAcq}"
                                .format(measurement=inputDict['measurement']
                                        , totalcharge=inputDict['charge']
                                        , maxcurrent=inputDict['maxCurrent']
                                        , meancurrent=inputDict['meanCurrent']
                                        , particles=inputDict['particles']
                                        , rawData=colString
                                        , roiCharge=inputDict['roiCharge']
                                        , roiMaxCurrent=inputDict['roiMaxCurrent']
                                        , roiMeanCurrent=inputDict['roiMeanCurrent']
                                        , roiParticles=inputDict['roiParticles']
                                        , roiParticles_status=inputDict['roiParticles_status']
                                        , roiFromEvents=inputDict['roiFromEvents']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , actualMarkers=inputDict['actualMarkers']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timestampCycle=inputDict['timestampCycle']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']
                                        )
                                )

    @staticmethod
    def parseVoltageData(inputDict: dict, influxDataString):
        influxDataString.append("{measurement}"
                                # generic
                                # ",acquisitionStamp={acquisitionStamp}"
                                # ********* TAGS **************
                                # ',tagSequenceStartStamp={sequenceStartStamp}'
                                #',tagFilename="{filename}"'
                                # ********* FIELDS ************
                                #  " chainIndex={chainIndex}"
                                # ",chainStartStamp={chainStartStamp}"
                                #  ",eventNumber={eventNumber}"
                                # ",eventStamp={eventStamp}"
                                ' filename="{filename}"'
                                #  ",processIndex={processIndex}"
                                # ",processStartStamp={processStartStamp}"
                                #  ",sequenceIndex={sequenceIndex}"
                                # ",timestampAcq={timestampAcq}"
                                #  ",timingGroupID={timingGroupID}"
                                #  ',voltage_units="{voltage_units}"'
                                #  ',voltageSet_units="{voltageSet_units}"'
                                # specific
                                ",voltage={voltage}"
                                ",voltage_max={voltage_max}"
                                ",voltage_min={voltage_min}"
                                ",voltageSet={voltageSet}"
                                ",linenumber={linenumber}"
                                ",sequenceStartStamp={sequenceStartStamp}"
                                " {timestamp}"
                                .format(measurement=inputDict['measurement']
                                        , voltage=inputDict['voltage']
                                        , voltage_max=inputDict['voltage_max']
                                        , voltage_min=inputDict['voltage_min']
                                        , voltage_units=inputDict['voltage_units']
                                        , voltageSet=inputDict['voltageSet']
                                        , voltageSet_units=inputDict['voltageSet_units']
                                        # generic
                                        , acquisitionStamp=inputDict['acquisitionStamp']
                                        , chainIndex=inputDict['chainIndex']
                                        , chainStartStamp=inputDict['chainStartStamp']
                                        , eventNumber=inputDict['eventNumber']
                                        , eventStamp=inputDict['eventStamp']
                                        , filename=inputDict['filename']
                                        , linenumber=inputDict['linenumber']
                                        , processIndex=inputDict['processIndex']
                                        , processStartStamp=inputDict['processStartStamp']
                                        , sequenceIndex=inputDict['sequenceIndex']
                                        , sequenceStartStamp=inputDict['sequenceStartStamp']
                                        , timestampAcq=inputDict['timestampAcq']
                                        , timingGroupID=inputDict['timingGroupID']
                                        , timestamp=inputDict['timestampAcq']))
