import collections
from collections import defaultdict
from enum import Enum
from types import TracebackType

import pandas

from Framework import CommonFunctions as cf, InfluxDbConfigReader as icr
from Framework.FluxStatementBuilder import QueryStatement
from Framework.ConstantsContainer import ConstantsContainer
from Framework import DbQueries as dbq
import influxdb_client
from influxdb_client.client.write_api import ASYNCHRONOUS, SYNCHRONOUS, WritePrecision
from influxdb_client import InfluxDBClient as flux_client
from influxdb import DataFrameClient
import pandas as pd
from pandas import DataFrame
from concurrent.futures import ProcessPoolExecutor
import threading
import datetime
from timeit import default_timer
import sys
import numpy as np


class InfluxDbConnector:
    __influxClient = None
    __productiveBD = None
    __databaseName = None
    __clientType: str = None
    __parallelWriting = None
    __serverAddress: str = None
    __serverUser: str = None
    __serverPassword: str = None
    __organization: str = None
    __token: str = None

    def __init__(self, clientType: str, serverConfig: str = 'productive', parallelWriting: bool = True):
        """
        constructor

        :param clientType: either 'HTML' or 'UDP'
        :param serverConfig: one of the configurations defined in resources/DatabaseConfigs.yml
        :param parallelWriting: write data to influxdb in parallel threads
        """
        self.__clientType = clientType
        self.__setServerDataFromConfig__(serverConfig)
        self.__parallelWriting = parallelWriting

    def checkIfDataIsInDb(self, fileName: str, recordCountFile: int, measurementID: str, lineNumber: int,
                          timestamp: str = None) -> str:

        # First check if file has been imported completely
        if timestamp is None:
            checkFileCompleteStatement = QueryStatement(self.__databaseName) \
                .filter_measurement_single(measurementID) \
                .filter_field_single('linenumber')\
                .filter_by_value('=~/' + fileName + '/') \
                .build_statement()
        else:
            checkFileCompleteStatement = QueryStatement(self.__databaseName) \
                .filter_measurement_single(measurementID) \
                .range(start_time=str(timestamp), stop_time=str(timestamp)) \
                .filter_field_single('linenumber')\
                .filter_by_value('=~/' + fileName + '/') \
                .build_statement()

        queryResult: pd.DataFrame = self.__performDbQuery(checkFileCompleteStatement)

        maxLineNumber = len(queryResult.values)

        if maxLineNumber == recordCountFile:
            return 'FILE'
        if maxLineNumber >= lineNumber:
            return 'LINE'

        return 'NONE'

    def checkIfRecordExistsInDb(self, measurementID: str, fileName: str, acquisitionStamp: str) -> bool:
        """
        Method querying the database if a record exists based on measurement ID, file name and acquisitionStamp

        :param measurementID: {str} the measurement identifier
        :param fileName: {str} file name
        :param acquisitionStamp: {str} acquisition time stamp
        :return:
        """

        selectStatement = QueryStatement(self.__databaseName)\
            .filter_measurement_single(measurementID)\
            .filter_measurement_single('acquisitionStamp')\
            .filter_by_value('== "' + acquisitionStamp + '"')\
            .filter_measurement_single('filename')\
            .filter_by_value('== "' + fileName + '"')\
            .count()\
            .build_statement()

        queryResult: pd.DataFrame = self.__performDbQuery(selectStatement)

        if queryResult is not None and len(queryResult) > 0:
            return True
        else:
            return False

    def close(self):
        self.__influxClient.close()

    def createDatabase(self):
        statement = 'create database cryring_db'
        return self.executeFluxStatement(queryStatement=statement)

    def getDataBasesAll(self):
        statement = 'SHOW DATABASES'
        return self.executeFluxStatement(queryStatement=statement)

    def getActiveDatabase(self):
        return self.__databaseName

    def executeFluxStatement(self, queryStatement: str, returnResultDirect: bool = False) -> DataFrame:
        """
        Method to query InfluxDB with Flux statement
        :param queryStatement: Flux query statement
        :param returnResultDirect: flag indicating if fetch result from InfluxDB shall be returned directly (without
        transforming data to row-based output. Default: False
        :return: pandas.DataFrame containing data
        """
        if self.__influxClient is None or self.__influxClient.api_client is None:
            self.__influxClient = self.initFluxClient()

        print('Executing statement:', queryStatement)

        startTime = default_timer()

        try:
            queryResult = self.__influxClient.query_api().query_data_frame(queryStatement)
        except Exception as err:
            print('Error occurred:', err)
            raise err

        print('Received', sys.getsizeof(queryResult), 'Bytes of data from InfluxDB. Fetching took'
              , str(default_timer()
              - startTime), 'seconds')

        self.__influxClient.query_api().__del__()
        self.__influxClient.__del__()

        if returnResultDirect:
            return queryResult

        if isinstance(queryResult, DataFrame):
            if 'result' in queryResult.columns.values:
                queryResult['result'] = queryResult['result'].convert_dtypes(infer_objects=False, convert_string=True)
            # if '_value'  in queryResult.columns.values:
            #     testValue = queryResult['_value'].values[0]
            #     if str(testValue).isnumeric():
            #         queryResult['_value'] = map(lambda x: int(x), queryResult['_value'])
            #     elif str(testValue).isdecimal():
            #         queryResult['_value'] = map(lambda x: float(x), queryResult['_value'])
            if '_field' in queryResult.columns.values:
                queryResult['_field'] = queryResult['_field'].convert_dtypes(infer_objects=False, convert_string=True)
            if '_measurement' in queryResult.columns.values:
                queryResult['_measurement'] = queryResult['_measurement'].convert_dtypes(infer_objects=False,
                                                                                 convert_string=True)

            if len(queryResult.values) == 0:
                return DataFrame()
            return self.transformDataFrame(queryResult)
        elif isinstance(queryResult, list):
            output = pd.DataFrame()
            for frame in queryResult:
                output = pd.concat([output, self.transformDataFrame(frame)], axis=1)
            return output

    def initFluxClient(self) -> flux_client:

        if self.__clientType == 'UDP':
            self.__influxClient: flux_client = flux_client(url=self.__serverAddress + ':8089', use_udp=True,
                                                                 token=self.__token, udp_port=8089)
        elif self.__clientType == 'HTTP':
            self.__influxClient: flux_client = flux_client(url=self.__serverAddress + ':8086', use_udp=False,
                                                                 token=self.__token,
                                                                 user='cry_data')

        return self.__influxClient

    def initInfluxQLClient(self) -> DataFrameClient:

         passwd = 'root'
         userName = 'root'
         if self.__serverPassword != '':
             passwd = self.__serverPassword
         if self.__serverUser:
             userName = self.__serverUser

         if self.__clientType == 'UDP':
             self.__influxClient: DataFrameClient = DataFrameClient(host=self.__serverAddress, use_udp=True,
                                                  udp_port=8089, username=userName, password=passwd, database=self.__databaseName)
         elif self.__clientType == 'HTTP':
             self.__[pd.to_datetime('2022-05-03'):]

         return self.__influxClient

    def getMaxRowNumber(self, fileName: str, measurementSuffix: str, deviceName: str) -> int:

        measurementID = deviceName + measurementSuffix
        queryStatement: str = QueryStatement(self.__databaseName)\
                                .filter_measurement_single(measurementID)\
                                .filter_field_single('filename')\
                                .filter_by_value('== "' + fileName + '"')\
                                .max()\
                                .build_statement()

        queryResult = self.__performDbQuery(queryStatement)

        return len(queryResult.values)

    def getMeasurementByFilename(self, fileName: str, measurementID: str, fieldName: str = None):

        queryStatement = "SELECT * FROM " + measurementID + """ WHERE filename='""" + fileName + """';"""

        if fieldName is not None:
            queryStatement = """SELECT """ + fieldName + """ FROM """ + measurementID + """ WHERE filename='""" + \
                             fileName + """';"""

        queryResult = self.__performDbQuery(queryStatement)

        output = list()

        for item in queryResult:
            output.extend(item)

        return output

    def fetchDataByInfluxQL(self, measurementID: str = '/.*/', fields='*',
                            whereStatements: str = None) -> pd.DataFrame:
        """
        Method querying the database for data related to one measurement
        :param measurementID: {str} specification of measurementID. Query available measurements using
        'showMeasurements()'
        :param field: {str} specification of fields in a measurement, separate multiple field with comma
        :param whereStatements: {str} a string of 'WHERE' statements - without the "WHERE " which is already part of the
        coded expression. Use SQL-like syntax
        :return: {influxdb.resultset.ResultSet} result sets retrieved from database.
        """
        if self.__influxClient is None:
            self.initInfluxQLClient()

        queryResult = None

        query = None

        if whereStatements is None:
            query = 'SELECT ' + fields + ' FROM ' + measurementID
        else:
            query = 'SELECT ' + fields + ' FROM ' + measurementID + ' WHERE ' + whereStatements

        try:
            queryResult = self.__influxClient.query(query)
        except Exception as err:
            print(err)
        self.__influxClient.close()

        if queryResult != {}:
            if isinstance(queryResult, collections.defaultdict):
                return DataFrame(queryResult[measurementID])
            elif isinstance(queryResult, pandas.DataFrame):
                return queryResult
        else:
            return DataFrame()

    def showFieldKeys(self, measurementID: str = None) -> pd.DataFrame:
        """
        Method to fetch the field keys available in the database.

        :param measurementID: {str} limiting parameter, restricting the selection to the given "measurement"
        :return: {influxdb.resultset.ResultSet} result sets retrieved from database.
        """
        if measurementID is None:
            queryResult: pd.DataFrame = self.__influxClient.query_api().query_data_frame(dbq.SHOW_FIELD_KEYS)
        else:
            measurements: pd.DataFrame = self.showMeasurements()
            if measurementID not in measurements.items():
                raise ValueError(
                    'Measurement ' + measurementID + ' is not available. Choose one of ' + str(measurements.items()))

            queryResult: pd.DataFrame = self.__influxClient.query_api().query_data_frame(
                dbq.SHOW_FIELD_KEYS + ' FROM ' + measurementID)

        self.__influxClient.close()

        return queryResult

    def showMeasurements(self):
        queryResult: pd.DataFrame = self.__influxClient.query_api().query_data_frame(dbq.SHOW_MEASUREMENTS)
        self.__influxClient.close()
        return queryResult

    def showSeries(self):
        queryResult: pd.DataFrame = self.__influxClient.query_api().query_data_frame(dbq.SHOW_SERIES)
        self.__influxClient.close()
        return queryResult

    def showTagKey(self, measurementID: str = None):
        if measurementID is None:
            queryResult: pd.DataFrame = self.__influxClient.query_api().query_data_frame(dbq.SHOW_TAG_KEYS)
        else:
            queryResult: pd.DataFrame = self.__influxClient.query_api().query_data_frame(
                dbq.SHOW_TAG_KEYS + ' FROM ' + measurementID)

        self.__influxClient.close()

        return queryResult

    def __performDbQuery(self, selectStatement: str) -> pd.DataFrame:
        if self.__influxClient is None or self.__influxClient.api_client() is None:
            self.initFluxClient()

        queryResult: DataFrame = self.__influxClient.query_api().query_data_frame(selectStatement)
        self.__influxClient.query_api().__del__()
        self.__influxClient.__del__()

        return queryResult

    @staticmethod
    def performWrite(exportDataFrame, measurementID: str, tagsDict: dict, dataFileName: str, lineNumber: int,
                     serverConfig: str):
        influxClient: flux_client = InfluxDbConnector('HTTP', serverConfig=serverConfig).initFluxClient()
        print("Writing: " + measurementID + ', file: ' + dataFileName + ', line: ' + str(lineNumber))
        influxClient.write_api().write(exportDataFrame, measurement=measurementID, tags=tagsDict, time_precision='n')
        influxClient.close()
        print("Wrote: " + measurementID + ', file: ' + dataFileName + ', line: ' + str(lineNumber))

    @staticmethod
    def breakListIntoChunks(inputList: list, chunkSize: int):
        for i in range(0, len(inputList), chunkSize):
            yield inputList[i:i + chunkSize]

    def transformDataFrame(self, influxResultDataFrame: DataFrame):

        startTime = default_timer()
        groupedFrame = influxResultDataFrame.groupby('_field')
        columnNames: np.array = np.array(influxResultDataFrame.drop_duplicates(subset=['_field'])[
                                             '_field'].values)
        output: DataFrame = DataFrame()
        for columnName in columnNames:
            subFrame = groupedFrame.get_group(columnName)
            if output.empty:
                output = DataFrame(subFrame['_time'].values)
            output = pd.concat([output, DataFrame(subFrame['_value'].values)], axis=1)
        # output = output.append(Series(timestamps), ignore_index=True)
        columnNames = np.insert(columnNames, 0, 'time')
        output.columns = columnNames
        output.set_index('time', inplace=True)
        # sort by timestamp
        output.sort_index(axis=0, ascending=False, inplace=True)
        print('Data processing took:', default_timer() - startTime)
        return output

    @staticmethod
    def updateProgressBarWriteChunks(startTime, chunkCounter: int, totalChunkCount: int):
        diffTime = datetime.datetime.now() - startTime
        timePerStep = (diffTime.seconds * 1e6 + diffTime.microseconds) / chunkCounter
        timeToEnd = timePerStep * (totalChunkCount - chunkCounter)
        timeToEnd = datetime.datetime.now() + datetime.timedelta(microseconds=timeToEnd)
        progressSuffix = 'Wrote data chunk ' + str(chunkCounter) + ' of ' + str(
            totalChunkCount) + ', expected end time: ' + str(timeToEnd)
        cf.printProgressBar(chunkCounter, totalChunkCount, prefix='Progress:', suffix=progressSuffix, length=100)

    @staticmethod
    def writeToDB(resultData: dict, dataFileName: str, lineNumber: int, serverConfig, writeAsync: bool = True):
        # write result data sets to influxDB...
        for parsedDataDict in resultData.items():
            if parsedDataDict[1] is None or len(parsedDataDict[1]) == 0:
                continue

            exportDataFrame: pd.DataFrame = parsedDataDict[1]

            # print("Writing: " + dataFileName + ", " + str(lineNumber) + ", " + parsedDataDict[0])

            tagColumns: list = ConstantsContainer().getContextualtags()[
                parsedDataDict[0]]  # NOTE: name equality here an in ConstantsContainer required!

            exportDataFrame = exportDataFrame.set_index('timestampAcq')
            measurementID = exportDataFrame[:1]
            measurementID = measurementID['measurement'][0]

            tagsDict = dict()

            for tag in tagColumns:
                dataFrameItem = exportDataFrame.get(tag)
                tagsDict[tag] = dataFrameItem[0]

            # print("Writing " + dataFileName + ", data set " + str(measurementID) + " to DB...")
            influxClient: flux_client = InfluxDbConnector('HTTP', serverConfig=serverConfig).initFluxClient()
            # print(exportDataFrame.get('acquisitionStamp'))

            if exportDataFrame.size > 5000:
                print(str(len(exportDataFrame.values)))

            if writeAsync:
                dbWriteThread = threading.Thread(target=InfluxDbConnector.performWrite, args=(
                exportDataFrame, measurementID, tagsDict, dataFileName, lineNumber))
                dbWriteThread.setDaemon(True)
                dbWriteThread.start()

                # if we are dealing with rawdata, wait with write operation to end to protect DB from overload
                if measurementID == 'YRT1DT1F_rawdata':
                    dbWriteThread.join()
            else:
                influxClient.write_points(exportDataFrame, measurement=measurementID, tags=tagsDict, batch_size=5000,
                                          time_precision='n', protocol='line')
                influxClient.close()

            # print(success)

    def __setServerDataFromConfig__(self, configName: str):

        configReader = icr.InfluxDbConfigReader()
        config = configReader.getDbConfiguration(configName=configName)
        self.__serverAddress = config['address']
        self.__databaseName = config['database']
        self.__organization = config['organization']
        # self._token = config['token']
        self.__serverUser = config['user']
        self.__serverPassword = config['password']
        self.__token = f'{self.__serverUser}:{self.__serverPassword}'

    def writeToDBStrings(self, influxStrings: list, writeParallel: bool, chunks: int) -> list:
        self.__parallelWriting = writeParallel
        self.__influxClient: influxdb_client.client = self.initFluxClient()
        if self.__parallelWriting:
            listChunks = list(InfluxDbConnector.breakListIntoChunks(influxStrings, chunks))
            taskWorkers = list()
            with ProcessPoolExecutor() as taskWorker:
                if len(listChunks) > 0:
                    chunkCounter: int = 1
                    for sublist in listChunks:
                        print('Submitting data chunk ', chunkCounter, 'of ', len(listChunks), ' chunks for writing '
                                                                                              'to database.', end='\r')
                        taskWorkers.append(taskWorker.submit(self.writeInfluxPoints, sublist, 'ASYNC'))
                        chunkCounter = chunkCounter + 1
            print()
            return [taskWorkers, len(listChunks)]

        else:
            for influxString in influxStrings:
                try:
                    self.writeInfluxPoints([influxString], mode='SYNC')
                except Exception as err:
                    print('Error during write: ', err)
                    exit()

        self.__influxClient.close()

    def writeInfluxPoints(self, pointList: list, mode: str = 'SYNC',
                          write_precision: WritePrecision = WritePrecision.NS):

        write_options = SYNCHRONOUS
        if mode == 'ASYNC':
            write_options = ASYNCHRONOUS

        write_response = None
        try:
            write_response = self.__influxClient.write_api(write_options=write_options).write(
                bucket=self.__databaseName, org=self.__organization, record=pointList, write_precision=write_precision)
        except Exception as err:
            print('Error during write: ', err)
            exit()

        return write_response

    def writeDataFrame(self, dataFrame: pd.DataFrame, measurement_name: str, tag_columns_names: list = [],
                       mode: str = 'SYNC', write_precision: WritePrecision = WritePrecision.NS):

        print('Writing Dataframe', dataFrame)

        write_options = SYNCHRONOUS
        if mode == 'ASYNC':
            write_options = ASYNCHRONOUS

        writeResult = None
        try:
            writeResult =  self.__influxClient \
                .write_api(write_options=write_options) \
                .write(bucket=self.__databaseName, org=self.__organization, record=dataFrame,
                       write_precision=write_precision, data_frame_measurement_name=measurement_name,
                       data_frame_tag_columns=tag_columns_names)
        except Exception as err:
            print('Error during write: ', err)
            exit()

        return writeResult


class Configurations(Enum):
    productive = 'productive'
    localhost = 'localhost'


if __name__ == "__main__":
    found = False
    dbConnector = InfluxDbConnector('HTTP')
    dbConnector.initFluxClient()
    result = dbConnector.executeFluxStatement('from(bucket:"' + dbConnector.__databaseName + '") |> range(start: '
                                                                                              '-1y) |> '
                                                                                   'filter(fn: '
                                               '(r) => r._measurement == "YRT1DC3" and r._field == "meancurrent" ) |> '
                                               'last()')
    print(result.values)
