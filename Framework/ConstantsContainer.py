
class ConstantsContainer(object):

    suffixAll = '_all'
    suffixCharge = '_charge'
    suffixCurrent = '_current'
    suffixFlow = '_flow'
    suffixMaxCurrent = '_maxcurrent'
    suffixMeanCurrent = '_meancurrent'
    suffixParticles = '_particles'
    suffixRawdata = '_rawdata'
    suffixRawdataCols = '_rawdatacols'
    suffixRoiCharge = '_roicharge'
    suffixRoiMaxCurrent = '_roimaxcurrent'
    suffixRoiMeanCurrent = '_roimeancurrent'
    suffixRoiParticles = '_roiparticles'
    suffixVoltage = '_voltage'

    # definition of database field for various measurement contexts
    _fieldsGeneric = {'acquisitionStamp', 'actualMarkers', 'chainIndex',
                      'chainStartStamp', 'eventNumber', 'eventStamp', 'filename', 'processIndex', 'processStartStamp',
                      'sequenceIndex', 'sequenceStartStamp', 'timestampAcq', 'timestampCycle', 'timingGroupID'}

    _fieldsCharge = {'charge', 'charge_status', 'charge_units'}
    _fieldsCurrent = {'current', 'current_max', 'current_min', 'current_units', 'currentSet', 'currentSet_units'}
    _fieldsFlow = {'flow', 'flowSet', 'flowSet_units', 'flow_status', 'flow_units'}
    _fieldsMaxCurrent = {'maxCurrent', 'maxCurrent_status', 'maxCurrent_units'}
    _fieldsMeanCurrent = {'meanCurrent', 'meanCurrent_units', 'meanCurrent_status', 'meanCurrentStddev',
                          'meanCurrentStddev_units'}
    _fieldsParticles = {'particles_status', 'particles'}
    _fieldsRawData = {'frequency', 'rawData'}
    _fieldsRawDataCols = {'frequency', 'rawDataCols'}
    _fieldsRoiCharge = {'roiCharge', 'roiCharge_status', 'roiCharge_units', 'roiFromEvents'}
    _fieldsRoiMaxCurrent = {'roiMaxCurrent', 'roiMaxCurrent_status', 'roiFromEvents', 'roiMaxCurrent_units'}
    _fieldsRoiMeanCurrent = {'roiMeanCurrent', 'roiMeanCurrentStddev', 'roiFromEvents', 'roiMeanCurrent_status',
                             'roiMeanCurrent_units', 'roiMeanCurrentStddev_units'}
    _fieldsRoiParticles ={'roiParticles', 'roiParticles_status', 'roiFromEvents'}
    _fieldsVoltage = {'voltage', 'voltage_max', 'voltage_min', 'voltage_units', 'voltageSet', 'voltageSet_units'}

    # definition of tags for various measurement contexts
    _tagsGlobal = ['chainIndex', 'chainStartStamp', 'deviceName', 'eventNumber', 'linenumber', 'filename', 'processIndex',
                   'processStartStamp', 'sequenceIndex', 'sequenceStartStamp', 'timestampCycle', 'timingGroupID']

    _contextualTags: dict = {'charge': ['charge_status', 'charge_units'],
                             'current': ['current_max', 'current_min', 'current_units', 'currentSet_units'],
                             'flow': ['flowSet_units', 'flow_status', 'flow_units'],
                             'maxcurrent': ['maxCurrent_status', 'maxCurrent_units'],
                             'meancurrent': ['meanCurrent_status', 'meanCurrent_units', 'meanCurrentStddev_units'],
                             'particles': ['particles_status'],
                             'rawdata': ['frequency'],
                             'roicharge': ['roiCharge_status', 'roiCharge_units'],
                             'roimaxcurrent': ['roiMaxCurrent_status', 'roiMaxCurrent_units'],
                             'roimeancurrent': ['roiMeanCurrent_status', 'roiMeanCurrent_units',
                                                'roiMeanCurrentStddev_units'],
                             'roiparticles': ['roiParticles_status'],
                             'voltage': ['voltage_max', 'voltage_min', 'voltage_units', 'voltageSet_units']
                       }

    _deploymentUnitAssigment = {
        'powerSupply': {'YRT1IN1E', 'YRT1IN1K', 'YRT1IN1M', 'YRT1IN1O', 'YRT1IN1X', 'YRT1LE'},
        'trafo': {'YRT1DT1F'},
        'massFlowController': {'YRT1IN1G'}
    }

    def __init__(self):
        for tag in self._contextualTags.items():
            # append global tags to contextual tags
            if tag[0] != 'flow':
                joinedTagList = self._contextualTags[tag[0]] + self._tagsGlobal
                self._contextualTags[tag[0]] = list(set(joinedTagList))

    def getContextualtags(self) -> dict:
        return self._contextualTags

    def getDeploymentUnitAssignments(self) -> dict:
        return self._deploymentUnitAssigment