SHOW_FIELD_KEYS = 'from(bucket:"cryring_db/autogen") |> range(start: -1y) |> filter(fn:(r) => r._measurement == ' \
                  '"YRT1IZ1EP") |> distinct(column:"_field")'
SHOW_MEASUREMENTS = 'import "influxdata/influxdb/v1" v1.measurements(bucket: "cryring_db/autogen")'
SHOW_SERIES = 'SHOW SERIES'
SHOW_TAG_KEYS = 'from(bucket:"cryring_db/autogen") |> range(start: -2y) |> group(none:true) |> keys(except:["_time",' \
                '"_value", "_start","_stop"])'
