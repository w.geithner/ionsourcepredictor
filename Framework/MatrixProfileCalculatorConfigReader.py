from os.path import dirname
from os.path import join
import yaml

MAIN_DIRECTORY = dirname(dirname(__file__))
defaultFileName = '//resources//MatrixProfileCalculatorConfigs.yml'


class MatrixProfileCalculatorConfigReader():

    __config_name = ''
    _calculatorConfigs = dict()

    def __init__(self):
        self._calculatorConfigs = self.__readConfigFile(defaultFileName)

    @staticmethod
    def __get_full_path(*path):
        return join(MAIN_DIRECTORY, *path)

    def __readConfigFile(self, fileName: str):
        with open(self.__get_full_path() + fileName) as theFile:
            fileContent = yaml.load(theFile, Loader=yaml.FullLoader)
            return fileContent

    def items(self):
        return self._calculatorConfigs.values()

    def configuration(self, configName: str):
        self.__config_name = configName
        if configName not in self._calculatorConfigs:
            print(configName + ' not found in configurations. Available configurations are: ' +
                  str(self._calculatorConfigs.keys()))
            exit()

        return self

    def parameter_name(self):
        return self._calculatorConfigs[self.__config_name]['parameter_name']

    def test_field_name(self):
        return self._calculatorConfigs[self.__config_name]['test_field_name']

    def start_time(self):
        return self._calculatorConfigs[self.__config_name]['start_time']

    def end_time(self):
        return self._calculatorConfigs[self.__config_name]['end_time']

    def spill_count(self) -> int:
        return int(self._calculatorConfigs[self.__config_name]['spill_count'])

    def write_rawdata(self):
        return self._calculatorConfigs[self.__config_name]['write_rawdata']

    def resample_factor(self) -> float:
            return float(self._calculatorConfigs[self.__config_name]['resample_factor'])

    def smoothing_size(self) -> int:
        return int(self._calculatorConfigs[self.__config_name]['smoothing_size'])

    def search_time_advance(self) -> float:
        return float(self._calculatorConfigs[self.__config_name]['search_time_advance'])

    def fetch_window_size(self) -> float:
        return float(self._calculatorConfigs[self.__config_name]['fetch_window_size'])


if __name__ == '__main__':
    configReader = MatrixProfileCalculatorConfigReader()
    configData = configReader.configuration('YRT1DT1F')
    dataKeys = configData().keys()

    for key in dataKeys:
        print(configData[key])
