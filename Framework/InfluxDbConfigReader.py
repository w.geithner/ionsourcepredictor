from os.path import dirname
from os.path import join
import yaml

MAIN_DIRECTORY = dirname(dirname(__file__))
defaultFileName = '//resources//DatabaseConfigs.yml'


class InfluxDbConfigReader():

    _dbConfigs = dict()

    def __init__(self):
        self._dbConfigs = self.__readConfigFile(defaultFileName)

    @staticmethod
    def __get_full_path(*path):
        return join(MAIN_DIRECTORY, *path)

    def getProductiveDB(self) -> str:
        return self._dbConfigs['productive']['database']

    def __readConfigFile(self, fileName: str):
        with open(self.__get_full_path() + fileName) as theFile:
            fileContent = yaml.load(theFile, Loader=yaml.FullLoader)
            return fileContent

    def productiveDBConfig(self) -> dict:
        return self._dbConfigs['productive']

    def getDbConfiguration(self, configName: str) -> dict:

        if configName not in self._dbConfigs:
            print(configName + ' not found in configurations. Available configurations are: ' +
                  str(self._dbConfigs.keys()))
            exit()

        return self._dbConfigs[configName]


if __name__ == '__main__':
    configReader = InfluxDbConfigReader()
    configData = configReader.getDbConfiguration('vml032DB')
    dataKeys = configData.keys()

    for key in dataKeys:
        print(configData[key])
