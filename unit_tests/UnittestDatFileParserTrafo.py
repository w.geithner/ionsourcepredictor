import unittest
from Framework.DatFileParsers.ClassDatFileParser_Trafo import DatFileParser_Trafo
from Framework.DatFileParsers.ClassParserFactory import ParserFactory


class TestDatFileParserTrafo(unittest.TestCase):

    _fileParser = None

    def testParseFile(self):
        if self._fileParser is None:
            self._fileParser: DatFileParser_Trafo = ParserFactory.getParser('20190315_00_YRT1DT1F.dat', True)

        self._fileParser.parseFile(None, '20190315_00_YRT1DT1F.dat', 20)


if __name__ == "__main__":
    unittest.main()