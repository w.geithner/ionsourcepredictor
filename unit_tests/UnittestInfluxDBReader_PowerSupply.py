import unittest
from Framework.influxDBReaders.InfluxDBReader_PowerSupply import InfluxDBReader_PowerSupply
import pandas as pd


class TestInfluxDBReader_PowerSupply(unittest.TestCase):

    _dbReaderInstance = None

    def testGetCurrentAll(self):
        self._dbReaderInstance = InfluxDBReader_PowerSupply('YRT1IN1K')
        result: pd.DataFrame = self._dbReaderInstance.getCurrentAll(test=True)

        assert len(result['current']) == 10

    def testGetCurrentInTimeFrame(self):
        self._dbReaderInstance = InfluxDBReader_PowerSupply('YRT1IN1K')
        result: pd.DataFrame = self._dbReaderInstance.getCurrentInAcqTimeFrame('2019-03-23',
                                                                               '00:00:00',
                                                                               '2019-03-23',
                                                                               '23:59:59')

        assert result is not None
        assert result['current'] is not None


if __name__ == '__main__':
    unittest.main()