import unittest
from Framework.ActiveMqConnector import *

class ActiceMqConnectionTest(unittest.TestCase):

    def testActiveMqConnection(self):
        connector = ActiveMqConnector()
        assert connector

    def testStartConnection(self):
        connector = ActiveMqConnector()
        assert connector.startConnection() is True

    def testStopConnection(self):
        connector = ActiveMqConnector()
        assert connector.startConnection() is True
        assert connector.stopConnection() is True

    def testSendMessage(self):
        connector = ActiveMqConnector()
        assert connector.startConnection() is True
        connector.sendStringMessageToQueue(stringMessage="hello queue", queue=ActiveMqQueue.test)


if __name__ == "__main__":
    unittest.main()