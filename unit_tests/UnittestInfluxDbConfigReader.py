from Framework.InfluxDbConfigReader import InfluxDbConfigReader

import unittest


class MyTestCase(unittest.TestCase):

    _configReader: InfluxDbConfigReader = None

    def setUp(self) -> None:
        self._configReader = InfluxDbConfigReader()

    def testInstantiation(self):
        self.assertIsNotNone(self._configReader)

    def test(self):
        config: dict = self._configReader.getDbConfiguration('productive')
        print(config)

    def testGetProductiveDB(self):
        productiveDB = self._configReader.getProductiveDB()
        self.assertEqual(productiveDB, 'cryring_db/autogen')


if __name__ == '__main__':
    unittest.main()
