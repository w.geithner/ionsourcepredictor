import unittest
from Framework.KafkaConnector.KafkaBrokerConnector import *

class UnittestKafkaBrokerConnector(unittest.TestCase):

    _kafkaBrokerConnector = None

    def testInstantiation(self):
        if self._kafkaBrokerConnector is None:
            self._kafkaBrokerConnector = KafkaBrokerConnector()

        assert self._kafkaBrokerConnector is not None

    def testSendMessage(self):
        self.testInstantiation()
        self._kafkaBrokerConnector.sendTextMessage(topic='cryring_db_inbound', messageText='deviceName (string:1) -> '
                                                                                        'TEST\nvalue (int:1) -> 1')

    def testConsumeFromBroker(self):
        self.testInstantiation()
        self._kafkaBrokerConnector.consumeFromBroker('cryring_db_inbound')

if __name__ == "__main()__":
    unittest.main()