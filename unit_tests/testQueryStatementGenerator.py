from Framework import CommonFunctions as cf
from Framework.FluxStatementBuilder import QueryStatement
from datetime import datetime
import unittest


class MyTestCase(unittest.TestCase):

    _cryring_db = 'cryring_db/autogen'
    _statementBuilder: QueryStatement = None

    def setUp(self) -> None:
        self._statementBuilder = QueryStatement(self._cryring_db)

    def testSetUp(self):
        statement = self._statementBuilder.build_statement()
        print(statement)
        self.assertTrue(statement.find(self._cryring_db) > -1)

    def testAddRangeStartOnly(self):
        time = datetime.now().strftime(cf.InfluxTimeFormatString())
        statement = self._statementBuilder.range(time).build_statement()
        print(statement)
        self.assertTrue(statement.find(time) > -1)

    def testChaining(self):
        time = datetime.now().strftime(cf.InfluxTimeFormatString())
        statement = self._statementBuilder.range(time).filter_measurement_single('YRT1DC3').build_statement()
        print(statement)

    def testMultipleFields(self):
        fields = ['a', 'b', 'c']
        statement = self._statementBuilder.filter_field_multiple(fields).build_statement()
        print(statement)


if __name__ == '__main__':
    unittest.main()
