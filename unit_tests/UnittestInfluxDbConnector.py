from Framework import CommonFunctions as cf
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
import influxdb_client
from influxdb_client import InfluxDBClient
from influxdb_client.client.write_api import WritePrecision
from random import random, seed
from pandas import DataFrame, Series
import unittest
from datetime import datetime
from time import sleep

from Framework.InfluxDbConnectorFlux import InfluxDbConnector


class TestInfluxDbConnector(unittest.TestCase):

    __influxConnector: InfluxDbConnector = None
    __influxClient: InfluxDBClient = None
    __productiveDB = 'cryring_db/autogen'

    TEST_FILE_TRAFO = '20190315_00_YRT1DT1F.dat'
    TEST_FILE_TRAFO_FAILS = '20090315_00_YRT1DT1F.dat'
    TEST_MEASUREMENT_TRAFO_FAILS = 'YRT1DT1F_fails'
    TEST_MEASUREMENT_TRAFO_CHARGE = 'YRT1DT1F_charge'
    TEST_MEASUREMENT_TRAFO_PARTICLES = 'YRT1DT1F_particles'

    TEST_QUERY = 'from(bucket:"' + __productiveDB + '") |> range(start: -1y) |> filter(fn: (r) => ' \
                                                            'r._measurement == ' \
                                             '"YRT1DC3" and r._field == "meancurrent" ) |> last()'

    def setUp(self):
        if self.__influxConnector is None:
            self.__influxConnector = InfluxDbConnector('HTTP')
        self.__influxClient = self.__influxConnector.initFluxClient()
        self.__productiveDB = icr().getProductiveDB()

    def testConnectDb(self):
        assert self.__influxConnector is not None
        assert self.__influxClient is not None
        assert self.__influxClient.health().status != 'fail'

    def testShowMesurements(self):
        """
        Test is measurements can be queried in DB.
        Prerequisite: database has data
        * check if query result is not None
        * check if result items number > 0

        :return: void
        """
        result: DataFrame = self.__influxConnector.showMeasurements()

        assert result.items() is not None
        assert len(result.values) > 0
        for point in result.values:
            print('Measurements in DB (testShowMeasurements): ', point)

    def testShowFieldKeys(self):
        result: DataFrame = self.__influxConnector.showFieldKeys()

        assert result.items() is not None
        assert len(result.values) > 0

    def testGetRecords(self):
        result: DataFrame = self.__influxConnector.executeFluxStatement(self.TEST_QUERY)

        assert result is not None
        print('Data record fetched in testGetRecords: ', result.items())
        for item in result.items():
            print(item)

    def testWritePointSynchronous(self):
        seed(1)
        thePoint = influxdb_client.Point(measurement_name="unit_test") \
            .field("random_sync", random()) \
            .time(int(datetime.now().timestamp() * 10**9), write_precision=WritePrecision.NS)

        self.__influxConnector.writeInfluxPoints([thePoint])

    def testWritePointAsynchronous(self):
        seed(1)
        thePoint = influxdb_client.Point(measurement_name="unit_test") \
            .field("random_async", random()) \
            .time(int(datetime.now().timestamp() * 10**9), write_precision=WritePrecision.NS)

        write_result = self.__influxConnector.writeInfluxPoints([thePoint], mode='ASYNC')
        assert write_result is not None

    def testWriteMultiPointsSYNC(self):
        seed(1)

        points: list = list()

        startTime = datetime.utcnow().strftime(cf.InfluxTimeFormatString())
        for loop in range(10):
            thePoint = influxdb_client.Point(measurement_name="unit_test") \
                .field("random_multi", random()) \
                .time(int(datetime.now().timestamp() * 10**9), write_precision=WritePrecision.NS)
            points.append(thePoint)
            sleep(0.01)

        stopTime = datetime.utcnow().strftime(cf.InfluxTimeFormatString())

        self.__influxConnector.writeInfluxPoints(points, mode='SYNC')
        result = self.__influxConnector.executeFluxStatement('from(bucket:"' + self.__productiveDB + '") |> range('
                                                                                                   'start: ' \
                                                              + startTime \
                                                              + ', stop: ' + stopTime + ') ' \
                                                              + '|> filter(fn:(r) => r._measurement == "unit_test"' \
                                                              + ' and r._field == "random_multi" )')

        # check that 10 entries have been written to DB
        assert len(result.values) == 10

    def testWriteDataFrameSynchronous(self):
        seed(2)

        theDataFrame: DataFrame = DataFrame()
        theIndex: list = list()

        # produce some data...
        for loop in range(20):
            dataDict = dict()
            for i in range(10):
                dataDict['random_' + str(i)] = random()
            dataDict['time'] = int(datetime.now().timestamp() * 10 ** 9)
            newSeries = Series(dataDict)

            theDataFrame = theDataFrame.append(newSeries, ignore_index=True)

        theDataFrame.set_index('time')
        self.__influxConnector.writeDataFrame(theDataFrame, measurement_name='unit_test')