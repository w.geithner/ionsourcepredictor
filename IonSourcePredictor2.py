"""
Literature: https://www.freecodecamp.org/news/multi-class-classification-with-sci-kit-learn-xgboost-a-case-study-using-brainwave-data-363d7fca5f69/
Approach:
- use feature "charge" or "meancurrent" of YRT1DT1F_all as label
- use data in YRT1DT1F_analysis as classifiers

- As a starting point I used .\\TutorialsTests\\XGBoost.py

"""
from Framework.FluxStatementBuilder import QueryStatement
from Framework.InfluxDbConnectorFlux import InfluxDbConnector, Configurations as conf
from Framework import CommonFunctions as cf
from pandas import DataFrame

dbConnector = InfluxDbConnector('HTTP', serverConfig=conf.productive.name)
influxClient = dbConnector.initFluxClient()

# fetch analysis data from influx server: YRT1DT1F_analysis. this are my multi-class training data , use as X-data
statement: str = QueryStatement(dbConnector.getActiveDatabase()).range().filter_measurement_regex('YRT1DT1F')
signalDescriptorDataSet: DataFrame = dbConnector.executeFluxStatement(statement)

for item in signalDescriptorDataSet:
    print(item)


# fetch YRT1DT1F_all.meancurrent from influxDB. this is my Y-Data (objective)
statement: str = 'SELECT charge FROM YRT1DT1F_all'

# combine two data sets to one where the goal Y-value is the meancurrent of analysis data set + delta time ahead
# perform multiclass classification
