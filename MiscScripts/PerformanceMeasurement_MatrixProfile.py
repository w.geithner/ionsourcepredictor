from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework import CommonFunctions as cf
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
from timeit import default_timer as timer
import pandas as pd
from pandas import DataFrame
import numpy as np
from scipy import stats, signal
from matplotlib import pyplot as plt
import seaborn as sns
from matrixprofile import algorithms as algo

sns.set()

_dataReader = InfluxDataExtractor()
_parameterName = 'YRT1DC3'
_testField = 'meancurrent'
_startTime = '2020-06-06T16:28:00.0Z'
_endTime = '2020-06-06T20:28:45.0Z'
_loopCount = 7
_maxLimitCount = 15
_mpLimitCount = 5
_measurementRepetitions = 5
_downsamplingMax = 9
_productiveDB = icr().productiveDBConfig()


def fetchData(startTime: str, endTime: str):
    statement = 'from(bucket:"' + _productiveDB + '")' \
                + ' |> range(start: ' + startTime + ', stop: ' + endTime + ')' \
                + ' |> filter(fn:(r) => r._measurement == "' + _parameterName \
                + '" and r._field =~ ' + cf.RawdataRegex() + ')' \
                + ' |> sort(desc: false) |> limit(n: ' + str(_mpLimitCount) + ')'

    result: DataFrame = _dataReader.executeStatement(statement)

    measurementDataSet = pd.Series(dtype=float)
    loopCounter = 0
    rawDataWindowSize = 0
    for item in result.values:
        if loopCounter == 0:
            rawDataWindowSize = len(item) - 1
        rawData = pd.Series(item[:len(item) - 1])
        measurementDataSet = measurementDataSet.append(rawData, ignore_index=True)

    return measurementDataSet, rawDataWindowSize


def measureDbFetchPerformance():

    fetchDataSet = pd.DataFrame()

    for limitCount in range(2, _maxLimitCount + 1, 1):
        statement = 'from(bucket:"' + _productiveDB + '")' + ' |> range(start: ' + _startTime + ', ' \
                                                                                                            'stop: ' + _endTime + \
                    ')' + ' |> filter(fn:(r) => r._measurement == "' + _parameterName + '" and r._field =~ ' + \
                    cf.RawdataRegex() + ')' + ' |> sort(columns:["time"], desc: false) |> limit(n: ' + str(
            _mpLimitCount) + ')'

        print('Testing:', statement)
        # collect statisticas on data fetching
        timeData = pd.Series()
        for loopCounter in range(_loopCount):
            fetchStart = timer()
            result: DataFrame = _dataReader.executeStatement(statement)
            fetchEnd = timer()
            fetchTime = fetchEnd - fetchStart
            timeData = timeData.append(pd.Series(np.array(fetchTime)))
            print(loopCounter + 1, ' of ' + _loopCount + ' loops')

        statistics = stats.describe(timeData.values)
        print(statistics)
        fetchDataSet = fetchDataSet.append({
                                               'limitcount': limitCount, 'mean': statistics.mean,
                                               'min': statistics.minmax[0], 'max': statistics.minmax[1],
                                               'variance': statistics.variance, 'kurtosis': statistics.kurtosis,
                                               'skewness': statistics.skewness}, ignore_index=True)
    fetchDataSet.to_csv(r'D:\Data\PapersInPrep\2020-09_CryringMachineLearning\Data\performance_influx_fetch.csv',
                        index=False, header=True)

    sns.set_context('paper')
    sns.set_style('ticks')
    plt.figure(figsize=(10, 6))
    # sns.pointplot(x='limitcount', y='mean', data=fetchDataSet, dodge=True, join=True, ci=None)
    plt.errorbar(x='limitcount', y='mean', yerr='variance', data=fetchDataSet, marker='o')
    plt.xlabel('number of data sets fetched from DB')
    plt.ylabel('mean fetch duration [s]')
    plt.title('Fetch duration from Influx database with increasing number of data sets')
    plt.show()


def measureMatrixProfilePerformance():
    measurementDataSet, rawDataWindowSize = fetchData(_startTime, _endTime)
    measurementDataSet.to_csv(r'D:\Data\PapersInPrep\2020-09_CryringMachineLearning\Data\matrix_profile_input.csv',
                        index=False, header=True)

    print('Fetched data')

    data = measurementDataSet.values
    data = np.array(signal.resample(measurementDataSet.values, len(measurementDataSet.values)), dtype=float)
    print(type(data))
    # raw data matrix profiling:
    timeData = pd.Series(dtype=float)
    for loopCounter in range(_measurementRepetitions):
        print('Raw data window size:', rawDataWindowSize)
        print('rawdata length:', len(data))
        startTime = timer()
        rawDataMatrixProfile = algo.scrimp_plus_plus(data, rawDataWindowSize, n_jobs=4)
        stopTime = timer()
        resultTime = stopTime - startTime
        print('Matrix profile calculation took:', resultTime)
        timeData = timeData.append(pd.Series(np.array(resultTime)))

    print(timeData)
    statistics = stats.describe(timeData.values)
    mpMeasurementResult = pd.DataFrame()

    mpMeasurementResult = mpMeasurementResult.append(pd.Series({
                                                               'factor': 1.0,
                                                               'mean': statistics.mean, 'min': statistics.minmax[0],
                                                               'max': statistics.minmax[1],
                                                               'variance': statistics.variance,
                                                               'kurtosis': statistics.kurtosis,
                                                               'skewness': statistics.skewness}), ignore_index=True)

    # resampled matrix profile

    resamplingMeasurementResult = pd.DataFrame()

    for downsamplingFactor in range(_downsamplingMax):
        matrixProfileTimeData = pd.Series(dtype=float)
        resampleTimeData = pd.Series(dtype=float)
        downsamplingRate = (9 - downsamplingFactor) / 10
        print('resampling factor:', downsamplingRate)
        resample_goal = int(len(data) * downsamplingRate)
        for loop in range(_measurementRepetitions):
            startTime = timer()
            resampled_data = signal.resample(data, resample_goal)
            stopTime = timer()
            resampleTimeData = resampleTimeData.append(pd.Series(np.array(stopTime - startTime)))

        resampleStatistics = stats.describe(resampleTimeData)
        resamplingMeasurementResult = resamplingMeasurementResult.append(pd.Series({
            'factor': downsamplingRate, 'mean': resampleStatistics.mean,
            'min': resampleStatistics.minmax[0], 'max': resampleStatistics.minmax[1],
            'variance': resampleStatistics.variance, 'kurtosis': resampleStatistics.kurtosis,
            'skewness': resampleStatistics.skewness}), ignore_index=True)


        # matrix profile with resampled data:
        mpWindowSize = int(resample_goal / _mpLimitCount)
        print('MP window size:', mpWindowSize)
        print('data set size:', len(resampled_data))
        for repetition in range(_measurementRepetitions):
            startTime = timer()
            resampledMatrixProfile = algo.scrimp_plus_plus(resampled_data, mpWindowSize, n_jobs=4)
            stopTime = timer()
            matrixProfileTimeData = matrixProfileTimeData.append(pd.Series(np.array(stopTime - startTime)))

        matrixProfileStatistics = stats.describe(matrixProfileTimeData)
        mpMeasurementResult = mpMeasurementResult.append(pd.Series({
                                                                   'factor': downsamplingRate,
                                                                   'mean': matrixProfileStatistics.mean,
                                                                   'min': matrixProfileStatistics.minmax[0],
                                                                   'max': matrixProfileStatistics.minmax[1],
                                                                   'variance': matrixProfileStatistics.variance,
                                                                   'kurtosis': matrixProfileStatistics.kurtosis,
                                                                   'skewness': matrixProfileStatistics.skewness}),
                                                     ignore_index=True)
    figure = plt.figure(figsize=(10,6))
    sns.set_style('ticks')
    sns.set_context('paper')
    plt.title('Calculation times depending on resampling rate')
    plt.xlabel('downsampling factor')
    plt.xlim(1.1,0.)
    plt.ylabel('calculation time [s]')
    plt.yscale('log')
    plt.errorbar(x=mpMeasurementResult['factor'], y=mpMeasurementResult['mean'], yerr=mpMeasurementResult[
        'variance'], label='matrix-profile', marker='o')
    plt.errorbar(x=resamplingMeasurementResult['factor'], y=resamplingMeasurementResult['mean'], yerr=resamplingMeasurementResult[
        'variance'], label='resampling', marker='o')
    plt.legend(loc='upper right')
    plt.xticks(np.arange(0.1, 1.1, 0.1))
    plt.grid(axis='y', which='both')
    for item in mpMeasurementResult.values:
        plt.annotate("{0:,.3f}".format(item[2]), (item[0], item[2]))
    for item in resamplingMeasurementResult.values:
        plt.annotate("{0:,.1f}".format(item[2]*1000) + 'ms', (item[0], item[2]))
    plt.show()


def measureSignalToNoise():

    sns.set_style('ticks')
    sns.set_context('paper')

    # fetch data with low matrix profile = signal
    signalData, signalWindowSize = fetchData(_startTime, _endTime)
    signalData = np.array(signal.resample(signalData.values, len(signalData.values)), dtype=float)

    plt.figure(figsize=(10,6))
    plt.plot(signalData)
    plt.title('Example of Noisy Signal')
    plt.xlabel('channel number')
    plt.ylabel('signal value [a.u.]')
    plt.show()
    plt.close()

    # fetch data with high matrix profile = noise
    noiseData, noiseWindowSize = fetchData('2020-06-06T16:50:00.0Z', _endTime)
    noiseData = np.array(signal.resample(noiseData.values, len(noiseData.values)), dtype=float)

    signalMP: dict = algo.scrimp_plus_plus(signalData, signalWindowSize, n_jobs=4)
    signalResult = pd.DataFrame(dtype=float)
    signalStats = stats.describe(signalMP['mp'])
    signalResult = signalResult.append(pd.Series({
        'factor': 1.0, 'mean': signalStats.mean, 'min': signalStats.minmax[0],
        'max': signalStats.minmax[1], 'variance': signalStats.variance,
        'kurtosis': signalStats.kurtosis, 'skewness': signalStats.skewness}), ignore_index=True)

    print(signalResult)

    noiseMP = algo.scrimp_plus_plus(noiseData, noiseWindowSize, n_jobs=4)
    noiseResult = pd.DataFrame(dtype=float)
    noiseStats = stats.describe(noiseMP['mp'])
    noiseResult = noiseResult.append(pd.Series({
        'factor': 1.0, 'mean': noiseStats.mean, 'min': noiseStats.minmax[0],
        'max': noiseStats.minmax[1], 'variance': noiseStats.variance,
        'kurtosis': noiseStats.kurtosis, 'skewness': noiseStats.skewness}), ignore_index=True)

    for downsamplingFactor in range(_downsamplingMax):
        downsamplingRate = (9 - downsamplingFactor) / 10
        print('resampling factor:', downsamplingRate)
        resample_goal = int(len(noiseData) * downsamplingRate)
        mpWindowSize = int(resample_goal / _mpLimitCount)

        signalResampled = signal.resample(signalData, resample_goal)
        signalMP: dict = algo.scrimp_plus_plus(signalResampled, mpWindowSize, n_jobs=4)
        signalStats = stats.describe(signalMP['mp'])
        signalResult = signalResult.append(pd.Series({
            'factor': downsamplingRate, 'mean': signalStats.mean, 'min': signalStats.minmax[0], 'max': signalStats.minmax[1],
            'variance': signalStats.variance, 'kurtosis': signalStats.kurtosis, 'skewness': signalStats.skewness}),
            ignore_index=True)

        noiseResampled = signal.resample(noiseData, resample_goal)
        noiseMP: dict = algo.scrimp_plus_plus(noiseResampled, mpWindowSize, n_jobs=4)
        noiseStats = stats.describe(noiseMP['mp'])
        noiseResult = noiseResult.append(pd.Series({
            'factor': downsamplingRate, 'mean': noiseStats.mean, 'min': noiseStats.minmax[0], 'max': noiseStats.minmax[1],
            'variance': noiseStats.variance, 'kurtosis': noiseStats.kurtosis, 'skewness': noiseStats.skewness}),
            ignore_index=True)

    signalToNoise = pd.DataFrame({'factor': signalResult['factor'], 'stn': (noiseResult['mean'] / signalResult[
        'mean'])})
    print('Signal to noise:', signalToNoise)

    fig, ax = plt.subplots(figsize=(10, 6))
    ax = signalToNoise.plot(x='factor', y='stn', label='signal / noise', marker='o', color='red')
    ax.set_ylabel('mean("signal") / mean("noise") [a.u.]')
    ax.legend(loc='lower left')
    # plt.axes.Axes['left'].set_color('red')
    ax.twinx()

    plt.errorbar(x=signalResult['factor'], y=signalResult['mean'], yerr=signalResult['variance'],
                 label='signal case', marker='o', capsize=5.0)
    plt.errorbar(x=noiseResult['factor'], y=noiseResult['mean'], yerr=noiseResult['variance'], label='noise case',
                 marker='o', capsize=5.0)

    plt.title('signal to noise at different downsampling ratios')
    plt.legend(loc='upper right')
    plt.xlabel('downsampling ratio')
    plt.ylabel('matrix-profile statistics values')
    plt.xticks(np.arange(0.1, 1.1, 0.1))
    plt.xlim(1.1, 0.)
    plt.show()


if __name__ == '__main__':
    measureSignalToNoise()
