from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework.FluxStatementBuilder import QueryStatement
from Framework import CommonFunctions as cf
import numpy as np

_fetchMeasurement = 'YRT1IN1K'
_writeMeasurement = 'YRT1IN1K'

measurements = ['YRT1IN1K/Acquisition', 'YRT1IN1K_all', 'YRT1IN1M', 'YRT1IN1M/Acquisition', 'YRT1IN1M_all',
                'YRT1IN1O', 'YRT1IN1O_all', 'YRT1IN1P', 'YRT1IN1X', 'YRT1IN1X/Acquisition', 'YRT1IN1X_all',
                'YRT1MH1', 'YR12DA1R', 'YRT1DC3', 'YRT1DC7', 'YRT1DP', 'YRT1DT1F', 'YRT1DT1F/Acquisition',
                'YRT1DT1F_all']

def __main__():
    localdb = InfluxDbConnector(clientType='HTTP', serverConfig='localhost')
    localdb.initFluxClient()
    remotedb = InfluxDbConnector(clientType='HTTP', serverConfig='productive')
    remotedb.initFluxClient()

    # for i in np.arange(start=767, stop=40000, step=1):

    fieldName = '/.*/'
    print(fieldName)

    for measurement in measurements:
        for year in np.arange(2019, 2021, 1):
            for monthIndex in np.arange(1, 13, 1):
                for dayIndex in np.arange(1, 32, 1):
                    day = str(dayIndex).zfill(2)
                    for hourIndex in np.arange(0, 24, 1):
                        hourStart = str(hourIndex).zfill(2)
                        prefix = str(year) + '-' + str(monthIndex).zfill(2) + '-' + day + 'T'
                        timeStart = prefix + str(hourIndex).zfill(2) + ':00:00.0Z'
                        if (hourIndex < 23):
                             timeStop = prefix + str(hourIndex + 1).zfill(2) + ':00:00.0Z'
                        else:
                            timeStop = prefix + '23:59:59.999Z'

                        query = QueryStatement(localdb.getActiveDatabase()) \
                            .range(timeStart, timeStop) \
                            .filter_measurement_single(measurement)  \
                            .filter_field_regex(fieldName) \
                            .build_statement()

                        try:
                            result = localdb.executeFluxStatement(query, returnResultDirect=False)
                            if len(result.values) == 0:
                                continue
                            result = result.rename(columns=str.lower)
                        except Exception as err:
                            print(err)
                            continue

                        writeMeasurement = measurement.replace('/Acquisition', '')
                        writeMeasurement = writeMeasurement.replace('_all', '')

                        writeResult = remotedb.writeDataFrame(result, measurement_name=writeMeasurement)
                        print(writeResult)


if __name__ == "__main__":
    __main__()