from Framework.InfluxDbConnectorFlux import InfluxDbConnector

_influxClient = InfluxDbConnector('HTTP')
_measurementID = 'YRT1DC3_MatrixProfileOffline'


def deleteSeriesDataAll():
    deleteStatement = 'DELETE FROM ' + _measurementID
    _influxClient.executeFluxStatement(deleteStatement)


if __name__ == '__main__':
    deleteSeriesDataAll()