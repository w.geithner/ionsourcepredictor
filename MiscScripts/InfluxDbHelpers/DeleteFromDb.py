from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework.ConstantsContainer import ConstantsContainer as const
from Framework import CommonFunctions


class DatabaseDeleter:

    _influxClient = InfluxDbConnector('HTTP')

    def deleteRoiParticlesByFilename(self, fileName):

        queryResult = self._influxClient.getMeasurementByFilename(fileName)

        for item in queryResult:

            deleteStatement = """DELETE FROM YRT1DT1F_roiparticles WHERE sequenceStartStamp='""" + item[
                'sequenceStartStamp'] + """'"""
            print(deleteStatement)

            self._influxClient.executeFluxStatement(deleteStatement)

    def deleteTrafoDataByFilename(self, fileName: str, measurementID: str, testFieldName:str=None):
        queryResult = self._influxClient.getMeasurementByFilename(fileName, measurementID, testFieldName)

        for item in queryResult:
            deleteStatement = """DELETE FROM """ + measurementID + """ WHERE time='""" + item['time'] + """';"""
            self._influxClient.executeFluxStatement(deleteStatement)
            print('Deleted by: ' + deleteStatement)

        print('Done deleting data of file ' + fileName)

    def deletePointsInTimeFrame(self, measurementID: str, deletionStartTime: str = None, deletionEndTime: str = None):
        deleteStatement = ''
        if deletionStartTime is not None and deletionEndTime is not None:
            deleteStatement = 'DELETE FROM ' + measurementID + """ WHERE time>='""" + deletionStartTime + """' AND 
            time<='""" + deletionEndTime + """';"""
        elif deletionStartTime is not None and deletionEndTime is None:
            deleteStatement = 'DELETE FROM ' + measurementID + """ WHERE time>='""" + deletionStartTime + """';"""
        elif deletionStartTime is None and deletionEndTime is not None:
            deleteStatement = 'DELETE FROM ' + measurementID + """ WHERE time<'""" + deletionEndTime + """';"""
        else:
            raise Exception('deletion start and end time were None. Please provide at least deletion start or end '
                            'time.')
        self._influxClient.executeFluxStatement(deleteStatement)
        print('Deleted by: ' + deleteStatement)

if __name__ == "__main__":
    deleter = DatabaseDeleter()
    #deleter.deleteTrafoDataByFilename('20190322_14_YRT1IN1O.dat', 'YRT1IN1O_all')
    startTime = '2020-05-12T07:00:00.0Z'
    # endTime =
    deleter.deletePointsInTimeFrame('YRT1DC3_MatrixProfileOffline', deletionStartTime=startTime)

