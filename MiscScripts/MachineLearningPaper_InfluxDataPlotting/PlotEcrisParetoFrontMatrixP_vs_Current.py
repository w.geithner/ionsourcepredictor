from matplotlib.collections import PathCollection
from matplotlib.legend import Legend

from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
from Framework.FluxStatementBuilder import QueryStatement
import numpy as np
import pandas as pd
from pandas import DataFrame
import seaborn as sns
from matplotlib import pyplot as plt
from timeit import default_timer as dt

sns.set()

_deviceName = 'YRT1DC3'
_detectorField = 'meancurrent'

def plotSingle(start, stop):
    timerStart = dt()
    _matrixProfileMeasurement = _deviceName + '_MatrixProfileOffline'
    _matrixProfileFields = ['raw_mean', 'smoothed_mean', 'resampled_mean', 'raw_mpwindowsize',
        'smoothed_mpwindowsize', 'resampled_mpwindowsize']

    mpStatement: str = QueryStatement(icr().getProductiveDB())\
                        .range(start, stop)\
                        .filter_measurement_single(_matrixProfileMeasurement)\
                        .filter_field_multiple(_matrixProfileFields)\
                        .build_statement()

    theDataExtractor = InfluxDataExtractor()
    matrixProfileFrame: DataFrame = theDataExtractor.executeStatement(mpStatement)
    print(type(matrixProfileFrame))
    if matrixProfileFrame is None or len(matrixProfileFrame) == 0:
        print('No data retrieved')
        return

    print('First fetch:', dt() - timerStart, ' seconds, data set length: ', len(matrixProfileFrame))

    matrixProfileFrame['raw_mean'] = matrixProfileFrame['raw_mean'] / np.sqrt(2) / np.sqrt(matrixProfileFrame[
                                                                                               'raw_mpwindowsize'])
    if 'smoothed_mpwindowsize' in matrixProfileFrame.keys():
        matrixProfileFrame['smoothed_mean'] = matrixProfileFrame['smoothed_mean'] / np.sqrt(2) / np.sqrt(
            matrixProfileFrame['smoothed_mpwindowsize'])
    else:
        matrixProfileFrame['smoothed_mean'] = matrixProfileFrame['smoothed_mean'] / np.sqrt(2) / np.sqrt(
            matrixProfileFrame['raw_mpwindowsize'])

    if 'resampled_mpwindowsize' in matrixProfileFrame.keys():
        matrixProfileFrame['resampled_mean'] = matrixProfileFrame['resampled_mean'] / np.sqrt(2) / np.sqrt(
            matrixProfileFrame['resampled_mpwindowsize'])
    else:
        matrixProfileFrame['resampled_mean'] = matrixProfileFrame['resampled_mean'] / np.sqrt(2) / np.sqrt(
            matrixProfileFrame['raw_mpwindowsize'] * 0.2)

    detectorStatement: str = QueryStatement(icr().getProductiveDB())\
                        .range(start, stop)\
                        .filter_measurement_single(_deviceName)\
                        .filter_field_single(_detectorField)\
                        .build_statement()

    detectorFrame = theDataExtractor.executeStatement(detectorStatement)

    print('Second fetch:', dt() - timerStart, ', data set legth: ', len(matrixProfileFrame.values))
    detectorFrame[_detectorField] = detectorFrame[_detectorField] * 10**9 # scale to nanoampere
    aggregateFrame = pd.concat([matrixProfileFrame, detectorFrame], axis = 1)
    aggregateFrame.dropna(axis = 0, inplace=True)

    print('total:', dt() - timerStart)
    return aggregateFrame


def plotData():

    times: dict = {}

    #times['YRT1DT1F 2019/03'] = {'start': '2019-03-22T14:00:00.0Z', 'stop': '2019-04-01T00:00:00.00Z'}
    times['reference1'] = {'start': '2020-05-13T10:10:00.0Z', 'stop': '2020-05-13T10:58:00.0Z', 'color': '#5F3789'}
    times['reference2'] = {'start': '2020-05-13T09:40:00.0Z', 'stop': '2020-05-13T09:55:00.50Z', 'color': '#376C89'}
    times['reference3'] = {'start': '2020-05-28T15:56:40.0Z', 'stop': '2020-05-28T16:26:00.0Z', 'color': '#3374FF'}
    times['gas_flow_test'] = {'start': '2020-05-26T15:00:00.0Z', 'stop': '2020-05-26T17:13:00.00Z', 'color': '#30EC3E'}
    times['experiment1'] = {'start': '2020-05-26T17:13:54.0Z', 'stop': '2020-05-26T23:59:59.00Z', 'color': '#C11D00'}
    times['reference4'] = {'start': '2020-05-27T19:28:40.0Z', 'stop': '2020-05-27T19:31:00.0Z', 'color': '#0440A4'}
    times['experiment2'] = {'start': '2020-05-27T19:32:30.0Z', 'stop': '2020-05-27T21:20:00.00Z', 'color': '#E77E00'}
    times['reference5'] = {'start': '2020-05-27T21:48:40.0Z', 'stop': '2020-05-27T22:00:00.0Z', 'color': '#0440A4'}
    times['experiment3'] = {'start': '2020-05-28T16:29:30.0Z', 'stop': '2020-05-28T17:50:00.00Z', 'color': '#D65D5D'}
    times['experiment3'] = {'start': '2020-05-28T17:50:00.0Z', 'stop': '2020-05-28T19:00:00.00Z', 'color': '#E3DF0F'}
    times['experiment4'] = {'start': '2020-05-28T19:35:00.0Z', 'stop': '2020-05-28T21:30:00.00Z', 'color': '#E5105E'}

    labels = [
        # 'YRT1DT1F 2019/03',
        '1. Manual operation reference (2020-05-13)',
        '2. Manual operation reference (2020-05-13)',
        '3. Manual operation reference (2020-05-26)',
        'Manual gas flow optimization (2020-05-26)',
        'EXP1: Optimized 4 params (2020-05-26)',
        '4. Manual operation reference (2020-05-27)',
        'EXP2: Optimized 4 params (2020-05-27)',
        '5. Manual operation reference (2020-05-27)',
        'EXP3: Optimized 4 params (2020-05-28)',
        'EXP4: Optimized 4 params (2020-05-28)',
        'EXP5: Optimized 4 params (2020-05-28)'
        #'EXP5: Optimized 4 params',
        #'EXP6: Optimized 4 params',
        #'EXP7: Optimized 4 params',
        #'EXP8: Optimized 4 params'
        ]

    #ax = fig.add_subplot(111)

    sns.set_context('paper')
    sns.set_style('ticks')
    fig = plt.figure(figsize=(10,10))

    loop_counter = 0
    for key in times.keys():
         frame = plotSingle(times.get(key)['start'], times.get(key)['stop'])
         if frame is None:
             continue
         presampledData = frame.loc[frame['raw_mpwindowsize'] < 10000]
         rawData = frame.loc[frame['raw_mpwindowsize'] >= 10000]
         if len(presampledData.values) > 0:
            plt.scatter(x='resampled_mean', y=_detectorField, data=presampledData, s=0.75, label=labels[loop_counter],
                        c=times.get(key)['color'])
         if len(rawData.values) > 0:
            plt.scatter(x='raw_mean', y=_detectorField, data=rawData, s=0.75, label=labels[loop_counter],
                        c=times.get(key)['color'])
         loop_counter = loop_counter + 1

    plt.legend(loc="upper right", numpoints=1, fontsize=14, markerscale=8)
    plt.xlabel('normalized matrix profile mean', fontsize=18)
    plt.ylabel('ion beam current [nA]', fontsize=18)
    plt.show()


if __name__ == '__main__':
    plotData()