from DataExtraction.InfluxDataExtractor import InfluxDataExtractor

_lowerTimeString = '2020-05-06T11:41:00.0Z'
_upperTimeString = '2020-06-06T20:10:00.0Z'


def plotData():
    theDataViewer = InfluxDataExtractor()
    theDataViewer.plotDataInTimeRange('YRT1DC3_MatrixProfile', _lowerTimeString, _upperTimeString, 'mean',
                                      xTimeAxis=True)


if __name__ == '__main__':
    plotData()