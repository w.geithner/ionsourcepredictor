from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
import pandas as pd
from timeit import default_timer as deft
from matplotlib import pyplot as plt
from scipy import stats
import seaborn as sns
sns.set()

_gaMeasurementID = 'YR00_GENETIC_SCAN'
_gaFieldID = 'iteration'
_signalMeasurementID = "YRT1DC3"
_signalFieldID = 'meancurrent'
_lowerTimeString = '2020-06-06T11:41:00.0Z'
_upperTimeString = '2020-06-06T20:10:00.0Z'


def plotData():
    theDataViewer = InfluxDataExtractor()
    gaStepData: pd.DataFrame = theDataViewer.getDataFromInfluxByTimeRange(_gaMeasurementID, _lowerTimeString,
                                                                          _upperTimeString, _gaFieldID)

    firstGaTime = gaStepData['time'][0]
    lastGaTime = gaStepData['time'][len(gaStepData['time']) - 1]

    gaStepData['time'] = pd.to_datetime(gaStepData['time'])

    loadStartTime = deft()
    signalData: pd.DataFrame = theDataViewer.getDataFromInfluxByTimeRange(_signalMeasurementID, firstGaTime, lastGaTime,
                                                                          _signalFieldID)

    signalData['time'] = pd.to_datetime(signalData['time'])
    loadEndTime = deft()

    print('Load time: ' + str(loadEndTime - loadStartTime) + ' seconds')

    outDataFrame: pd.DataFrame = pd.DataFrame()
    gaStepIndex: int = 0
    for gaTime in gaStepData['time']:
        if gaStepIndex < len(gaStepData['time']) - 1:
            upperGaTime = gaStepData['time'][gaStepIndex + 1]
            mask = (signalData['time'] >= gaTime) & (signalData['time'] < upperGaTime)
            signalSlice: pd.DataFrame = signalData.loc[mask]
            sliceStats = stats.describe(signalSlice['meancurrent'])
            sliceOutDict = dict()
            sliceOutDict['gastep'] = int(gaStepIndex)
            sliceOutDict['time'] = gaTime
            sliceOutDict['meancurrent_mean'] = sliceStats[2] * 1e9
            sliceOutDict['meancurrent_variance'] = sliceStats[3] * 1e9
            outDataFrame = outDataFrame.append(pd.Series(sliceOutDict), ignore_index=True)
            gaStepIndex = gaStepIndex + 1

    sns.set_context('paper')
    with sns.axes_style('white'):
        plot = sns.pointplot(x='gastep', y='meancurrent_mean', data=outDataFrame, join=True, scale=0.5)
        plot.errorbar(x=outDataFrame['gastep'], y=outDataFrame['meancurrent_mean'], yerr=outDataFrame[
            'meancurrent_variance'])
        plt.title('bi-objective GA optimization, current on detector')
        plt.xlabel('GA step number')
        plt.ylabel('ion beam on Faraday cup [nA]')
        plt.show()


if __name__ == '__main__':
    plotData()
