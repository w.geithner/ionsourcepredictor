from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
from Framework.FluxStatementBuilder import QueryStatement
from Framework import CommonFunctions as cf
import numpy as np
import pandas as pd
from pandas import DataFrame, Series
import seaborn as sns
from matplotlib.gridspec import GridSpec
from matplotlib import pyplot as plt
from timeit import default_timer as dt

# sns.set()

_startTime = '2020-05-29T02:19:00.0Z'
_endTime = '2020-05-29T02:21:30.0Z'

# _startTime = '2020-05-13T02:19:00.0Z'
# _endTime = '2020-05-13T02:19:07.0Z'

def plotData():
    mpStatement: str = QueryStatement(icr().getProductiveDB()) \
        .range(_startTime, _endTime) \
        .filter_measurement_single('YRT1DC3_MatrixProfileOffline') \
        .filter_field_single('raw_variance') \
        .build_statement()

    theDataExtractor = InfluxDataExtractor()
    matrixProfileFrame: DataFrame = theDataExtractor.executeStatement(mpStatement)

    mpStatement = QueryStatement(icr().getProductiveDB()) \
        .range(_startTime, _endTime) \
        .filter_measurement_single('YRT1DC3') \
        .filter_field_single('meancurrent') \
        .build_statement()

    detectorFrame: DataFrame = theDataExtractor.executeStatement(mpStatement)

    mpStatement = QueryStatement(icr().getProductiveDB()) \
        .range(_startTime, _endTime) \
        .filter_measurement_single('YRT1DC3') \
        .filter_field_regex(cf.RawdataRegex()) \
        .build_statement()
    rawDataFrame: DataFrame = theDataExtractor.executeStatement(mpStatement)

    rawDataSeries: Series = Series(dtype=int)
    for item in rawDataFrame.values:
        itemSeries: Series = Series(item)
        rawDataSeries = rawDataSeries.append(itemSeries, ignore_index=True)

    sns.set_context('paper')
    sns.set_style('ticks')

    plt.subplots(3, 1)
    plt.figure(figsize=(10, 6))

    #theFigure.add_subplot(theGrid[0])
    plt.subplot(3,1,1)
    plt.plot(rawDataSeries)
    plt.ylabel('frequency')
    plt.xlabel('sample number')
    plt.title('stitched raw-data of spills')

    # theFigure.add_subplot(theGrid[1])
    plt.subplot(3,1,2)
    plt.plot(detectorFrame)
    plt.ylabel('beam current [A]')
    plt.title('mean current from Faraday-cup')

    # theFigure.add_subplot(theGrid[2])
    plt.subplot(3,1,3)
    plt.plot(matrixProfileFrame)
    plt.ylabel('matrix-profile variance')
    plt.title('variance of matrix-profile')

    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05, hspace=0.5)
    plt.show()


if __name__ == '__main__':
    plotData()
