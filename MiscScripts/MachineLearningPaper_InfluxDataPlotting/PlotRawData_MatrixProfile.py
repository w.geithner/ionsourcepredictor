from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
from Framework.FluxStatementBuilder import QueryStatement
from Framework import CommonFunctions as cf
import numpy as np
from matrixprofile.algorithms import scrimp_plus_plus as spp
from pandas import DataFrame, Series
import seaborn as sns
from matplotlib import pyplot as plt

sns.set()

_startTime = '2020-12-05T21:02:00.0Z'
_endTime = '2020-12-05T21:02:48.0Z'

def plotData():
    padArray = np.empty((10000), dtype=float)
    padArray[:] = np.nan

    mpStatement: str = QueryStatement(icr().getProductiveDB()) \
        .range(_startTime, _endTime) \
        .filter_measurement_single('YRT1DT1F') \
        .filter_field_regex(cf.RawdataRegex()) \
        .build_statement()

    theDataExtractor = InfluxDataExtractor()
    rawDataFrame: DataFrame = theDataExtractor.executeStatement(mpStatement)

    print('fetched data')
    rawDataSeries: Series = Series(dtype=int)
    windowSize = 0
    for item in rawDataFrame.values:
        windowSize = len(item)
        itemSeries: Series = Series(item)
        rawDataSeries = rawDataSeries.append(itemSeries, ignore_index=True)

    matrixProfile = spp(rawDataSeries.values, window_size=windowSize)

    mpValues: np.ndarray = np.append(padArray, matrixProfile['mp'], axis=0)
    mpSeries: Series = Series(matrixProfile['mp'])
    mpDataFrame: DataFrame = DataFrame()
    mpDataFrame = mpDataFrame.append(mpSeries, ignore_index=True)
    mpDataFrame = mpDataFrame.append(Series(np.arange(0, len(mpValues))), ignore_index=True)
    mpDataFrame = mpDataFrame.transpose()
    mpDataFrame.columns = ['value', 'index']
    # mpDataFrame.set_index('index')

    print(mpDataFrame.values)

    sns.set_context('paper')
    sns.set_style('ticks')

    plt.subplots(nrows=2, sharex=True)
    plt.figure(figsize=(10, 6))

    plt.subplot(2, 1, 1)
    plt.plot(rawDataSeries)
    plt.title('raw data')
    plt.xlabel(None)
    plt.ylabel('frequency')
    plt.subplot(2, 1, 2)
    plt.plot('index', 'value', data=mpDataFrame)
    plt.title('matrix-profile')
    plt.xlabel('channel index')
    plt.ylabel('matrix-profile value')

    plt.tight_layout()
    # plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    plt.show()
    plt.close()

    plt.figure(figsize=(10, 6))
    plt.hist(x=mpDataFrame['value'], bins=50, range=(10, 30))
    plt.title('histogram of values in matrix-profile')
    plt.xlabel('matrix-profile value')
    plt.ylabel('frequency')
    plt.show()


if __name__ == '__main__':
    plotData()
