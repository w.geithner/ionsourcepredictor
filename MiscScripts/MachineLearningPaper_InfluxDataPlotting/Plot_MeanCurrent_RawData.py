from timeit import default_timer
from Framework import CommonFunctions as cf
from Framework import InfluxDbConnectorFlux, FluxStatementBuilder
from datetime import datetime
import pandas as pd
from timeit import default_timer as timer
from matplotlib import pyplot as plt
import numpy as np
from scipy import stats
import seaborn as sns
sns.set()

_signalMeasurementID = "YRT1DT1F"
_signalFieldID = 'meancurrent'
_lowerTimeString = '2020-12-10T11:41:00.0Z'
_upperTimeString = '2020-12-10T11:41:20.0Z'


def plotData():

    meanCurrentQuery = FluxStatementBuilder \
        .QueryStatement(bucket_name='cryring_db/autogen') \
        .range(_lowerTimeString, _upperTimeString) \
        .filter_measurement_single(_signalMeasurementID) \
        .filter_field_single('meancurrent') \
        .build_statement()

    rawDataQuery = FluxStatementBuilder \
        .QueryStatement(bucket_name='cryring_db/autogen') \
        .range(_lowerTimeString, _upperTimeString) \
        .filter_measurement_single(_signalMeasurementID) \
        .filter_field_regex('/rawdata[0-9]{6}/') \
        .build_statement()

    sns.set_context('paper')
    start_time = default_timer()
    connector = InfluxDbConnectorFlux.InfluxDbConnector('HTTP')
    connector.initFluxClient()
    meanCurrentResult = connector.executeFluxStatement(rawDataQuery)
    print('Data fetching:', default_timer() - start_time)
    print(meanCurrentResult)
    plt.plot(meanCurrentResult.values.flatten())
    plt.show()


if __name__ == '__main__':
    plotData()
