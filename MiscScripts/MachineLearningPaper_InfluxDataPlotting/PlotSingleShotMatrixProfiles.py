from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework import CommonFunctions as cf
import pandas as pd

from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
import numpy as np
from scipy import signal, stats
import matrixprofile
from timeit import default_timer as ti
from datetime import datetime

_lowerTimeString = '2020-05-29T04:16:15.0Z'
_upperTimeString = '2020-05-29T04:17:15.0Z'

# _lowerTimeString = '2020-06-06T16:32:40.0Z'
# _upperTimeString = '2020-06-06T16:33:25.0Z'


def plotData():
    if datetime.strptime(_lowerTimeString, cf.InfluxTimeFormatString()).timestamp() > datetime.strptime(_upperTimeString,
                                                                                                        cf.InfluxTimeFormatString()).timestamp():
        print('Error: lower time is later then upper time')

    theDataViewer = InfluxDataExtractor()

    start = ti()
    result = theDataViewer.getDataFromInfluxByTimeRange('YRT1DC3', _lowerTimeString, _upperTimeString, cf.RawdataRegex())
    stop = ti()
    print('Data loading time: ', str(stop - start), 's')

    result = result.drop(columns='time')
    numberOfSpills = len(result.get_values())
    print('Number of spills in result: ', numberOfSpills)

    rawDataSeries: pd.Series = pd.Series()
    resampledDataSeries = pd.Series()
    singleShot = pd.Series(result.get_values()[0])
    firstResampledSingleSpill = None
    resampledMpWindowSize = 0
    for rawData in result.get_values():
        rawDataSeries = rawDataSeries.append(pd.Series(rawData), ignore_index=True)
        resampledSingleSpill = signal.resample(rawData, num=2000)
        if firstResampledSingleSpill is None:
            firstResampledSingleSpill = resampledSingleSpill
        resampledDataSeries = resampledDataSeries.append(pd.Series(resampledSingleSpill), ignore_index=True)
        if resampledMpWindowSize == 0:
            resampledMpWindowSize = len(resampledSingleSpill)

    # smoothingWindowSize = 20
    # lowerWindow = int(smoothingWindowSize)
    # upperWindow = len(rawDataSeries.get_values()) - smoothingWindowSize
    # convolutionResult = signal.resample(rawDataSeries.get_values(), num=2000)
    # convolutionResult = signal.convolve(rawDataContainer.get_values(),
    #                                   signal.windows.blackmanharris(smoothingWindowSize))

    # rawDataMean = np.mean(rawDataSeries.get_values())
    # convolutionMean = np.mean(convolutionResult)
    # convolutionResult = convolutionResult * rawDataMean / convolutionMean
    # dataSeriesSmoothed = pd.Series(convolutionResult[lowerWindow:upperWindow])

    rawDataMpWindowSize = len(singleShot)

    start = ti()
    mpRawData = matrixprofile.algorithms.scrimp_plus_plus(rawDataSeries.get_values(), rawDataMpWindowSize, n_jobs=4)
    end = ti()
    print('Processing time raw data: ', str(end - start), 's')

    mpRawDataStats = stats.describe(mpRawData['mp'])
    mpRawDataSeries: pd.Series = pd.Series(mpRawData['mp'])
    print('Raw data length: ', len(rawDataSeries.get_values()))

    start = ti()
    mpSmoothedData = matrixprofile.algorithms.scrimp_plus_plus(resampledDataSeries.get_values(),
                                                               resampledMpWindowSize, n_jobs=4)

    end = ti()
    print('Processing time of resampled data: ', str(end - start), 's')

    mpSmoothedDataStats = stats.describe(mpSmoothedData['mp'])

    print('Smoothed data length: ', len(resampledDataSeries.get_values()))
    print('Mean value of raw data matrix profile: ', mpRawDataStats.mean)
    print('Variance of raw data matrix profile: ', mpRawDataStats.variance)
    print('Mean of smoothed data matrix profile: ', mpSmoothedDataStats.mean)
    print('Variance of smoothed data matrix profile: ', mpSmoothedDataStats.variance)


    mpSmoothedSeries: pd.Series = pd.Series(mpSmoothedData['mp'])

    theFigure = plt.figure(tight_layout=True)
    theGrid = GridSpec(5, 2, figure=theFigure)

    # plt.subplot(411)
    theFigure.add_subplot(theGrid[0, :])
    rawDataSeries.plot()
    singleShot.plot()
    plt.title('a) raw data, ' + str(numberOfSpills) + ' spills')
    # thePlots[3] = rawDataPlot

    theFigure.add_subplot(theGrid[1, :])
    mpRawDataSeries.plot()
    plt.title('b) matrix profile of raw data, window size = ' + str(rawDataMpWindowSize))

    theFigure.add_subplot(theGrid[2, :])
    resampledDataSeries.plot()
    plt.plot(firstResampledSingleSpill)
    plt.title('c) downsampled data, downsample factor = ' + str(len(resampledDataSeries.get_values()) / len(
        rawDataSeries.get_values())))

    theFigure.add_subplot(theGrid[3, :])
    mpSmoothedSeries.plot()
    plt.title('d) matrix-profile of downsampled data, window size = ' + str(resampledMpWindowSize))

    theFigure.add_subplot(theGrid[4, 0])
    mpRawDataSeries.plot.hist(bins=100)
    plt.title('e1) distribution of raw data matrix-profile')

    theFigure.add_subplot(theGrid[4, 1])
    mpSmoothedSeries.plot.hist(bins=100)
    plt.title('e2) distribution of downsampled data matrix-profile')

    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05, hspace=0.5)
    plt.show()


if __name__ == '__main__':
    plotData()
