from Framework import CommonFunctions as cf
from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from datetime import datetime
import pandas as pd
from timeit import default_timer as timer
from matplotlib import pyplot as plt
import numpy as np
from scipy import stats
import seaborn as sns
sns.set()
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

_mpMeasurementID = 'YRT1DC3_MatrixProfile'
_gaMeasurementID = 'YR00_GENETIC_SCAN'
_gaFieldID = 'iteration'
_signalMeasurementID = "YRT1DC3"
_signalFieldID = 'meancurrent'
_lowerTimeString = '2020-06-06T11:41:00.0Z'
_upperTimeString = '2020-06-06T20:10:00.0Z'


def plotData():
    theDataViewer = InfluxDataExtractor()

    gaStepData: pd.DataFrame = theDataViewer.getDataFromInfluxByTimeRange(_gaMeasurementID, _lowerTimeString,
                                                                          _upperTimeString, _gaFieldID)

    firstGaTime = gaStepData['time'][0]
    lastGaTime = gaStepData['time'][len(gaStepData['time']) - 1]

    gaStepData['time'] = pd.to_datetime(gaStepData['time'])

    loadStartTime = timer()
    signalData: pd.DataFrame = theDataViewer.getDataFromInfluxByTimeRange(_signalMeasurementID, firstGaTime, lastGaTime,
                                                                          _signalFieldID)

    signalData['time'] = pd.to_datetime(signalData['time'])
    loadEndTime = timer()

    mpData: pd.DataFrame = theDataViewer.getDataFromInfluxByTimeRange(_mpMeasurementID, firstGaTime, lastGaTime, 'mean, max, variance')

    mpData['variance'] = .25*mpData['variance']

    mpData['stepnumber'] = np.arange(len(mpData['time']))

    print('Load time: ' + str(loadEndTime - loadStartTime) + ' seconds')

    outDataFrame: pd.DataFrame = pd.DataFrame()
    gaStepIndex: int = 0
    for gaTime in gaStepData['time']:
        if gaStepIndex < len(gaStepData['time']) - 1:
            upperGaTime = gaStepData['time'][gaStepIndex + 1]
            mask = (signalData['time'] >= gaTime) & (signalData['time'] < upperGaTime)
            signalSlice: pd.DataFrame = signalData.loc[mask]
            sliceStats = stats.describe(signalSlice['meancurrent'])
            sliceOutDict = dict()
            sliceOutDict['time'] = gaTime
            sliceOutDict['meancurrent_mean'] = sliceStats[2] * 1e9
            sliceOutDict['meancurrent_variance'] = sliceStats[3] * 1e9
            outDataFrame = outDataFrame.append(pd.Series(sliceOutDict), ignore_index=True)
            gaStepIndex = gaStepIndex + 1

    outDataFrame['stepnumber'] = stepnumbers = np.arange(len(outDataFrame['time']))

    sns.set_context('paper')
    theFigure, thePlots = plt.subplots(2, 1, figsize=(10, 6))
    theFigure.subplots_adjust(top=0.95)
    with sns.axes_style('ticks'):
        plt.subplot(211)
        gaPlot = sns.lineplot(x='stepnumber', y='meancurrent_mean', data=outDataFrame)
        # gaPlot.errorbar(x=outDataFrame['stepnumber'], y=outDataFrame['meancurrent_mean'], yerr=outDataFrame[
        #   'meancurrent_variance'])
        gaPlot.xaxis.set_major_locator(MultipleLocator(50))
        gaPlot.xaxis.set_major_formatter(FormatStrFormatter('%d'))
        gaPlot.xaxis.set_minor_locator(MultipleLocator(10))
        plt.rcParams['xtick.top'] = plt.rcParams['xtick.bottom'] = True
        plt.xlabel(None)
        plt.ylabel('ion beam on Faraday cup [nA]')

        thePlots[0] = gaPlot

        plt.subplot(212, sharex=thePlots[0])
        sns.lineplot(x='stepnumber', y='mean', data=mpData, sizes=1., label='mp mean')
        # mpPlot = sns.lineplot(x='stepnumber', y='max', data=mpData, sizes=(1.), markers='o')
        mpPlot = sns.lineplot(x='stepnumber', y='variance', data=mpData, sizes=1.,
                              label='mp variance')
        mpPlot.xaxis.set_major_locator(MultipleLocator(50))
        mpPlot.xaxis.set_major_formatter(FormatStrFormatter('%d'))
        mpPlot.xaxis.set_minor_locator(MultipleLocator(10))
        plt.legend(bbox_to_anchor=(0.19, 0.07), borderaxespad=0)
        plt.xlabel('GA step number')
        plt.ylabel('Matrix profile statistics data')
        thePlots[1] = mpPlot

    theFigure.suptitle('bi-objective GA optimization, current on detector', y=1.)
    #plt.tight_layout()
    # plt.subplots_adjust(left=0.125, bottom=0.1, right=0.9, top=0.9, wspace=0.2, hspace=0.35)
    plt.subplots_adjust(hspace=0.13)
    plt.show()


if __name__ == '__main__':
    plotData()
