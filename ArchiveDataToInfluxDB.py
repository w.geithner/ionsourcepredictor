from Framework.DatFileParsers.ClassAbstractDatFileParser import AbstractDatFileParser
from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework.DatFileParsers.ClassDatFileParser_MassFlowController import DatFileParser_MassFlowController
from Framework.DatFileParsers.ClassDatFileParser_Trafo import DatFileParser_Trafo
from Framework.DatFileParsers.ClassDatFileParser_PowerSupply import DatFileParser_PowerSupply
from Framework import CommonFunctions as cf
from Framework.ConstantsContainer import ConstantsContainer as const
from Framework.ListToInfluxStringParser import ListToInfluxStringParser as lisp
from concurrent.futures import ProcessPoolExecutor
import concurrent.futures
import re
import datetime
from os import listdir
import os
from os.path import join
import sys
import argparse


def checkIfFileIsInDB(fileName: str, measurementSuffix: str, deviceName: str, totalLineCount: int, serverConfig: str):
    if InfluxDbConnector('HTTP', serverConfig=serverConfig).checkIfDataIsInDb(fileName, totalLineCount - 2,
                                                                              deviceName + measurementSuffix, 0,
                                                                              None) == 'FILE':
        print('File ' + fileName + ', measurement ' + deviceName + measurementSuffix
              + ' already in DB.')
        return True

    return False


def checkIfLineIsInDB(fileName: str, measurementSuffix: str, deviceName: str, totalLineCount: int, lineCounter: int,
                      skipCheckIfDataIsInDb: bool, timestamp, serverConfig: str) -> bool:
    if InfluxDbConnector('HTTP', serverConfig=serverConfig).checkIfDataIsInDb(fileName, totalLineCount - 2, deviceName +
                                                                                                 measurementSuffix,
                                                   lineCounter, timestamp) == 'LINE' and not skipCheckIfDataIsInDb:
        print('File ' + fileName + ', line ' + str(lineCounter) + ', measurement ' + deviceName + measurementSuffix
              + ' is already in DB.')
        return False
    else:
        # return pd.DataFrame().from_dict(dataList)
        return True


def getMaxRowNumber(fileName: str, measurementSuffix: str, deviceName: str, serverConfig: str):
    return InfluxDbConnector('HTTP', serverConfig=serverConfig).getMaxRowNumber(fileName=fileName,  deviceName=deviceName,
                                                     measurementSuffix=measurementSuffix)


def performTrafoLineParsing(parser, dataFileLine, deviceName, dataFileName, lineCounter, maxRowCounters: dict,
                            rawDataResamplingFactor: float, storageMode: str = None) -> list:

    parserResult = parser.parseLine(dataFileLine, deviceName, dataFileName, rawDataResamplingFactor, lineCounter,
                                    storageMode=storageMode)

    if len(parserResult) > 0:
        dictionaryList = list()

        # logic to append disctionary only of no data is in database
        for parsedDataDict in parserResult.items():
            if parsedDataDict[0] == 'trafo' and lineCounter > maxRowCounters[deviceName + const.suffixCharge]:
                dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'charge' and lineCounter > maxRowCounters[deviceName + const.suffixCharge]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'maxCurrent' and lineCounter > maxRowCounters[deviceName +
            #                                                                         const.suffixMaxCurrent]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'meancurrent' and lineCounter > maxRowCounters[deviceName +
            #                                                                          const.suffixMeanCurrent]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'particles' and lineCounter > maxRowCounters[deviceName +
            #                                                                        const.suffixParticles]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            elif parsedDataDict[0] == 'rawdata' and lineCounter > maxRowCounters[deviceName +
                                                                                 const.suffixRawdata]:
                dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'rawdatacols' and lineCounter > maxRowCounters[deviceName +
            #                                                                          const.suffixRawdataCols]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'roiCharge' and lineCounter > maxRowCounters[deviceName + const.suffixRoiCharge]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'roiMaxCurrent' and lineCounter > maxRowCounters[deviceName +
            #                                                                            const.suffixRoiMaxCurrent]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'roiMeanCurrent' and lineCounter > maxRowCounters[deviceName +
            #                                                                             const.suffixRoiMeanCurrent]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])
            # elif parsedDataDict[0] == 'roiParticles' and lineCounter > maxRowCounters[deviceName +
            #                                                                           const.suffixRoiParticles]:
            #     dictionaryList = cf.appendDictToList(dictionaryList, parsedDataDict[1])

        influxStringList = lisp.parseListToInfluxString(dictionaryList)

        return influxStringList


def main():

    argumentParser = argparse.ArgumentParser(description='This is the DAT-file to influxDb importer.')
    argumentParser.add_argument('-pp', '--process_parallel', choices=['False', 'True'], required=True,
                                help='-pp: enables parallel processing of data file lines')
    argumentParser.add_argument('-wp', '--write_parallel', choices=['False', 'True'], required=True,
                                help='-wp: enables parallel processing of write to DB')
    argumentParser.add_argument('-c', '--chunks', required=True,
                                help='number of data points written to influx in parallel mode.'
                                     'Has no effect in non-parallel mode')
    argumentParser.add_argument('-regex', '--regular_expression', required=False,
                                help='-regex: regular expression/search pattern for '
                                                               'DAT files to influde into DB upload.')
    argumentParser.add_argument('-hd', '--header_lines', required=False, default=2,
                                help='-hd: number of header lines in the file prefixed by #')

    argumentParser.add_argument('-config', '--server_configuration', required=True, help='-config: name of '
                                                                                         'configuration defined in '
                                                                                         'yaml configuration file ('
                                                                                         'default file name = '
                                                                                         'DatabaseConfis.yml.')

    argumentParser.add_argument('-d',
                                '--directory',
                                required=False,
                                default='archive_files',
                                help='Directory relative to directory where script is located, where .dat file are stored.')

    argumentParser.add_argument('-rdm'
                                , '--raw_data_mode'
                                , choices=['r', 'c', 'rc']
                                , required=False
                                , default='c'
                                , help='Mode for storing the raw data:\n'
                                       '"r" = one row per raw data bin\n'
                                       '"c" = one column per raw data bin\n'
                                       '"rc" = write raw data as rows and columns')

    argumentParser.add_argument('-cf'
                                , '--compression_factor'
                                , required=False
                                , type=float
                                , default=1.0
                                , help='Compression factor of raw data, meaning raw data will be re-sampled by this '
                                       'factor. Works only for writing raw data in columns.')

    arguments = argumentParser.parse_args(sys.argv[1:])
    processParallel = arguments.process_parallel
    writeParallel = arguments.write_parallel
    chunks = int(arguments.chunks)
    regexStatement = arguments.regular_expression
    configName = arguments.server_configuration
    datDirectory = arguments.directory
    storageMode = arguments.raw_data_mode
    rawDataResamplingFactor = arguments.compression_factor

    myPath = os.getcwd() + os.sep + datDirectory + os.sep

    files = [f for f in listdir(myPath) if os.path.isfile(join(myPath, f))]

    for dataFileName in files:
        fileProcessingStartTime = datetime.datetime.now()
        if regexStatement is None:
            fileRegex = re.compile("\d\d\d\d\d\d\d\d_\d\d_")
        else:
            fileRegex = re.compile(regexStatement)

        if not fileRegex.match(dataFileName):
            # doNothing = True
            continue

        totalLineCount = cf.getLinesInFile(myPath, dataFileName)

        fileObject = open(myPath + dataFileName, "r")
        dataFileLine = fileObject.readline()

        # determine parser type by reading header lines in file.
        deploymentUnitType = None
        deviceName = None

        while dataFileLine.startswith('#'):
            if dataFileLine.find('deploymentUnit=') > -1:
                splitArray = dataFileLine.split('=')
                deploymentUnitType = splitArray[1]
                deploymentUnitType = deploymentUnitType.replace('\n', '')
            if dataFileLine.find('deviceName=') > -1:
                splitArray = dataFileLine.split('=')
                deviceName = splitArray[1]
                deviceName = deviceName.replace('\n', '')

            dataFileLine = fileObject.readline()
        fileObject.close()

        maxRowCounts = dict()

        print('Started processing file ' + dataFileName + '...')

        if deploymentUnitType == "massFlowController":

            # perform check if file has been uploaded to DB
            # Note: the "totalLineCount - 2" assumes two header lines prefixed by a '#'
            rowsInDb = getMaxRowNumber(fileName=dataFileName, deviceName=deviceName,
                                       measurementSuffix=const.suffixFlow, serverConfig=configName)
            if rowsInDb >= totalLineCount - 2:
                print('File ' + dataFileName + ' is already in DB...')
                continue

            parser = DatFileParser_MassFlowController(deviceName=deviceName, serverConfig=configName)
            parserResult: list = parser.parseFile(myPath, dataFileName)
            if len(parserResult) > 0:
                print("Writing " + dataFileName + " to DB...")
                InfluxDbConnector('HTTP', serverConfig=configName).writeToDBStrings(parserResult, writeParallel, chunks)

        if deploymentUnitType == "powerSupply":

            # perform check if file has been uploaded to DB
            # Note: the "totalLineCount - 2" assumes two header lines prefixed by a '#'
            rowsInDb = getMaxRowNumber(fileName=dataFileName, deviceName=deviceName,
                                       measurementSuffix=const.suffixAll, serverConfig=configName)

            if rowsInDb >= totalLineCount - 2:
                print('File ' + dataFileName + ' is already in DB...')
                continue

            parser = DatFileParser_PowerSupply(deviceName=deviceName, serverConfig=configName)
            parserResult: list = parser.parseFile(myPath, dataFileName)
            if len(parserResult) > 0:
                print("Writing " + dataFileName + " to DB...")
                InfluxDbConnector('HTTP', serverConfig=configName).writeToDBStrings(parserResult, writeParallel, chunks)

        if deploymentUnitType == "trafo":

            checkSuffix = None
            if storageMode == 'r':
                checkSuffix = const.suffixRawdata
            elif storageMode == 'c':
                checkSuffix = ''

            if checkIfFileIsInDB(dataFileName, checkSuffix, deviceName, totalLineCount, serverConfig=configName):
                continue

            maxRowCounts[deviceName + const.suffixAll] = getMaxRowNumber(fileName=dataFileName,
                                                                         deviceName=deviceName,
                                                                         measurementSuffix=
                                                                         const.suffixAll, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixCharge] = getMaxRowNumber(fileName=dataFileName,
                                                                            deviceName=deviceName,
                                                                            measurementSuffix=
                                                                            const.suffixCharge, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixMaxCurrent] = getMaxRowNumber(fileName=dataFileName,
                                                                                deviceName=deviceName,
                                                                                measurementSuffix=
                                                                                const.suffixMaxCurrent, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixMeanCurrent] = getMaxRowNumber(fileName=dataFileName,
                                                                                 deviceName=deviceName,
                                                                                 measurementSuffix=
                                                                                 const.suffixMeanCurrent, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixParticles] = getMaxRowNumber(fileName=dataFileName,
                                                                               deviceName=deviceName,
                                                                               measurementSuffix=
                                                                               const.suffixParticles, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixRawdata] = getMaxRowNumber(fileName=dataFileName,
                                                                             deviceName=deviceName,
                                                                             measurementSuffix=const.
                                                                             suffixRoiParticles, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixRawdataCols] = getMaxRowNumber(fileName=dataFileName,
                                                                                 deviceName=deviceName,
                                                                                 measurementSuffix=const.
                                                                                 suffixRawdataCols, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixRoiCharge] = getMaxRowNumber(fileName=dataFileName,
                                                                               deviceName=deviceName,
                                                                               measurementSuffix=
                                                                               const.suffixRoiCharge, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixRoiMaxCurrent] = getMaxRowNumber(fileName=dataFileName,
                                                                                   deviceName=deviceName,
                                                                                   measurementSuffix
                                                                                   =const.suffixRoiMaxCurrent, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixRoiMeanCurrent] = getMaxRowNumber(fileName=dataFileName,
                                                                                    deviceName=deviceName,
                                                                                    measurementSuffix
                                                                                    =const.suffixRoiMeanCurrent, serverConfig=configName)

            maxRowCounts[deviceName + const.suffixRoiParticles] = getMaxRowNumber(fileName=dataFileName,
                                                                                  deviceName=deviceName,
                                                                                  measurementSuffix
                                                                                  =const.suffixRoiParticles, serverConfig=configName)

            print('Found ' + str (maxRowCounts[deviceName + const.suffixRoiParticles]) + ' data records related to '
                                                                                         'this file in DB.')

            fileObject = open(myPath + dataFileName, "r")
            lineCounter = 0

            dataFileLine = fileObject.readline()

            startTime = datetime.datetime.now()

            parser = DatFileParser_Trafo(deviceName=deviceName, serverConfig=configName)

            dbData = list()

            taskWorkers = list()

            with ProcessPoolExecutor() as taskWorker:
                while dataFileLine:

                    if dataFileLine.startswith('#'):
                        dataFileLine = fileObject.readline()
                        continue

                    lineCounter = lineCounter + 1

                    maxRowCount = maxRowCounts[deviceName + const.suffixRawdataCols]

                    if lineCounter <= maxRowCount:
                        dataFileLine = fileObject.readline()
                        AbstractDatFileParser.updateProgressBar(startTime, lineCounter, totalLineCount, dataFileName)
                        continue

                    if processParallel:
                        taskWorkers.append(taskWorker.submit(performTrafoLineParsing,
                                                             parser,
                                                             dataFileLine,
                                                             deviceName,
                                                             dataFileName,
                                                             lineCounter,
                                                             maxRowCounts,
                                                             rawDataResamplingFactor,
                                                             storageMode))

                    else:
                        dbData.extend(performTrafoLineParsing(parser,
                                                              dataFileLine,
                                                              deviceName,
                                                              dataFileName,
                                                              lineCounter,
                                                              maxRowCounters=maxRowCounts,
                                                              rawDataResamplingFactor=rawDataResamplingFactor,
                                                              storageMode=storageMode))

                        #time.sleep(0.2)  # wait 200 ms to prevent overload

                        AbstractDatFileParser.updateProgressBar(startTime, lineCounter, totalLineCount, dataFileName)

                    dataFileLine = fileObject.readline()

                fileObject.close()

                if processParallel:
                    completedTaskCounter = 0
                    for completedTask in concurrent.futures.as_completed(taskWorkers):
                        completedTaskCounter = completedTaskCounter + 1
                        dbData.extend(completedTask.result())
                        AbstractDatFileParser.updateProgressBar(startTime, completedTaskCounter, totalLineCount,
                                                                dataFileName)

            dbClient = InfluxDbConnector('HTTP', serverConfig=configName)

            taskInfo = None

            try:
                print("Writing " + dataFileName + " to DB...")
                taskInfo = dbClient.writeToDBStrings(dbData, writeParallel, chunks)
            except Exception as error:
                print('Error when writing to DB: ' + str(error))
                exit()

            if writeParallel == True:
                completedTaskCounter = 0
                startTime = datetime.datetime.now()
                print('Done submitting, waiting for tasks being executed...')
                for completedTask in concurrent.futures.as_completed(taskInfo[0]):
                    completedTaskCounter = completedTaskCounter + 1
                    InfluxDbConnector.updateProgressBarWriteChunks(startTime, completedTaskCounter, taskInfo[1])

        datetimeFormat = '%Y-%m-%d %H:%M:%S.%f'
        processingTime = datetime.datetime.strptime(str(datetime.datetime.now()), datetimeFormat) \
                         - datetime.datetime.strptime(str(fileProcessingStartTime), datetimeFormat)
        print('Done processing file ', dataFileName, ', processing took ', processingTime, ' seconds.')

    exit()


if __name__ == "__main__":
    main()
