import yaml
import os


class ConfigProvider:

    _configData = None

    def __init__(self):
        script_dir = os.path.dirname(__file__)
        file_path = os.path.join(script_dir, './resources/ActiveMqConfig.yml')
        print(file_path)

        with open(file_path, "r") as configFile:
            self._configData = yaml.load(configFile, Loader=yaml.FullLoader)

    def getBroker_Url(self):
        return self._configData['message_broker']['url']

    def getBroker_Port(self):
        return self._configData['message_broker']['port']

    def getBroker_LoggingQueueName(self):
        return self._configData['message_broker']['matrix_profile_queue']

    def getBroker_ErrorQueueName(self):
        return self._configData['message_broker']['error_queue']

    def getBroker_TestQueueName(self):
        return self._configData['message_broker']['test_queue']

    def getBroker_User(self):
        return self._configData['message_broker']['user']

    def getBroker_password(self):
        return self._configData['message_broker']['password']
