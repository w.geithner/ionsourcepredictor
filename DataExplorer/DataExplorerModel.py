from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from Framework.FluxStatementBuilder import QueryStatement
from Framework.InfluxDbConfigReader import InfluxDbConfigReader as icr
import numpy as np
import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt


class DataExplorerModel():
    __influxConnector = InfluxDbConnector('HTTP', serverConfig='productive')
    __influxClient = None

    def __init__(self):\
        self._productiveDB = icr().getProductiveDB()

    def closeInfluxDbClient(self):
        self.__influxClient.close()

    def getAvailableSequenceStartStamps(self, measurementID: str = '/./') -> dict:
        """
        Method to fetch all available sequence start stamps from DB. these are required for sequence-tagged data

        :returns dictionary of two lists of sequence time stamps and acquisition time stamps in the database
        """

        statement: str = QueryStatement(self._productiveDB) \
            .filter_measurement_single(measurementID) \
            .filter_field_single('sequenceStartStamp') \
            .build_statement()

        result = self.__executeQuery(statement=statement)

        output = dict()

        sequenceStartStampItems = list()
        timeStampItems = list()

        counter = 0
        limit = None
        for chunks in result:
            for item in chunks:
                print('Processing timestamp item', item, end='\r')
                sequenceStartStampItems.append(item['sequenceStartStamp'])
                timeStamp: pd.Timestamp = pd.to_datetime(item['time'], format='%Y-%m-%dT%H:%M:%S.%fZ')
                unixTime = timeStamp.value
                timeStampItems.append(unixTime)
                if limit is not None:
                    counter = counter + 1
                    if counter == limit:
                        break

        print()
        print('fetched ' + str(len(np.unique(sequenceStartStampItems))) + ' sequence start timestamps.')
        output['sequenceStartStamps'] = np.unique(sequenceStartStampItems)
        output['timeStamps'] = np.unique(timeStampItems)
        return output

    def getFirstLastAvailableTimeStamps(self, measurementID: str):
        statement: str = QueryStatement(self._productiveDB) \
            .filter_measurement_single(measurementID) \
            .filter_field_single('sequenceStartStamp') \
            .first() \
            .build_statement()
        timeStamps = dict()
        sequenceStartStamps = dict()
        for result in self.__executeQuery(statement=statement).get_points():
            timeStamps['first'] = result['time']
            sequenceStartStamps['first'] = result['first']

        statement: str = QueryStatement(self._productiveDB) \
            .filter_measurement_single(measurementID) \
            .filter_field_single('sequenceStartStamp') \
            .last() \
            .build_statement()
        for result in self.__executeQuery(statement=statement).get_points():
            timeStamps['last'] = result['time']
            sequenceStartStamps['last'] = result['last']

        return timeStamps, sequenceStartStamps


    def getMeasurementsForSequenceStartStamp(self, timeStamp, measurementID: str = '/./'):
        statement: str = QueryStatement(self._productiveDB) \
            .filter_measurement_single(measurementID) \
            .filter_field_single('sequenceStartStamp') \
            .__add_to_statement__(' |> filter(fn: (r) => r._value == ' + timeStamp + ')') \
            .build_statement()

        print('Executing statement: ' + statement)
        result = self.__executeQuery(statement=statement)
        return result

    def getTrafoRawDataAll(self) -> DataFrame:
        statement: str = QueryStatement(self._productiveDB).filter_measurement_single('YRT1DT1F_all').filter_field_regex('/rd.').build_statement()
        result = self.__executeQuery(statement=statement, autoclose=True)

        print('Finished fetching all raw data.')

        rawDataFrame: DataFrame = DataFrame()

        for item in result:
            itemContent = item[0]
            dataKeys = itemContent.keys()
            rawData = list()
            columns = list()
            time = None
            index = 0
            for key in dataKeys:
                rawData.append(itemContent[key])
                columns.append(key)

            shotSeries = pd.Series(rawData, columns)
            rawDataFrame = rawDataFrame.append(shotSeries, ignore_index=True)

        return rawDataFrame

    def getTrafoRawDataForTimeStampWindow(self, lowerTimeStamp: int, upperTimeStamp: int = None) -> DataFrame:
        """
        Method fetching trafo raw data frmo measurement YRT1DT1F_all by time stamp.
        :param lowerTimeStamp: lower value of timestamp defining fetch window. If parameter upperTimeStamp is omitted in
        method call, the time stamp WHERE condition must be equal to this value
        :param upperTimeStamp: upper timestamp fetch window. Can be omitted in method call.
        :return:
        """
        statement: str = ''
        if upperTimeStamp is None:
            statement = QueryStatement(self._productiveDB).filter_measurement_single('YRT1DT1F').range(str(
                lowerTimeStamp), str(lowerTimeStamp)).filter_field_multiple(['filename', '/rawdata./']).build_statement()
        else:
            statement = QueryStatement(self._productiveDB).filter_measurement_single('YRT1DT1F').range(
                str(lowerTimeStamp), str(upperTimeStamp)).filter_field_multiple(
                ['filename', '/rawdata./']).build_statement()
        result: DataFrame = self.__executeQuery(statement=statement)
        return result

    def __executeQuery(self, statement, autoclose: bool = False) -> DataFrame:
        if self.__influxClient is None:
            self.__influxClient = self.__influxConnector.initFluxClient()

        self.__influxConnector.initFluxClient()
        try:
            return self.__influxConnector.executeFluxStatement(statement)
        except Exception as err:
            print('Error occurred: ' + str(err))
            exit()

    def getTimeStampFromDB(self, fieldName: str, measurementID: str) -> int:
        # first find out the first and last timestamp in database
        statement: str = QueryStatement(self._productiveDB).filter_measurement_single(
            measurementID).filter_field_single(fieldName).build_statement()
        result: DataFrame = self.__executeQuery(statement=statement)
        if len(result.values) > 0:
            # timeStampString: str = pd.to_datetime().timestamp()
            # timeStamp: pd.Timestamp = pd.to_datetime(timeStampString, format='%Y-%m-%dT%H:%M:%S.%fZ')
            timeStamp: pd.Timestamp = result.index[0]
            epochTime = timeStamp.value
            # epochTime = int(calendar.timegm(timeStamp.utctimetuple()))
            # epochTime = epochTime  # correct for Greenwich mean time
            return epochTime
        else:
            return 0

    def getDataInTimeRange(self, measurementID: str, selectField: str = None, startEpoch: int = None, endEpoch: int = \
        None) -> pd.DataFrame:
        if startEpoch is None:
            firstEpochTime = self.getTimeStampFromDB('first(charge)', 'YRT1DT1F_all')
        else:
            firstEpochTime = startEpoch

        if endEpoch is None:
            lastEpochTime = firstEpochTime = self.getTimeStampFromDB('last(charge)', 'YRT1DT1F_all')
        else:
            lastEpochTime = endEpoch
        # Now read rawdata in time range
        statement: str = QueryStatement(self._productiveDB) \
                            .filter_measurement_single(measurementID) \
                            .range(str(firstEpochTime), str(lastEpochTime)) \
                            .filter_field_single(selectField) \
                            .build_statement()
        result: pd.DataFrame = self.__executeQuery(statement=statement)

        return result


def processRawDataUsingSequenceStartStamp():
    model = DataExplorerModel()
    measurementID = 'YRT1DT1F_all'
    startStamps = model.getAvailableSequenceStartStamps(measurementID=measurementID)

    rawDataFrame = pd.DataFrame()

    for i in range(0, 200):
        result = model.get.getTrafoRawDataForSequenceStartStamp(startStamps[i])
        for item in result:
            itemContent = item[0]
            dataKeys = itemContent.keys()
            rawData = list()
            columns = list()
            time = None
            index = 0
            for key in dataKeys:
                if str(key).find('rd') > -1:
                    rawData.append(itemContent[key])
                    columns.append(key)
                if str(key).find('time') > -1:
                    donothing = True
            # rawData.append(pd.to_datetime(itemContent[key]))
            # columns.append(key)

            shotSeries = pd.Series(rawData, columns)
            rawDataFrame = rawDataFrame.append(shotSeries, ignore_index=True)

    model.closeInfluxDbClient()

    plt.pcolor(rawDataFrame)
    plt.show()


def main():
    model = DataExplorerModel()
    measurementID = 'YRT1DT1F_rawdata'
    startTimeStamp = model.getTimeStampFromDB(fieldName='first(rawdata)', measurementID=measurementID)
    endTimeStamp = model.getTimeStampFromDB(fieldName='last(rawdata)', measurementID=measurementID)

    timeSlot = 1 * 1 * 100000000

    while startTimeStamp < endTimeStamp:
        endEpoch = startTimeStamp + timeSlot
        dbResult: DataFrame = model.getDataInTimeRange(selectField='rawdata', measurementID=measurementID,
                                                       startEpoch=startTimeStamp, endEpoch=endEpoch)


        x_data = list()
        y_data = list()
        count = 0
        for rawDataSet in dbResult.get_points():
            count = count + 1

        print(count)
        #     x_data.append(rawDataSet['time'])
        #     y_data.append(rawDataSet['rawdata'])
        #
        # plt.close()
        # plt.plot(x_data, y_data)
        # plt.show()

        startTimeStamp = startTimeStamp + timeSlot - 1


if __name__ == '__main__':
    main()