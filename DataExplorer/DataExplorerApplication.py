""""
This file contains only the main() routine (plus supporting function) to be able to start the application
Actual functionality is encapsulated in Model and View classes
Programming model see: https://doc.qt.io/qt-5/model-view-programming.htm
"""
from plistlib import Data

from DataExplorer.DataExplorerView import DataExplorerView

def main():

    theView = DataExplorerView()

if __name__ == "__main__":
    main()
