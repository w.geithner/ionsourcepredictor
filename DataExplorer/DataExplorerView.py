from DataExplorer.DataExplorerModel import DataExplorerModel
from PyQt5 import QtWidgets, QtGui, uic

uiDesignFileName = 'DataExplorer.ui'
Ui_MainWindow, QtBaseClass = uic.loadUiType(uiDesignFileName)


class DataExplorerView(QtWidgets.QMainWindow, Ui_MainWindow):

    _uiModel = None
    _theApplication = None

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self._uiModel = DataExplorerModel()
        self.__initUi__()
        self._theApplication.exec()

    def __initUi__(self):

        mainWindow = QtGui.QWindow()
