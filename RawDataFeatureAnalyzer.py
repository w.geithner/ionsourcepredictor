from DataExplorer.DataExplorerModel import DataExplorerModel
from Framework.InfluxDbConnectorFlux import InfluxDbConnector
from pandas import DataFrame
import numpy as np
import concurrent.futures
from concurrent.futures import ProcessPoolExecutor
import pandas as pd
import datetime
import matplotlib.pyplot as plt
from scipy import signal
from scipy import stats
from scipy import optimize
import peakutils

showPlots = False

averagingWindowSize = 200
averagingWindow = signal.windows.blackmanharris(averagingWindowSize)
model = DataExplorerModel()


def pinkNoiseFitFunction(fitParams, freq, yData):
    return np.exp(1/(fitParams[0] * freq)) + \
           fitParams[1] * \
           np.exp(-np.power(freq - fitParams[2], 2.) / (2 * np.power(fitParams[3], 2.))) + \
           fitParams[4] * \
           np.exp(-np.power(freq - fitParams[5], 2.) / (2 * np.power(fitParams[6], 2.))) \
           - yData


def pinkNoiseFitFunction2(x, P1, P2, P3, P4, P5, P6, P7, P8, P9):
    return P1 + P2 / x + P3 * x + \
           P4 * \
           np.exp(-(x - P5)**2 / (2 * P6**2)) + \
           P7 * \
           np.exp(-(x - P8)**2 / (2 * P9**2))

def parabolicFunction(x, P0, P1, P2):
    """
    Function for checking the curvature of the raw data signal
    :param x: independent variable
    :param P0: Offset/Baseline
    :param P1: parable scaling factor
    :param P2: parable x-offset
    :return: the function
    """
    return P0 + P1 * (x - P2)**2


def processLine(startStamp: int, rawDataSet: np.array, fileName: str):

    rawData = DataFrame(rawDataSet, index=[0]).filter(regex='rd.').to_numpy()[0]

    if showPlots:
        plt.close()
        plt.plot(rawData)
        plt.title('RawData')
        plt.show()

    resultRow = dict()
    resultString = 'YRT1DT1F_analysis'

    resultRow['sequenceStartStamp'] = startStamp
    resultString = resultString + ' sequenceStartStamp=' + str(startStamp)
    resultString = resultString + ',filename="' + fileName + '"'

    # smooth data
    # see. https://www.quora.com/How-do-I-perform-moving-average-in-Python
    rawDataSmoothed = np.convolve(rawData, averagingWindow)
    # truncate data by width of smoothing window to remove artifacts
    rawDataSmoothedWindowed = rawDataSmoothed[averagingWindowSize + 1:len(rawData) - averagingWindowSize - 1]

    if showPlots:
        plt.close()
        plt.plot(rawDataSmoothed[averagingWindowSize: len(rawDataSmoothed) - averagingWindowSize])
        plt.title('raw data smoothed')
        plt.show()

    # get derivative
    gradient = np.gradient(rawDataSmoothedWindowed)
    gradient = np.convolve(gradient, averagingWindow)

    if showPlots:
        plt.close()
        plt.plot(gradient)
        plt.title('derivative')
        plt.show()

    # rising gradient should be in first half of data
    risingGradient = np.max(gradient[:int(len(gradient)/2)])
    resultString = resultString + ',gradient_value_rising=' + str(risingGradient)
    risingIndex = np.where(gradient == risingGradient)
    risingIndex = risingIndex[0][0]
    resultRow['channel_rising'] = risingIndex
    resultString = resultString + ',channel_rising=' + str(risingIndex)

    # find index of rising and falling slope of the signal
    fallingGradient = np.min(gradient[int(len(gradient)/2):])
    resultString = resultString + ',gradient_value_falling=' + str(risingGradient)
    fallingIndex = np.where(gradient == fallingGradient)
    fallingIndex = fallingIndex[0][0]
    resultRow['channelFalling'] = fallingIndex
    resultString = resultString + ',channel_falling=' + str(fallingIndex)

    signalHighData = None
    if fallingIndex > risingIndex:
        signalHighData = rawData[risingIndex:fallingIndex]

    if showPlots:
        plt.close()
        plt.plot(signalHighData)
        plt.title('Signal High')
        plt.show()

    # ***********************************************************************************************
    # statistics factors of Beam ON signal
    # ***********************************************************************************************
    signalHighMean = None
    if signalHighData is not None:
        nObs, minmax, mean, variance, skewness, kurtosis = stats.describe(signalHighData)
        signalHighMean = mean
        resultRow['signalHigh'] = signalHighMean
        resultString = resultString + ',signal_high_mean=' + str(signalHighMean)
        resultString = resultString + ',signal_high_variance=' + str(variance)
        resultString = resultString + ',signal_high_skewness=' + str(skewness)
        resultString = resultString + ',signal_high_kurtosis=' + str(kurtosis)

    signalSlope = None

    # ***********************************************************************************************
    # slope of the Beam ON signal
    # ***********************************************************************************************
    if signalHighData is not None and len(signalHighData) >= 5:
        try:
            slope, intercept, r_value, p_value, std_err = stats.linregress(np.arange(0, len(signalHighData)), signalHighData)
            signalSlope = list()
            for x in range(0, len(signalHighData)):
                signalSlope.append(slope * x + intercept)

            resultString = resultString + ',signal_fit_slope=' + str(slope)
            resultString = resultString + ',signal_fit_Intercept=' + str(intercept)
            resultString = resultString + ',signal_fit_rvalue=' + str(r_value)
            resultString = resultString + ',signal_fit_pvalue=' + str(p_value)
            resultString = resultString + ',signal_fit_stderr=' + str(std_err)
        except Exception as err:
            print('Error when performing linear regression: ', err)

        # ***********************************************************************************************
        # curvature of the Beam ON signal
        # ***********************************************************************************************
        try:
            xdata = np.arange(len(signalHighData))
            fitResult, fitCov = optimize.curve_fit(parabolicFunction, xdata=xdata,
                                             ydata=signalHighData, p0=[signalHighMean, 0.0, len(signalHighData)/2])

            resultString = resultString + ',signal_parabolic_ampl=' + str(fitResult[1])
            resultString = resultString + ',signal_parabolic_center=' + str(fitResult[2])

            if showPlots:
                plt.close()
                plt.plot(signalHighData)
                plt.plot(xdata, parabolicFunction(xdata, *fitResult))
                plt.show()

        except Exception as err:
            print ('Error when performing parabolic fit: ', err, ' setting default values')
            resultString = resultString + ',signal_parabolic_ampl=' + str(0.0)
            resultString = resultString + ',signal_parabolic_center=' + str(len(signalHighData)/2)

    # ***********************************************************************************************
    # background characterizaton
    # ***********************************************************************************************
    backgroundBeforeSignal = rawData[0:risingIndex - averagingWindowSize]
    backgroundAfterSignal = rawData[fallingIndex + averagingWindowSize:len(rawData)]
    backgroundData = np.concatenate((backgroundBeforeSignal, backgroundAfterSignal), axis=0)

    # plt.close()
    # plt.plot(backgroundData)
    # plt.show()

    backgroundMean = np.mean(backgroundData)
    resultRow['background'] = backgroundMean
    resultString = resultString + ',background_mean=' + str(backgroundMean)
    backgroundStd = np.std(backgroundData)
    resultString = resultString + ',background_std=' + str(backgroundStd)
    resultRow['sigAmplitude'] = signalHighMean - backgroundMean
    resultString = resultString + ',signal_amplitude=' + str(signalHighMean - backgroundMean)

    # Perform gymnastics for data within and without window between falling/rising slopes
    # FFT of SMOOTHED data within window
    if signalHighData is not None and len(signalHighData) >= 3 and signalSlope is not None:
        try:
            inputData = np.subtract(signalHighData, signalSlope)
            smoothingWindowSize = 201
            smoothingWindow = np.ones(smoothingWindowSize)
            smoothingWindow = signal.windows.blackmanharris(smoothingWindowSize)
            inputSignal = np.convolve(inputData, smoothingWindow)
            inputSignal = inputSignal[smoothingWindowSize:len(inputSignal) - smoothingWindowSize]
            peakIndexes = peakutils.indexes(inputSignal, min_dist=1, thres=0.5)

            resultString = resultString + ',signal_peak_count=' + str(len(peakIndexes))
        except Exception as err:
            print('Error during peak determination: ', err)

    if resultString.endswith(','):
        resultString.rstrip(',')

    resultString = resultString + ' ' + str(startStamp)

    return resultString


def main():
    # check what is the last analysis dataset available in DB
    lastConvertedInDB = model.getTimeStampFromDB(fieldName='last(background_mean)', measurementID='YRT1DT1F_analysis')
    datetimeFormat = '%Y-%m-%d %H:%M:%S.%f'

    measurementID = 'YRT1DT1F_all'

    # fetch all sequenceStartStamps from DB
    print('Fetching raw data timestamps from DB. This takes a while...')
    timeStamps, sequenceStartStamps = model.getFirstLastAvailableTimeStamps(measurementID)

    submittedCounter = 1
    timeWindow = 600000000000

    startTimeStamp = timeStamp = lastConvertedInDB

    if timeStamp == 0:
        startTimeStamp = timeStamp = pd.to_datetime(timeStamps['first']).value

    endTimeStamp = pd.to_datetime(timeStamps['last']).value

    while timeStamp <= endTimeStamp:
        resultList = list()
        for resultItem in model.getTrafoRawDataForTimeStampWindow(timeStamp, timeStamp + timeWindow).get_points():
            resultList.append(resultItem)

        if len(resultList) == 0:
            print('No data for time  window ', timeStamp, ' -> ', timeStamp + timeWindow)
            timeStamp = timeStamp + timeWindow
            continue

        runningProcesses = list()
        with ProcessPoolExecutor() as processExecutor:
            for rawDataSet in resultList:
                rawDataTime = rawDataSet['time']
                timestamp = pd.to_datetime(rawDataTime)

                if timestamp.value <= lastConvertedInDB:
                    print(timestamp.value, ' is already in DB.YRT1DT1F_analysis.', end='\r')
                    continue

                startTime = datetime.datetime.now()

                runningProcesses.append(processExecutor.submit(processLine, timestamp.value, rawDataSet, rawDataSet[
                    'filename']))
                # resultString, fftData = processLine(timestamp.value, rawData=rawData, fileName=fileName)
                print('Submitted: ', timestamp, ' task list length: ', len(runningProcesses), end='\r')

                if len(runningProcesses) == 30:
                    print()
                    resultStrings = list()
                    # collect results
                    finishedCounter = 0
                    for finishedItem in concurrent.futures.as_completed(runningProcesses):
                        resultString = ''
                        try:
                            resultString = finishedItem.result()
                        except Exception as err:
                            print('Error caught for finished item: ' + str(err))
                        if len(resultString) > 0:
                            resultStrings.append(resultString)

                        finishedCounter = finishedCounter + 1
                        print(finishedCounter, ' of ', len(runningProcesses), ' finished', end='\r')

                    print()
                    runningProcesses = list()

                    endTime = datetime.datetime.now()
                    processingTime = datetime.datetime.strptime(str(endTime), datetimeFormat) - datetime.datetime.strptime(
                        str(startTime), datetimeFormat)
                    print('Processing data of ', timestamp, ', ', submittedCounter, ' of ', len(timeStamps), 'processed, '
                                                                                                       'processing took ', processingTime,
                          ' array length: ', len(resultStrings), end='\n')

                    try:
                        dbClient = InfluxDbConnector('HTTP')
                        dbClient.writeToDBStrings(resultStrings, True, chunks=1000)
                    except Exception as error:
                        print('Error when writing to DB: ' + str(error))
                        exit()

                    resultStrings.clear()

                submittedCounter = submittedCounter + 1

        print()
        timeStamp = timeStamp + timeWindow

    model.closeInfluxDbClient()

    exit()

if __name__ == "__main__":
    main()