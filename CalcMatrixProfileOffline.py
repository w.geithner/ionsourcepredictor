from enum import Enum

from DataExtraction.InfluxDataExtractor import InfluxDataExtractor
from Framework import CommonFunctions as cf
from Framework.FluxStatementBuilder import QueryStatement
from Framework.MatrixProfileCalculatorConfigReader import MatrixProfileCalculatorConfigReader as config_reader
from Framework.InfluxDbConnectorFlux import InfluxDbConnector, Configurations as conf
import pandas as pd
from pandas import DataFrame, Series
from scipy import signal, stats
import matrixprofile
from datetime import datetime
from timeit import default_timer as dt
import numpy as np
import concurrent.futures
import platform
import sys

config = config_reader().configuration(sys.argv[1]) # config reader reads file "resources/MatrixProfileCalculator.yml"
_parameterName: str = config.parameter_name()
_testField: str = config.test_field_name()
_startTime: str = config.start_time()
_endTime: str = config.end_time()
_spillCount: int = config.spill_count()
_writeRawdata: bool = config.write_rawdata()
_resampleFactor: float = config.resample_factor()
_smoothingSize: int = config.smoothing_size()
_searchTimeAdvance = config.search_time_advance()
_fetchWindowSize = config.fetch_window_size()

class MatrixProfileProcessor():
    __dbConnector: InfluxDbConnector = InfluxDbConnector('HTTP', serverConfig=conf.productive.value)
    __endTime: str = ''
    __parameterName: str = ''
    __resampleFactor: float = 0.
    __smoothingSize: int = 0
    __spillCount: int = 0
    __startTime: str = ''
    __testField: str = ''
    __writeRawData: bool = False

    def __init__(self, parameterName: str, testFieldName: str, startTime: str, endTime: str, spillCount: int,
                 resampleFactor: float, smoothingSize: int, writeMpRawData: bool = False):
        self.__parameterName = parameterName
        self.__setStartTime__(startTime)
        self.__endTime = endTime
        self.__testField = testFieldName
        self.__spillCount = spillCount
        self.__writeRawData = writeMpRawData
        self.__resampleFactor = resampleFactor
        self.__smoothingSize = smoothingSize
        self.__dbConnector.initFluxClient()

    def __getMatrixProfileInfluxPoint__(self, dataSet: np.ndarray, windowSize: int, prefix: str,
                                    init: bool = False) -> str:
        matrixProfile: dict = matrixprofile.algorithms.scrimp_plus_plus(dataSet, window_size=windowSize,
                                                                     n_jobs=4)
        mpStatistics = stats.describe(matrixProfile['mp'])

        firstChar = ','

        if init:
            firstChar = ' '

        influxPointPart: str = firstChar + prefix + '_min=' + str(mpStatistics.minmax[0]) \
                              + ',' + prefix + '_max=' + str(mpStatistics.minmax[1]) \
                              + ',' + prefix + '_mean=' + str(mpStatistics.mean) \
                              + ',' + prefix + '_variance=' + str(mpStatistics.variance) \
                              + ',' + prefix + '_skewness=' + str(mpStatistics.skewness) \
                              + ',' + prefix + '_kurtosis=' + str(mpStatistics.kurtosis) \
                              + ',' + prefix + '_mpwindowsize=' + str(windowSize)

        if self.__writeRawData:
            pointCount = 1
            for value in matrixProfile['mp']:
                influxPointPart = influxPointPart + ",rawdata_mp" + str(pointCount).zfill(6) + '=' + str(value)
                pointCount = pointCount + 1

        return influxPointPart

    def __process__(self, startTime: str, endTime: str):

        timerStart = dt()

        theDataExtractor = InfluxDataExtractor()

        statement = QueryStatement(self.__dbConnector.getActiveDatabase()) \
            .range(startTime, endTime) \
            .filter_measurement_single(self.__parameterName) \
            .filter_field_regex(cf.RawdataRegex()) \
            .limit(self.__spillCount) \
            .__add_to_statement__(' |> group(columns:["_field"])') \
            .sort(sort_by_columns=['_time'], sort_direction='ASC') \
            .build_statement()

        result: DataFrame = theDataExtractor.executeStatement(statement)
        if len(result.values) < self.__spillCount:
            # if no data has been found move start time by 20 seconds
            startTimestamp = datetime.strptime(startTime, cf.InfluxTimeFormatString()).timestamp()
            self.__setStartTime__(datetime.fromtimestamp(startTimestamp + _searchTimeAdvance).strftime(cf.InfluxTimeFormatString()))
            return

        loopCounter = 0
        datasetTime = ''
        rawMpWindowLength = 0
        rawDataSet = pd.Series(dtype=float)

        datasetTime = pd.to_datetime(result.index[0])
        newStartTime:pd.Timestamp = result.index[len(result.index) - 2]
        self.__setStartTime__(newStartTime.strftime(cf.InfluxTimeFormatString()))

        # create data series for further processing
        for rawData in result.values:
            if loopCounter == 0:
                rawMpWindowLength = len(rawData)  # datasetTime = rawData[len(rawData) - 1]
            elif loopCounter == 4:
                print('dataset time:', datasetTime)
            dataToAppend: Series = Series(rawData, dtype=float)
            rawDataSet = rawDataSet.append(dataToAppend, ignore_index=True)
            loopCounter = loopCounter + 1

        print('Raw data window size:', rawMpWindowLength)

        influxPoint = self.__parameterName + '_MatrixProfileOffline shotcount=' + str(self.__spillCount)

        rawFuture = None
        smoothedData = signal.convolve(rawDataSet.values, signal.windows.blackmanharris(self.__smoothingSize))
        smoothedData = smoothedData[self.__smoothingSize:(len(smoothedData) - self.__smoothingSize)]
        resampledData = signal.resample(rawDataSet.values, int(len(rawDataSet.values) * self.__resampleFactor))

        runningProcesses = list()
        with concurrent.futures.ProcessPoolExecutor() as executor:
            print('Submitting raw')
            runningProcesses.append(executor.submit(self.__getMatrixProfileInfluxPoint__, rawDataSet.values,
                                                                                    rawMpWindowLength, 'raw'))
            print('Submitting smooth')
            runningProcesses.append(executor.submit(self.__getMatrixProfileInfluxPoint__, smoothedData,
                                                                                    int(len(smoothedData) /
                                                                                        self.__spillCount),
                                                                                           'smoothed'))
            print('Submitting resampled')
            runningProcesses.append(executor.submit(self.__getMatrixProfileInfluxPoint__, resampledData,
                                                                                           int(rawMpWindowLength *
                                                                              self.__resampleFactor), 'resampled'))
        runningProcessCount = 3
        influxPointAppends = ''
        while runningProcessCount > 0:
            runningProcessCount = 3
            for process in runningProcesses:
                if process.done():
                    influxPointAppends = influxPointAppends + process.result()
                    runningProcessCount = runningProcessCount - 1

        influxPoint = influxPoint + influxPointAppends
        influxPoint = influxPoint + ',processing_time=' + str(dt() - timerStart)
        influxPoint = influxPoint + ',os="' + platform.system() + '"'
        influxPoint = influxPoint + ',cpu="' + platform.processor() + '"'

        # add 7200 seconds to adjust UTC and MEST
        influxpointTime = int((pd.to_datetime(datasetTime).timestamp() + 0.0) * 10**9)

        influxPoint = influxPoint + ' ' + str(influxpointTime)

        # print("New influx point: ", influxPoint)
        print('Writing to InfluxDB:', influxPoint)
        self.__dbConnector.writeToDBStrings([influxPoint], writeParallel=True, chunks=5000)
        print('Process cycle took: ', str(dt() - timerStart), ' seconds.')

    def runProcess(self):
        statement = QueryStatement(self.__dbConnector.getActiveDatabase()) \
                        .range(self.__startTime, self.__endTime) \
                        .filter_measurement_single(self.__parameterName) \
                        .filter_field_single(self.__testField) \
                        .first() \
                        .build_statement()


        theDataExtractor = InfluxDataExtractor()
        result: pd.DataFrame = theDataExtractor.executeStatement(statement)
        # get new start time by shifting start time
        shifted_time: pd.Timestamp = result.index[0]- pd.to_timedelta(0.1, unit='seconds')
        self.__setStartTime__(shifted_time.strftime(cf.InfluxTimeFormatString()))

        while True:
            print('Fetching next 5 data sets starting from:', self.__startTime)
            startTimestamp = datetime.strptime(self.__startTime, cf.InfluxTimeFormatString()).timestamp()
            self.__endTime = datetime.fromtimestamp(startTimestamp + _fetchWindowSize).strftime(cf.InfluxTimeFormatString())
            if startTimestamp > datetime.strptime(_endTime, cf.InfluxTimeFormatString()).timestamp():
                exit()
            self.__process__(self.__startTime, self.__endTime)

    def __setStartTime__(self, startTime):
        self.__startTime = startTime


if __name__ == '__main__':
    MatrixProfileProcessor(_parameterName, _testField, _startTime, _endTime, _spillCount, _resampleFactor,
                           _smoothingSize, _writeRawdata).runProcess()
