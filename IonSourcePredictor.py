# File built on InfluxDBDataReader.py (discontinued)
# switch to TF2.0 syntax

from Framework.influxDBReaders.InfluxDBReader_Trafo import InfluxTrafoDataReader
from pandas.plotting import register_matplotlib_converters
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
import os

from sklearn.preprocessing import MinMaxScaler
import tensorflow as tf


"""
Requirements: tensorflow 2.x

Script fetching raw data from the Influx-DB instance and transforming this data to the format required to train a
neural network following the blog 
https://medium.com/mlreview/a-simple-deep-learning-model-for-stock-price-prediction-using-tensorflow-30505541d877
"""


def plotXYPlot(xValues: np.array, yValues: np.array):
    plt.close()
    plt.plot(xValues, yValues)
    plt.show()


def plotMatrix(matrix: list, transpose: bool = False):
    plt.close()
    if transpose:
        matrix = np.transpose(matrix)

    plt.matshow(matrix)
    plt.show()


def getNextPowerOfTwo(number: int):
    pos = math.ceil(math.log2(number))
    result = math.pow(2, pos)

    return int(result)


def main():

    register_matplotlib_converters()  # to prevent a 'deprected message'

    dataReader = InfluxTrafoDataReader()

    # Read rawdata for certain timefame (cannot read all as this takes too long)
    resultRawData: pd.DataFrame = dataReader.getRawDataByAcqTimeFrame(lowerDate='2019-03-15', lowerTime='00:00:00',
                                                                      upperDate='2019-03-23', upperTime='10:59:00')

    # Read mean current data
    resultRoiMeanCurrent: pd.DataFrame = dataReader.getRoiMeanCurrentAll()

    print('Got data from database. Doing data gymnastics now...')

    # logic to shift data selection of roi mean currents (optimization goal) by N indices (for neural network training)
    # get timestamps contained in rawdata
    rawDataTimeStamps = resultRawData['timestampCycle'].drop_duplicates()

    # create an array of indices shifted ny N
    shiftBy = 1

    shiftedRoiMeanCurrents: pd.DataFrame = pd.DataFrame()

    for timeStamp in rawDataTimeStamps.array:
        originalRoiMeanCurrent = resultRoiMeanCurrent.loc[resultRoiMeanCurrent['timestampCycle'] == timeStamp]
        origIndex = originalRoiMeanCurrent.index
        shiftedIndex = origIndex + shiftBy
        shiftedRoiMeanCurrent: pd.DataFrame = resultRoiMeanCurrent.iloc[shiftedIndex].copy()
        originalTimestamp = originalRoiMeanCurrent['timestampCycle'].values[0]
        shiftedRoiMeanCurrent.at[shiftedRoiMeanCurrent.index, 'timestampCycle'] = originalTimestamp
        shiftedRoiMeanCurrents = shiftedRoiMeanCurrents.append(shiftedRoiMeanCurrent, sort=True)

    # Produce a merged DataFrame containing roi mean data plus rawdata
    inputData = []

    for item in shiftedRoiMeanCurrents.iterrows():
        itemData = item[1]
        itemTimeStamp = itemData['timestampCycle']
        # pick raw data-set related to current timestampCycle, remove not needed columns and transpose list to get
        # a row vector
        rawDataPackage: pd.DataFrame = resultRawData.loc[resultRawData['timestampCycle'] == itemTimeStamp] \
            .drop(['time', 'sequenceStartStamp', 'timestampCycle'], axis=1)
        rawDataPackage = rawDataPackage.transpose()

        # assign new column names to transposed DataFrame
        rawDataPackage.columns = list(map(lambda counter: 'raw' + f'{counter:05}', np.arange(rawDataPackage.size)))
        inputDataRow = [itemData['roiMeanCurrent']]

        inputRawData = rawDataPackage.values.flatten()

        # plt.plot(inputRawData)
        # plt.show()
        # plt.close()

        inputDataRow.extend(inputRawData)
        inputData.append(inputDataRow)

    print('Done with data gymnastics. Starting with network preparation. ' + str(len(inputData)) + ' data sets.')

    # *****************************************************************************************************************
    # Start or neural network training, code directly taken from:
    # https://medium.com/mlreview/a-simple-deep-learning-model-for-stock-price-prediction-using-tensorflow-30505541d877
    # *****************************************************************************************************************

    columnCount = len(inputData[0])
    rowCount = len(inputData)

    trainDataStart = 0
    trainDataEnd = int(np.floor(0.8 * rowCount))
    testDataStart = trainDataEnd
    testDataEnd = rowCount

    dataScaler = MinMaxScaler()

    # data preparation for training and test
    trainingData = inputData[trainDataStart: trainDataEnd]
    # scale data to fit to sigmodal activation function
    trainingData = dataScaler.fit_transform(trainingData)
    x_train = trainingData[:, 1:]
    y_train = trainingData[:, 0]

    plt.close()
    plt.plot(x_train[1])
    plt.show()
    plt.close()

    testData = inputData[testDataStart: testDataEnd]
    testData = dataScaler.transform(testData)
    x_test = testData[:, 1:]
    y_test = testData[:, 0]

    rawDataChannelCount = len(x_train[0])

    X = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None, rawDataChannelCount])
    Y = tf.compat.v1.placeholder(dtype=tf.float32, shape=[None])

    neuronCount_1 = getNextPowerOfTwo(int(rawDataChannelCount * 2.0))
    neuronCount_2 = int(neuronCount_1 / 2)
    neuronCount_3 = int(neuronCount_2 / 2)
    neuronCount_4 = int(neuronCount_3 / 2)
    neuronCount_5 = int(neuronCount_4 / 2)
    n_target = 1

    # for initializers see:
    # https://www.tensorflow.org/api_docs/python/tf/initializers

    sigma: int = 1
    weight_initializer = tf.variance_scaling_initializer(mode="fan_avg", distribution="uniform", scale=sigma)
    bias_initializer = tf.zeros_initializer()

    # Layer 1: Variables for hidden weights and biases
    W_hidden_1 = tf.Variable(weight_initializer(shape=[rawDataChannelCount, neuronCount_1]))
    bias_hidden_1 = tf.Variable(bias_initializer([neuronCount_1]))
    # Layer 2: Variables for hidden weights and biases
    W_hidden_2 = tf.Variable(weight_initializer([neuronCount_1, neuronCount_2]))
    bias_hidden_2 = tf.Variable(bias_initializer([neuronCount_2]))
    # Layer 3: Variables for hidden weights and biases
    W_hidden_3 = tf.Variable(weight_initializer([neuronCount_2, neuronCount_3]))
    bias_hidden_3 = tf.Variable(bias_initializer([neuronCount_3]))
    # Layer 4: Variables for hidden weights and biases
    W_hidden_4 = tf.Variable(weight_initializer([neuronCount_3, neuronCount_4]))
    bias_hidden_4 = tf.Variable(bias_initializer([neuronCount_4]))
    W_hidden_5 = tf.Variable(weight_initializer([neuronCount_4, neuronCount_5]))
    bias_hidden_5 = tf.Variable(bias_initializer([neuronCount_5]))

    # Output layer: Variables for output weights and biases
    W_out = tf.Variable(weight_initializer([neuronCount_5, n_target]))
    bias_out = tf.Variable(bias_initializer([n_target]))

    # Hidden layer
    hidden_1 = tf.nn.relu(tf.add(tf.matmul(X, W_hidden_1), bias_hidden_1))
    hidden_2 = tf.nn.relu(tf.add(tf.matmul(hidden_1, W_hidden_2), bias_hidden_2))
    hidden_3 = tf.nn.relu(tf.add(tf.matmul(hidden_2, W_hidden_3), bias_hidden_3))
    hidden_4 = tf.nn.relu(tf.add(tf.matmul(hidden_3, W_hidden_4), bias_hidden_4))
    hidden_5 = tf.nn.relu(tf.add(tf.matmul(hidden_4, W_hidden_5), bias_hidden_5))

    # Output layer (must be transposed)
    out = tf.transpose(tf.add(tf.matmul(hidden_5, W_out), bias_out))

    # Cost function
    mse = tf.reduce_mean(tf.math.squared_difference(out, Y))

    # Optimizer
    opt = tf.compat.v1.train.AdamOptimizer().minimize(mse)

    # Make Session
    net = tf.compat.v1.Session()
    # Run initializer
    net.run(tf.compat.v1.global_variables_initializer())

    # Setup interactive plot
    plt.ion()
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    line1, = ax1.plot(y_test)
    line2, = ax1.plot(y_test * 0.5)
    plt.show()

    # Number of epochs and batch size
    epochs = 100
    batch_size = 126

    for e in range(epochs):

        # Shuffle training data
        shuffle_indices = np.random.permutation(np.arange(len(y_train)))
        X_train = x_train[shuffle_indices]
        y_train = y_train[shuffle_indices]

        # Minibatch training
        for i in range(0, len(y_train) // batch_size):
            start = i * batch_size
            batch_x = X_train[start:start + batch_size]
            batch_y = y_train[start:start + batch_size]
            # Run optimizer with batch
            net.run(opt, feed_dict={X: batch_x, Y: batch_y})

            # Show progress
            if np.mod(i, 5) == 0:
                # Prediction
                pred = net.run(out, feed_dict={X: x_test})
                line2.set_ydata(pred)
                plt.title('Epoch ' + str(e) + ', Batch ' + str(i))
                file_name = os.getcwd() + os.sep + 'output/epoch_' + str(e) + '_batch_' + str(i) + '.png'
                plt.savefig(file_name)
                plt.pause(0.01)
    # Print final MSE after Training
    mse_final = net.run(mse, feed_dict={X: x_test, Y: y_test})
    print(mse_final)



if __name__ == '__main__':
    main()


